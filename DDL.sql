﻿/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     12/21/2017 3:22:40 PM                        */
/*==============================================================*/

drop index if exists AMOUNTHISTORY_PK;

drop table if exists AMOUNTHISTORY CASCADE;

drop index if exists ARRANGEMENT_PK;

drop table if exists ARRANGEMENT CASCADE;

drop index if exists OPER_IN_ARRANGEMENT_H_FK;

drop index if exists EMP_IN_ARRANGEMENT_H_FK;

drop index if exists DEVICE_IN_ARRANGEMENTHISTORY_FK;

drop index if exists ARRANGEMENTHISTORY_PK;

drop table if exists ARRANGEMENTHISTORY CASCADE;

drop index if exists BRANCH_PK;

drop table if exists BRANCH CASCADE;

drop index if exists CINEMA_PK;

drop table if exists CINEMA CASCADE;

drop index if exists DEVICE_IN_BRANCH_FK;

drop index if exists DEVICE_PK;

drop table if exists DEVICE CASCADE;

drop index if exists EMPLOYEEROLE_IN_EMPLOYEE_FK;

drop index if exists EMPLOYEE_PK;

drop table if exists EMPLOYEE CASCADE;

drop index if exists EMPLOYEEROLE_PK;

drop table if exists EMPLOYEEROLE CASCADE;

drop index if exists ENTRANCEHISTORY_PK;

drop table if exists ENTRANCEHISTORY CASCADE;

drop index if exists EXPARRANGEMENT_H_PK;

drop table if exists EXPARRANGEMENTH CASCADE;

drop index if exists MOVIE_PK;

drop table if exists MOVIE CASCADE;

drop index if exists ARRANGEMENT_IN_MOVIECHEQUE_FK;

drop index if exists MOVIECHEQUE_PK;

drop table if exists MOVIECHEQUE CASCADE;

drop index if exists OPER_IN_MCHEQUE_H_FK;

drop index if exists DEVICE_IN_MOVIECHEQUEHISTORY_FK;

drop index if exists EMP_IN_MCHEQUE_H_FK;

drop index if exists MOVIECHEQUEHISTORY_PK;

drop table if exists MOVIECHEQUEHISTORY CASCADE;

drop index if exists ARRAN_IN_MARRAN_FK;

drop index if exists MOVIE_IN_MOVIEINARRANGEMENT_FK;

drop index if exists MOVIEINARRANGEMENT_PK;

drop table if exists MOVIEINARRANGEMENT CASCADE;

drop index if exists OPERATION_PK;

drop table if exists OPERATION CASCADE;

drop index if exists PRODUCT_PK;

drop table if exists PRODUCT CASCADE;

drop index if exists ARRAN_IN_PARRAN_H_FK;

drop index if exists PRODUCTARRANGEMENTHISTORY_PK;

drop table if exists PRODUCTARRANGEMENTHISTORY CASCADE;

drop index if exists MCHEQUE_IN_PMCHEQUE_H_FK;

drop index if exists PRODUCTHISTORY_PK;

drop table if exists PRODUCTHISTORY CASCADE;

drop index if exists PRODUCT_IN_PARRAN_FK;

drop index if exists ARRAN_IN_PARRAN_FK;

drop index if exists PRODUCTINARRANGEMENT_PK;

drop table if exists PRODUCTINARRANGEMENT CASCADE;

drop index if exists PRODUCT_IN_PMCHEQUE_FK;

drop index if exists MCHEQUE_IN_PMCHEQUE_FK;

drop index if exists PRODUCTINMOVIECHEQUE_PK;

drop table if exists PRODUCTINMOVIECHEQUE CASCADE;

drop domain if exists AMOUNT;

drop domain if exists ARRANGEMENTID;

drop domain if exists BARCODE;

drop domain if exists BRANCHID;

drop domain if exists "DATE";

drop domain if exists DATE_MH;

drop domain if exists DEVICEID;

drop domain if exists DURATION;

drop domain if exists EMPLOYEEID;

drop domain if exists EMPLOYEEROLEID;

drop domain if exists ENTRANCE;

drop domain if exists EXPIRATIONDATE;

drop domain if exists E_MAIL;

drop domain if exists FIRSTNAME;

drop domain if exists HISTORYID;

drop domain if exists MOVIECHEQUEID;

drop domain if exists MOVIEID;

drop domain if exists "NAME";

drop domain if exists OPERATIONDESCRIPTION;

drop domain if exists OPERATIONID;

drop domain if exists "PASSWORD";

drop domain if exists PHONENO;

drop domain if exists PRICE;

drop domain if exists PRODUCTID;

drop domain if exists REMAININGAMOUNT;

drop domain if exists ROLEDESCRIPTION;

drop domain if exists ROLENAME;

drop domain if exists "STATUS";

drop domain if exists SURNAME;

/*==============================================================*/
/* Domain: AMOUNT                                               */
/*==============================================================*/
create domain AMOUNT as INT4;

/*==============================================================*/
/* Domain: ARRANGEMENTID                                        */
/*==============================================================*/
create domain ARRANGEMENTID as INT4;

/*==============================================================*/
/* Domain: BARCODE                                              */
/*==============================================================*/
create domain BARCODE as VARCHAR(65);

/*==============================================================*/
/* Domain: BRANCHID                                             */
/*==============================================================*/
create domain BRANCHID as INT4;

/*==============================================================*/
/* Domain: CINEMAID                                             */
/*==============================================================*/
create domain CINEMAID as INT4;

/*==============================================================*/
/* Domain: DATE                                                 */
/*==============================================================*/
create domain "DATE" as DATE;

/*==============================================================*/
/* Domain: DATE_MH                                              */
/*==============================================================*/
create domain DATE_MH as TIMESTAMP;

/*==============================================================*/
/* Domain: DEVICEID                                             */
/*==============================================================*/
create domain DEVICEID as INT4;

/*==============================================================*/
/* Domain: DURATION                                             */
/*==============================================================*/
create domain DURATION as INT4;

/*==============================================================*/
/* Domain: EMPLOYEEID                                           */
/*==============================================================*/
create domain EMPLOYEEID as INT4;

/*==============================================================*/
/* Domain: EMPLOYEEROLEID                                       */
/*==============================================================*/
create domain EMPLOYEEROLEID as INT4;

/*==============================================================*/
/* Domain: ENTRANCE                                             */
/*==============================================================*/
create domain ENTRANCE as INT4;

/*==============================================================*/
/* Domain: EXPIRATIONDATE                                       */
/*==============================================================*/
create domain EXPIRATIONDATE as DATE;

/*==============================================================*/
/* Domain: E_MAIL                                               */
/*==============================================================*/
create domain E_MAIL as VARCHAR(65);

/*==============================================================*/
/* Domain: FIRSTNAME                                            */
/*==============================================================*/
create domain FIRSTNAME as VARCHAR(65);

/*==============================================================*/
/* Domain: HISTORYID                                            */
/*==============================================================*/
create domain HISTORYID as INT8;

/*==============================================================*/
/* Domain: MOVIECHEQUEID                                        */
/*==============================================================*/
create domain MOVIECHEQUEID as INT4;

/*==============================================================*/
/* Domain: MOVIEID                                              */
/*==============================================================*/
create domain MOVIEID as INT4;

/*==============================================================*/
/* Domain: NAME                                                 */
/*==============================================================*/
create domain "NAME" as VARCHAR(65);

/*==============================================================*/
/* Domain: OPERATIONDESCRIPTION                                 */
/*==============================================================*/
create domain OPERATIONDESCRIPTION as VARCHAR(255);

/*==============================================================*/
/* Domain: OPERATIONID                                          */
/*==============================================================*/
create domain OPERATIONID as INT4;

/*==============================================================*/
/* Domain: PASSWORD                                             */
/*==============================================================*/
create domain "PASSWORD" as VARCHAR(65);

/*==============================================================*/
/* Domain: PHONENO                                              */
/*==============================================================*/
create domain PHONENO as INT4;

/*==============================================================*/
/* Domain: PRICE                                                */
/*==============================================================*/
create domain PRICE as MONEY;

/*==============================================================*/
/* Domain: PRODUCTID                                            */
/*==============================================================*/
create domain PRODUCTID as INT4;

/*==============================================================*/
/* Domain: REMAININGAMOUNT                                      */
/*==============================================================*/
create domain REMAININGAMOUNT as MONEY;

/*==============================================================*/
/* Domain: ROLEDESCRIPTION                                      */
/*==============================================================*/
create domain ROLEDESCRIPTION as VARCHAR(650);

/*==============================================================*/
/* Domain: ROLENAME                                             */
/*==============================================================*/
create domain ROLENAME as VARCHAR(65);

/*==============================================================*/
/* Domain: STATUS                                               */
/*==============================================================*/
create domain "STATUS" as BOOL;

/*==============================================================*/
/* Domain: SURNAME                                              */
/*==============================================================*/
create domain SURNAME as VARCHAR(65);

/*==============================================================*/
/* Table: AMOUNTHISTORY                                         */
/*==============================================================*/
create table AMOUNTHISTORY (
   HISTORYID_M          HISTORYID            not null,
   OLDREMAININGAMOUNT   REMAININGAMOUNT      null,
   NEWREMAININGAMOUNT   REMAININGAMOUNT      null,
   constraint PK_AMOUNTHISTORY primary key (HISTORYID_M)
);

/*==============================================================*/
/* Index: AMOUNTHISTORY_PK                                      */
/*==============================================================*/
create unique index AMOUNTHISTORY_PK on AMOUNTHISTORY (
HISTORYID_M
);

/*==============================================================*/
/* Table: ARRANGEMENT                                           */
/*==============================================================*/
create table ARRANGEMENT (
   ARRANGEMENTID        SERIAL	not null,
   "NAME"               "NAME"                 not null,
   PRICE                PRICE                not null,
   EXPIRATIONDATE       EXPIRATIONDATE       not null,
   constraint PK_ARRANGEMENT primary key (ARRANGEMENTID)
);

/*==============================================================*/
/* Index: ARRANGEMENT_PK                                        */
/*==============================================================*/
create unique index ARRANGEMENT_PK on ARRANGEMENT (
ARRANGEMENTID
);

/*==============================================================*/
/* Table: ARRANGEMENTHISTORY                                    */
/*==============================================================*/
create table ARRANGEMENTHISTORY (
   HISTORYID_A          SERIAL	not null,
   BRANCHID             BRANCHID             not null,
   DEVICEID             DEVICEID             not null,
   EMPLOYEEID           EMPLOYEEID           not null,
   OPERATIONID          OPERATIONID          not null,
   ARRANGEMENTID_AH     ARRANGEMENTID        not null,
   DATE_AH              DATE                 not null,
   constraint PK_ARRANGEMENTHISTORY primary key (HISTORYID_A)
);

/*==============================================================*/
/* Index: ARRANGEMENTHISTORY_PK                                 */
/*==============================================================*/
create unique index ARRANGEMENTHISTORY_PK on ARRANGEMENTHISTORY (
HISTORYID_A
);

/*==============================================================*/
/* Table: BRANCH_IN_CINEMA                                      */
/*==============================================================*/
create table BRANCH_IN_CINEMA (
   CINEMAID             CINEMAID             not null,
   BRANCHID             BRANCHID             not null,
   constraint PK_BRANCH_IN_CINEMA primary key (CINEMAID, BRANCHID)
);

/*==============================================================*/
/* Index: BRANCH_IN_CINEMA_PK                                   */
/*==============================================================*/
create unique index BRANCH_IN_CINEMA_PK on BRANCH_IN_CINEMA (
CINEMAID,
BRANCHID
);

/*==============================================================*/
/* Index: BRANCH_IN_CINEMA_FK                                   */
/*==============================================================*/
create  index BRANCH_IN_CINEMA_FK on BRANCH_IN_CINEMA (
CINEMAID
);

/*==============================================================*/
/* Index: BRANCH_IN_CINEMA2_FK                                  */
/*==============================================================*/
create  index BRANCH_IN_CINEMA2_FK on BRANCH_IN_CINEMA (
BRANCHID
);

/*==============================================================*/
/* Table: CINEMA                                                */
/*==============================================================*/
create table CINEMA (
   CINEMAID             CINEMAID             not null,
   NAME                 NAME                 not null,
   constraint PK_CINEMA primary key (CINEMAID)
);

/*==============================================================*/
/* Index: CINEMA_PK                                             */
/*==============================================================*/
create unique index CINEMA_PK on CINEMA (
CINEMAID
);

/*==============================================================*/
/* Index: DEVICE_IN_ARRANGEMENTHISTORY_FK                       */
/*==============================================================*/
create  index DEVICE_IN_ARRANGEMENTHISTORY_FK on ARRANGEMENTHISTORY (
BRANCHID,
DEVICEID
);

/*==============================================================*/
/* Index: EMP_IN_ARRANGEMENT_H_FK                               */
/*==============================================================*/
create  index EMP_IN_ARRANGEMENT_H_FK on ARRANGEMENTHISTORY (
EMPLOYEEID
);

/*==============================================================*/
/* Index: OPER_IN_ARRANGEMENT_H_FK                              */
/*==============================================================*/
create  index OPER_IN_ARRANGEMENT_H_FK on ARRANGEMENTHISTORY (
OPERATIONID
);

/*==============================================================*/
/* Table: BRANCH                                                */
/*==============================================================*/
create table BRANCH (
   BRANCHID             SERIAL	not null,
   "NAME"                 "NAME"                 not null,
   EMAIL                E_MAIL               not null,
   PHONENO              PHONENO              not null,
   constraint PK_BRANCH primary key (BRANCHID)
);

/*==============================================================*/
/* Index: BRANCH_PK                                             */
/*==============================================================*/
create unique index BRANCH_PK on BRANCH (
BRANCHID
);

/*==============================================================*/
/* Table: DEVICE                                                */
/*==============================================================*/
create table DEVICE (
   BRANCHID             BRANCHID	not null,
   DEVICEID             SERIAL             not null,
   "NAME"                 "NAME"                 not null,
   constraint PK_DEVICE primary key (BRANCHID, DEVICEID)
);

/*==============================================================*/
/* Index: DEVICE_PK                                             */
/*==============================================================*/
create unique index DEVICE_PK on DEVICE (
BRANCHID,
DEVICEID
);

/*==============================================================*/
/* Index: DEVICE_IN_BRANCH_FK                                   */
/*==============================================================*/
create  index DEVICE_IN_BRANCH_FK on DEVICE (
BRANCHID
);

/*==============================================================*/
/* Table: EMPLOYEE                                              */
/*==============================================================*/
create table EMPLOYEE (
   EMPLOYEEID           SERIAL	not null,
   EMPLOYEEROLEID       EMPLOYEEROLEID       not null,
   FIRSTNAME            FIRSTNAME            not null,
   SURNAME              SURNAME              not null,
   E_MAIL               E_MAIL               not null,
   "PASSWORD"             "PASSWORD"             not null,
   "STATUS"               "STATUS"               not null,
   constraint PK_EMPLOYEE primary key (EMPLOYEEID)
);

/*==============================================================*/
/* Index: EMPLOYEE_PK                                           */
/*==============================================================*/
create unique index EMPLOYEE_PK on EMPLOYEE (
EMPLOYEEID
);

/*==============================================================*/
/* Index: EMPLOYEEROLE_IN_EMPLOYEE_FK                           */
/*==============================================================*/
create  index EMPLOYEEROLE_IN_EMPLOYEE_FK on EMPLOYEE (
EMPLOYEEROLEID
);

/*==============================================================*/
/* Table: EMPLOYEEROLE                                          */
/*==============================================================*/
create table EMPLOYEEROLE (
   EMPLOYEEROLEID       SERIAL	not null,
   ROLENAME             ROLENAME             not null,
   ROLEDESCRIPTION      ROLEDESCRIPTION      not null,
   constraint PK_EMPLOYEEROLE primary key (EMPLOYEEROLEID)
);

/*==============================================================*/
/* Index: EMPLOYEEROLE_PK                                       */
/*==============================================================*/
create unique index EMPLOYEEROLE_PK on EMPLOYEEROLE (
EMPLOYEEROLEID
);

/*==============================================================*/
/* Table: ENTRANCEHISTORY                                       */
/*==============================================================*/
create table ENTRANCEHISTORY (
   HISTORYID_M          HISTORYID            not null,
   OLDENTRANCE          ENTRANCE             null,
   NEWENTRANCE          ENTRANCE             not null,
   constraint PK_ENTRANCEHISTORY primary key (HISTORYID_M)
);

/*==============================================================*/
/* Index: ENTRANCEHISTORY_PK                                    */
/*==============================================================*/
create unique index ENTRANCEHISTORY_PK on ENTRANCEHISTORY (
HISTORYID_M
);

/*==============================================================*/
/* Table: EXPARRANGEMENTH                                       */
/*==============================================================*/
create table EXPARRANGEMENTH (
   HISTORYID_A          HISTORYID            not null,
   OLDEXPIRATIONDATE_A  EXPIRATIONDATE       null,
   NEWEXPIRATIONDATE_A  EXPIRATIONDATE       not null,
   constraint PK_EXPARRANGEMENTH primary key (HISTORYID_A)
);

/*==============================================================*/
/* Index: EXPARRANGEMENT_H_PK                                   */
/*==============================================================*/
create unique index EXPARRANGEMENT_H_PK on EXPARRANGEMENTH (
HISTORYID_A
);

/*==============================================================*/
/* Table: MOVIE                                                 */
/*==============================================================*/
create table MOVIE (
   MOVIEID              SERIAL	not null,
   "NAME"                 "NAME"                 not null,
   DURATION             DURATION             not null,
   constraint PK_MOVIE primary key (MOVIEID)
);

/*==============================================================*/
/* Index: MOVIE_PK                                              */
/*==============================================================*/
create unique index MOVIE_PK on MOVIE (
MOVIEID
);

/*==============================================================*/
/* Table: MOVIECHEQUE                                           */
/*==============================================================*/
create table MOVIECHEQUE (
   MOVIECHEQUEID        SERIAL	not null,
   ARRANGEMENTID        ARRANGEMENTID        null,
   ENTRANCE             ENTRANCE             not null,
   REMAININGAMOUNT      REMAININGAMOUNT      not null,
   BARCODE              BARCODE              not null,
   EXPIRATIONDATE       EXPIRATIONDATE       not null,
   "STATUS"             "STATUS"             not null,
   constraint PK_MOVIECHEQUE primary key (MOVIECHEQUEID)
);

/*==============================================================*/
/* Index: MOVIECHEQUE_PK                                        */
/*==============================================================*/
create unique index MOVIECHEQUE_PK on MOVIECHEQUE (
MOVIECHEQUEID
);

/*==============================================================*/
/* Index: ARRANGEMENT_IN_MOVIECHEQUE_FK                         */
/*==============================================================*/
create  index ARRANGEMENT_IN_MOVIECHEQUE_FK on MOVIECHEQUE (
ARRANGEMENTID
);

/*==============================================================*/
/* Table: MOVIECHEQUEHISTORY                                    */
/*==============================================================*/
create table MOVIECHEQUEHISTORY (
   HISTORYID_M          SERIAL	not null,
   EMPLOYEEID           EMPLOYEEID           not null,
   BRANCHID             BRANCHID             not null,
   DEVICEID             DEVICEID             not null,
   OPERATIONID          OPERATIONID          not null,
   MOVIECHEQUEID_MH     MOVIECHEQUEID        not null,
   DATE_MH              DATE_MH              not null,
   STATUS_MH            "STATUS"               not null,
   constraint PK_MOVIECHEQUEHISTORY primary key (HISTORYID_M)
);

/*==============================================================*/
/* Index: MOVIECHEQUEHISTORY_PK                                 */
/*==============================================================*/
create unique index MOVIECHEQUEHISTORY_PK on MOVIECHEQUEHISTORY (
HISTORYID_M
);

/*==============================================================*/
/* Index: EMP_IN_MCHEQUE_H_FK                                   */
/*==============================================================*/
create  index EMP_IN_MCHEQUE_H_FK on MOVIECHEQUEHISTORY (
EMPLOYEEID
);

/*==============================================================*/
/* Index: DEVICE_IN_MOVIECHEQUEHISTORY_FK                       */
/*==============================================================*/
create  index DEVICE_IN_MOVIECHEQUEHISTORY_FK on MOVIECHEQUEHISTORY (
BRANCHID,
DEVICEID
);

/*==============================================================*/
/* Index: OPER_IN_MCHEQUE_H_FK                                  */
/*==============================================================*/
create  index OPER_IN_MCHEQUE_H_FK on MOVIECHEQUEHISTORY (
OPERATIONID
);

/*==============================================================*/
/* Table: MOVIEINARRANGEMENT                                    */
/*==============================================================*/
create table MOVIEINARRANGEMENT (
   MOVIEID              MOVIEID              not null,
   ARRANGEMENTID        ARRANGEMENTID        not null,
   AMOUNT               AMOUNT               not null,
   constraint PK_MOVIEINARRANGEMENT primary key (MOVIEID, ARRANGEMENTID)
);

/*==============================================================*/
/* Index: MOVIEINARRANGEMENT_PK                                 */
/*==============================================================*/
create unique index MOVIEINARRANGEMENT_PK on MOVIEINARRANGEMENT (
MOVIEID,
ARRANGEMENTID
);

/*==============================================================*/
/* Index: MOVIE_IN_MOVIEINARRANGEMENT_FK                        */
/*==============================================================*/
create  index MOVIE_IN_MOVIEINARRANGEMENT_FK on MOVIEINARRANGEMENT (
MOVIEID
);

/*==============================================================*/
/* Index: ARRAN_IN_MARRAN_FK                                    */
/*==============================================================*/
create  index ARRAN_IN_MARRAN_FK on MOVIEINARRANGEMENT (
ARRANGEMENTID
);

/*==============================================================*/
/* Table: OPERATION                                             */
/*==============================================================*/
create table OPERATION (
   OPERATIONID          SERIAL	not null,
   OPERATIONDESCRIPTION OPERATIONDESCRIPTION not null,
   constraint PK_OPERATION primary key (OPERATIONID)
);

/*==============================================================*/
/* Index: OPERATION_PK                                          */
/*==============================================================*/
create unique index OPERATION_PK on OPERATION (
OPERATIONID
);

/*==============================================================*/
/* Table: PRODUCT                                               */
/*==============================================================*/
create table PRODUCT (
   PRODUCTID            SERIAL	not null,
   "NAME"                 "NAME"                 not null,
   PRICE                PRICE                not null,
   constraint PK_PRODUCT primary key (PRODUCTID)
);

/*==============================================================*/
/* Index: PRODUCT_PK                                            */
/*==============================================================*/
create unique index PRODUCT_PK on PRODUCT (
PRODUCTID
);

/*==============================================================*/
/* Table: PRODUCTARRANGEMENTHISTORY                             */
/*==============================================================*/
create table PRODUCTARRANGEMENTHISTORY (
   HISTORYID_A          HISTORYID            not null,
   PRODUCTHISTORYID_A   PRODUCTID            not null,
   OLDAMOUNT_A          AMOUNT               null,
   NEWAMOUNT_A          AMOUNT               not null,
   constraint PK_PRODUCTARRANGEMENTHISTORY primary key (HISTORYID_A, PRODUCTHISTORYID_A)
);

/*==============================================================*/
/* Index: PRODUCTARRANGEMENTHISTORY_PK                          */
/*==============================================================*/
create unique index PRODUCTARRANGEMENTHISTORY_PK on PRODUCTARRANGEMENTHISTORY (
HISTORYID_A,
PRODUCTHISTORYID_A
);

/*==============================================================*/
/* Index: ARRAN_IN_PARRAN_H_FK                                  */
/*==============================================================*/
create  index ARRAN_IN_PARRAN_H_FK on PRODUCTARRANGEMENTHISTORY (
HISTORYID_A
);

/*==============================================================*/
/* Table: PRODUCTHISTORY                                        */
/*==============================================================*/
create table PRODUCTHISTORY (
   HISTORYID_M          HISTORYID            not null,
   PRODUCTHISTORYID     PRODUCTID            not null,
   OLDAMOUNT            AMOUNT               null,
   NEWAMOUNT            AMOUNT               not null,
   constraint PK_PRODUCTHISTORY primary key (HISTORYID_M, PRODUCTHISTORYID)
);

/*==============================================================*/
/* Index: PRODUCTHISTORY_PK                                     */
/*==============================================================*/
create unique index PRODUCTHISTORY_PK on PRODUCTHISTORY (
HISTORYID_M,
PRODUCTHISTORYID
);

/*==============================================================*/
/* Index: MCHEQUE_IN_PMCHEQUE_H_FK                              */
/*==============================================================*/
create  index MCHEQUE_IN_PMCHEQUE_H_FK on PRODUCTHISTORY (
HISTORYID_M
);

/*==============================================================*/
/* Table: PRODUCTINARRANGEMENT                                  */
/*==============================================================*/
create table PRODUCTINARRANGEMENT (
   ARRANGEMENTID        ARRANGEMENTID        not null,
   PRODUCTID            PRODUCTID            not null,
   AMOUNT               AMOUNT               not null,
   constraint PK_PRODUCTINARRANGEMENT primary key (ARRANGEMENTID, PRODUCTID)
);

/*==============================================================*/
/* Index: PRODUCTINARRANGEMENT_PK                               */
/*==============================================================*/
create unique index PRODUCTINARRANGEMENT_PK on PRODUCTINARRANGEMENT (
ARRANGEMENTID,
PRODUCTID
);

/*==============================================================*/
/* Index: ARRAN_IN_PARRAN_FK                                    */
/*==============================================================*/
create  index ARRAN_IN_PARRAN_FK on PRODUCTINARRANGEMENT (
ARRANGEMENTID
);

/*==============================================================*/
/* Index: PRODUCT_IN_PARRAN_FK                                  */
/*==============================================================*/
create  index PRODUCT_IN_PARRAN_FK on PRODUCTINARRANGEMENT (
PRODUCTID
);

/*==============================================================*/
/* Table: PRODUCTINMOVIECHEQUE                                  */
/*==============================================================*/
create table PRODUCTINMOVIECHEQUE (
   MOVIECHEQUEID        MOVIECHEQUEID        not null,
   PRODUCTID            PRODUCTID            not null,
   AMOUNT               AMOUNT               not null,
   constraint PK_PRODUCTINMOVIECHEQUE primary key (MOVIECHEQUEID, PRODUCTID)
);

/*==============================================================*/
/* Index: PRODUCTINMOVIECHEQUE_PK                               */
/*==============================================================*/
create unique index PRODUCTINMOVIECHEQUE_PK on PRODUCTINMOVIECHEQUE (
MOVIECHEQUEID,
PRODUCTID
);

/*==============================================================*/
/* Index: MCHEQUE_IN_PMCHEQUE_FK                                */
/*==============================================================*/
create  index MCHEQUE_IN_PMCHEQUE_FK on PRODUCTINMOVIECHEQUE (
MOVIECHEQUEID
);

/*==============================================================*/
/* Index: PRODUCT_IN_PMCHEQUE_FK                                */
/*==============================================================*/
create  index PRODUCT_IN_PMCHEQUE_FK on PRODUCTINMOVIECHEQUE (
PRODUCTID
);

alter table AMOUNTHISTORY
   add constraint FK_AMOUNTHI_MOVIECHEQ_MOVIECHE foreign key (HISTORYID_M)
      references MOVIECHEQUEHISTORY (HISTORYID_M)
      on delete restrict on update restrict;

alter table ARRANGEMENTHISTORY
   add constraint FK_ARRANGEM_DEVICE_IN_DEVICE foreign key (BRANCHID, DEVICEID)
      references DEVICE (BRANCHID, DEVICEID)
      on delete restrict on update restrict;

alter table ARRANGEMENTHISTORY
   add constraint FK_ARRANGEM_EMPLOYEE__EMPLOYEE foreign key (EMPLOYEEID)
      references EMPLOYEE (EMPLOYEEID)
      on delete restrict on update restrict;

alter table ARRANGEMENTHISTORY
   add constraint FK_ARRANGEM_OPERATION_OPERATIO foreign key (OPERATIONID)
      references OPERATION (OPERATIONID)
      on delete restrict on update restrict;

alter table BRANCH_IN_CINEMA
   add constraint FK_BRANCH_I_BRANCH_IN_CINEMA foreign key (CINEMAID)
      references CINEMA (CINEMAID)
      on delete restrict on update restrict;

alter table BRANCH_IN_CINEMA
   add constraint FK_BRANCH_I_BRANCH_IN_BRANCH foreign key (BRANCHID)
      references BRANCH (BRANCHID)
      on delete restrict on update restrict;

alter table DEVICE
   add constraint FK_DEVICE_DEVICE_IN_BRANCH foreign key (BRANCHID)
      references BRANCH (BRANCHID)
      on delete restrict on update restrict;

alter table EMPLOYEE
   add constraint FK_EMPLOYEE_EMPLOYEER_EMPLOYEE foreign key (EMPLOYEEROLEID)
      references EMPLOYEEROLE (EMPLOYEEROLEID)
      on delete restrict on update restrict;

alter table ENTRANCEHISTORY
   add constraint FK_ENTRANCE_ENTRANCEH_MOVIECHE foreign key (HISTORYID_M)
      references MOVIECHEQUEHISTORY (HISTORYID_M)
      on delete restrict on update restrict;

alter table EXPARRANGEMENTH
   add constraint FK_EXPARRAN_ARRANGEME_ARRANGEM foreign key (HISTORYID_A)
      references ARRANGEMENTHISTORY (HISTORYID_A)
      on delete restrict on update restrict;

alter table MOVIECHEQUE
   add constraint FK_MOVIECHE_ARRANGEME_ARRANGEM foreign key (ARRANGEMENTID)
      references ARRANGEMENT (ARRANGEMENTID)
      on delete restrict on update restrict;

alter table MOVIECHEQUEHISTORY
   add constraint FK_MOVIECHE_DEVICE_IN_DEVICE foreign key (BRANCHID, DEVICEID)
      references DEVICE (BRANCHID, DEVICEID)
      on delete restrict on update restrict;

alter table MOVIECHEQUEHISTORY
   add constraint FK_MOVIECHE_EMPLOYEE__EMPLOYEE foreign key (EMPLOYEEID)
      references EMPLOYEE (EMPLOYEEID)
      on delete restrict on update restrict;

alter table MOVIECHEQUEHISTORY
   add constraint FK_MOVIECHE_OPERATION_OPERATIO foreign key (OPERATIONID)
      references OPERATION (OPERATIONID)
      on delete restrict on update restrict;

alter table MOVIEINARRANGEMENT
   add constraint FK_MOVIEINA_ARRANGEME_ARRANGEM foreign key (ARRANGEMENTID)
      references ARRANGEMENT (ARRANGEMENTID)
      on delete restrict on update restrict;

alter table MOVIEINARRANGEMENT
   add constraint FK_MOVIEINA_MOVIE_IN__MOVIE foreign key (MOVIEID)
      references MOVIE (MOVIEID)
      on delete restrict on update restrict;

alter table PRODUCTARRANGEMENTHISTORY
   add constraint FK_PRODUCTA_ARRANGEME_ARRANGEM foreign key (HISTORYID_A)
      references ARRANGEMENTHISTORY (HISTORYID_A)
      on delete restrict on update restrict;

alter table PRODUCTHISTORY
   add constraint FK_PRODUCTH_MOVIECHEQ_MOVIECHE foreign key (HISTORYID_M)
      references MOVIECHEQUEHISTORY (HISTORYID_M)
      on delete restrict on update restrict;

alter table PRODUCTINARRANGEMENT
   add constraint FK_PRODUCTI_ARRAGEMEN_ARRANGEM foreign key (ARRANGEMENTID)
      references ARRANGEMENT (ARRANGEMENTID)
      on delete restrict on update restrict;

alter table PRODUCTINARRANGEMENT
   add constraint FK_PRODUCTI_PRODUCT_I_PRODUCT foreign key (PRODUCTID)
      references PRODUCT (PRODUCTID)
      on delete restrict on update restrict;

alter table PRODUCTINMOVIECHEQUE
   add constraint FK_PRODUCTI_MOVIECHEQ_MOVIECHE foreign key (MOVIECHEQUEID)
      references MOVIECHEQUE (MOVIECHEQUEID)
      on delete restrict on update restrict;

alter table PRODUCTINMOVIECHEQUE
   add constraint FK_PRODUCTI_PRODUCT_I_PRODUCT foreign key (PRODUCTID)
      references PRODUCT (PRODUCTID)
      on delete restrict on update restrict;

/*==============================================================*/
/* Hard coded data: Operation					                */
/*==============================================================*/
INSERT INTO Operation(operationDescription) VALUES('Arrangement updated');
INSERT INTO Operation(operationDescription) VALUES('Movie cheque created');
INSERT INTO Operation(operationDescription) VALUES('Arrangement added');
INSERT INTO Operation(operationDescription) VALUES('Movie cheque is blocked');
INSERT INTO Operation(operationDescription) VALUES('Changed the amount of the product');
INSERT INTO Operation(operationDescription) VALUES('Movie cheque added to history');
INSERT INTO Operation(operationDescription) VALUES('Transaction made for movie entrances');
INSERT INTO Operation(operationDescription) VALUES('Transaction made for product purchases');
INSERT INTO Operation(operationDescription) VALUES('Entrances amount of the movie added to history');

/*==============================================================*/
/* Hard coded data: EmployeeRole				                */
/*==============================================================*/
INSERT INTO EmployeeRole(rolename, roledescription) VALUES('Marketingafdeling', 'Marketingafdeling');
INSERT INTO EmployeeRole(rolename, roledescription) VALUES('Financiele administratie', 'Financiële administratie');
INSERT INTO EmployeeRole(rolename, roledescription) VALUES('Medewerker', 'Medewerker');

/*==============================================================*/
/* Global function: Generate Barcode                            */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_create_barcode(employeeID INT)
RETURNS VARCHAR(65) AS $BODY$
BEGIN
 	/* MD5HASH(timestamp+employeeID)+timestamp+employeeID */
    RETURN concat(md5(concat((SELECT EXTRACT(EPOCH FROM localtimestamp) * 100000)::VARCHAR(65), employeeID::INT  * trunc(random()* 100000000000 + 1)))::VARCHAR(65), concat((SELECT EXTRACT(EPOCH FROM localtimestamp) * 100000)::VARCHAR(65), employeeID::INT)::VARCHAR(65));
END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Global function: Get employee role by employee ID            */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_get_employee_role(_employeeID INT)
RETURNS VARCHAR(65) AS $BODY$
DECLARE rolename VARCHAR(65);
BEGIN
    SELECT er.roleName INTO rolename
    FROM Employee e INNER JOIN employeeRole er ON e.employeeRoleID = er.employeeRoleID
    WHERE e.employeeID = _employeeID;

    RETURN rolename;

    EXCEPTION WHEN OTHERS THEN
    RAISE NOTICE 'Error occured, the employee does not exist or does not have any role.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;
    RETURN NULL;
END;
$BODY$ LANGUAGE plpgsql;
