/*====================================================================*/
/* Business rule name:      Business Rule 1 TESTS                     */
/* Author:                   Tom van Grinsven                         */
/* Function:           Inserts an invalid price for arrangement       */
/*                       succeeds when the arrangement is not inserted*/
/*====================================================================*/
CREATE OR REPLACE FUNCTION fc_br_1_test_1()
RETURNS BOOLEAN AS $BODY$
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Ladies Night 25-7', -1::MONEY, TO_DATE('01-01-2020', 'DD-MM-YYYY'));
  RETURN FALSE;

  EXCEPTION WHEN OTHERS THEN
      RETURN TRUE;
  END;
$BODY$ LANGUAGE plpgsql;

/*====================================================================*/
/* Business rule name:      Business Rule 1 TESTS                     */
/* Author:              Tom van Grinsven                              */
/* Function:            Inserts a valid price for arrangement succeeds*/
/*                        when the arrangement is not inserted        */
/*                        price domain can not be below zero          */
/*====================================================================*/
CREATE OR REPLACE FUNCTION fc_br_1_test_2()
RETURNS BOOLEAN AS $BODY$
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Ladies Night 25-7', 10::MONEY, TO_DATE('01-01-2020', 'DD-MM-YYYY'));
  RETURN TRUE;

  EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 1 TESTS               */
/* Author:                  Tom van Grinsven                    */
/* Function:        Inserts an invalid price for a product      */
/*                  succeeds when the product is not inserted   */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_br_1_test_3()
RETURNS BOOLEAN AS $BODY$
BEGIN
  INSERT INTO Product ("NAME", price) VALUES ('Heineken 0.33CL', -1::MONEY);

  RETURN FALSE;

  EXCEPTION WHEN OTHERS THEN
      RETURN TRUE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 1 TESTS               */
/* Author:                    Tom van Grinsven                  */
/* Function:            nserts a valid price for a product      */
/*                      succeeds when the product is inserted   */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_br_1_test_4()
RETURNS BOOLEAN AS $BODY$
BEGIN
  INSERT INTO Product ("NAME", price) VALUES ('Heineken 0.33CL', 2.5::MONEY);

  RETURN TRUE;

  EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 2 TESTS               */
/* Author:                  Tom van Grinsven                    */
/* Function:        Inserts a moviecheque with an invalid       */
/*      remaining amount. Succeeds when the moviecheque is not  */
/*                           inserted.                          */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_br_2_test_1() RETURNS BOOLEAN
AS $BODY$
BEGIN
  INSERT INTO MovieCheque (entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (2, -1::MONEY, (SELECT fc_create_barcode(1)), TO_DATE('01-01-2020', 'DD-MM-YYYY'), TRUE);
    EXCEPTION WHEN OTHERS THEN
      RETURN TRUE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 2 TESTS               */
/* Author:                    Tom van Grinsven                  */
/* Function:           Inserts a moviecheque with a valid       */
/*      remaining amount. Succeeds when the moviecheque is      */
/*                           inserted.                          */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_br_2_test_2() RETURNS BOOLEAN
AS $BODY$
BEGIN
  INSERT INTO MovieCheque (entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (2, 10::MONEY, (SELECT fc_create_barcode(1)), TO_DATE('01-01-2020', 'DD-MM-YYYY'), TRUE);
      RETURN TRUE;
    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 3 - TESTS             */
/* Author:                    Tom van Grinsven                  */
/* Function:     Tests IF a moviecheque with an invalid         */
/* expirationDate is not updated. Succeeds when the update      */
/*                    does not happen                           */
/*==============================================================*/
-- Function that tests IF the updated MovieCheque with invalid expiration DATE is not updated in the database
CREATE OR REPLACE FUNCTION fc_br_3_test_1()
RETURNS BOOLEAN AS $BODY$
DECLARE _MovieChequeID INT;
BEGIN
  INSERT INTO MovieCheque (entrance, barcode, expirationDate, "STATUS") VALUES (1, (SELECT fc_create_barcode(1)), TO_DATE('01-01-2002', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _MovieChequeID;

  UPDATE MovieCheque SET entrance = 0 WHERE movieChequeID = _MovieChequeID;

  RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN TRUE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 3 - TESTS             */
/* Author:                    Tom van Grinsven                  */
/* Function:     Tests IF a moviecheque with a valid            */
/* expirationDate is updated. Succeeds when the update          */
/*                        does happen                           */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_br_3_test_2()
RETURNS BOOLEAN AS $BODY$
DECLARE _MovieChequeID INT;
BEGIN
  INSERT INTO MovieCheque (entrance, barcode, expirationDate, "STATUS") VALUES (1, (SELECT fc_create_barcode(2)), TO_DATE('01-01-2099', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _MovieChequeID;

  UPDATE MovieCheque SET entrance = 0 WHERE movieChequeID = _MovieChequeID;
      RETURN TRUE;

  EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 3 - TESTS             */
/* Author:            Tom van Grinsven                          */
/* Function:          Tests IF a moviecheque with a (valid)     */
/* expirationDate of today is not updated. Succeeds when the    */
/*                      update does happen                      */
/*==============================================================*/
-- Function that tests IF the updated MovieCheque with the expirationDate of today is succesfully updated in the database.
CREATE OR REPLACE FUNCTION fc_br_3_test_3()
RETURNS BOOLEAN AS $BODY$
DECLARE _MovieChequeID INT;
BEGIN
  INSERT INTO MovieCheque (entrance, barcode, expirationDate, "STATUS") VALUES (1, (SELECT fc_create_barcode(3)), CURRENT_DATE, TRUE) RETURNING movieChequeID INTO _MovieChequeID;

  UPDATE MovieCheque SET entrance = 0 WHERE movieChequeID = _MovieChequeID;
      RETURN TRUE;

  EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 6 - TESTS             */
/* Author:                  Tom van Grinsven                    */
/* Function:                Tests for business rule 6           */
/* Inserts a duplicate barcode. Succeeds when the barcodes are  */
/*                                    not inserted              */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_br_6_test_1() RETURNS BOOLEAN
AS $BODY$
BEGIN
  INSERT INTO MovieCheque (entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (1, 1, 'barcode', TO_DATE('01-01-2020', 'DD-MM-YYYY'), TRUE);
    INSERT INTO MovieCheque (entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (1, 1, 'barcode', TO_DATE('01-01-2020', 'DD-MM-YYYY'), TRUE);
    EXCEPTION WHEN OTHERS THEN
      RETURN TRUE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 6 - TESTS             */
/* Author:                  Tom van Grinsven                    */
/* Function:                Tests for business rule 6           */
/* Inserts two unique barcodes. Succeeds when the barcodes are  */
/* inserted                                                     */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_br_6_test_2() RETURNS BOOLEAN
AS $BODY$
BEGIN
  INSERT INTO MovieCheque (entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (1, 1, (SELECT fc_create_barcode(1)), TO_DATE('01-01-2020', 'DD-MM-YYYY'), TRUE);
    INSERT INTO MovieCheque (entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (1, 1, (SELECT fc_create_barcode(2)), TO_DATE('01-01-2020', 'DD-MM-YYYY'), TRUE);
      RETURN TRUE;
    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 13  TESTS             */
/* Author:                  Tom van Grinsven                    */
/* Function:     Tests written to test Business Rule 13         */
/* This test updates the expirationDate of an arrangement.      */
/* Succeeds when the expirationDate of a moviecheque linked to  */
/* that arrangement is also updated                             */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_br_13_test_1()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('testarrangement', 10, TO_DATE('01-01-2020', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID, 1, 1, (SELECT fc_create_barcode(1)), TO_DATE('01-01-2020', 'DD-MM-YYYY'), TRUE);

  UPDATE Arrangement SET expirationDate = TO_DATE('12-12-2020', 'DD-MM-YYYY') WHERE arrangementID = _arrangementID;

  IF EXISTS(SELECT 1 FROM MovieCheque WHERE arrangementID = _arrangementID AND expirationDate = TO_DATE('12-12-2020', 'DD-MM-YYYY')) THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;

  EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 13  TESTS             */
/* Author:                  Tom van Grinsven                    */
/* Function:     Tests written to test Business Rule 13         */
/*    This test updates the expirationDate of an arrangement.   */
/*    Succeeds when the expirationDate of the                   */
/*  two moviecheque linked to that arrangement are also updated */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_br_13_test_2()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('testarrangement2', 12, TO_DATE('01-01-2020', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID, 1, 1, (SELECT fc_create_barcode(2)), TO_DATE('01-01-2020', 'DD-MM-YYYY'), TRUE);
    INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID, 1, 1, (SELECT fc_create_barcode(3)), TO_DATE('01-01-2020', 'DD-MM-YYYY'), TRUE);

  UPDATE Arrangement SET expirationDate = TO_DATE('12-12-2020', 'DD-MM-YYYY') WHERE arrangementID = _arrangementID;

  IF ((SELECT COUNT (*) FROM MovieCheque WHERE arrangementID = _arrangementID) = 2) THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;

  EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 15 TESTS              */
/* Author:                  Tom van Grinsven                    */
/* Function:                Tests for business rule 15          */
/* Tests IF when a moviecheque is inserted without specified    */
/* remaining amount, the remaining amount is set to 0           */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_br_15_test_1() RETURNS BOOLEAN
AS $BODY$
DECLARE _movieChequeID INT;
BEGIN
  INSERT INTO MovieCheque (entrance, barcode, expirationDate, "STATUS") VALUES (1, (SELECT fc_create_barcode(1)), TO_DATE('01-01-2020', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _movieChequeID;
    IF EXISTS(SELECT 1 FROM MovieCheque WHERE movieChequeID = _movieChequeID AND remainingAmount = 0::MONEY) THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 16 TESTS              */
/* Author:                  Tom van Grinsven                    */
/* Function:                Tests for business rule 16          */
/* Tests IF when a moviecheque is inserted with no specified    */
/* number of entrances, the number ofentrances are set to zero  */
/*==============================================================*/
-- Function that inserts a new moviecheque with out specifying the amount of entrances. Succeeds when the entrances of that moviecheque is zero.
CREATE OR REPLACE FUNCTION fc_br_16_test_1() RETURNS BOOLEAN
AS $BODY$
DECLARE _movieChequeID INT;
BEGIN
  INSERT INTO MovieCheque (remainingAmount, barcode, expirationDate, "STATUS") VALUES (1, (SELECT fc_create_barcode(1)), TO_DATE('01-01-2020', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _movieChequeID;
    IF EXISTS(SELECT 1 FROM MovieCheque WHERE movieChequeID = _movieChequeID AND entrance = 0) THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 18 TESTS              */
/* Author:                  Tom van Grinsven                    */
/* Function:                Tests for business rule 18          */
/* Tests IF when a moviecheque is inserted OR updated, the DATE */
/* of inserting OR updating can not be after the expiration.    */
/* Succeeds when the moviecheque is successfully inserted.      */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_br_18_test_1() RETURNS BOOLEAN
AS $BODY$
DECLARE _movieChequeID INT;
BEGIN
  INSERT INTO MovieCheque (entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES ( 0, 1, (SELECT fc_create_barcode(1)), TO_DATE('05-12-2040','DD-MM-YYYY'), TRUE) RETURNING moviechequeid INTO _movieChequeID;
  IF EXISTS(SELECT 1 FROM MovieCheque WHERE movieChequeID = _movieChequeID) THEN
    RETURN TRUE;
    END IF;
    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 18 TESTS              */
/* Author:                Tom van Grinsven                      */
/* Function:              Tests for business rule 18            */
/* Tests IF when a moviecheque is inserted OR updated, the DATE */
/* of inserting OR updating can not be after the expiration.    */
/* Succeeds when the moviecheque is not inserted because the    */
/* expirationDate is set before the current DATE.               */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_br_18_test_2() RETURNS BOOLEAN
AS $BODY$
DECLARE _movieChequeID INT;
BEGIN
  INSERT INTO MovieCheque (entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (0, 1, (SELECT fc_create_barcode(2)), TO_DATE('05-12-2010','DD-MM-YYYY'), TRUE) RETURNING moviechequeid INTO _movieChequeID;
  IF EXISTS(SELECT 1 FROM MovieCheque WHERE movieChequeID = _movieChequeID) THEN
    RETURN FALSE;
    END IF;
    EXCEPTION WHEN OTHERS THEN
      RETURN TRUE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 18 TESTS              */
/* Author:            Tom van Grinsven                          */
/* Function:          Tests for business rule 18                */
/* Tests IF when a moviecheque is inserted OR updated, the DATE */
/* of inserting OR updating can not be after the expiration.    */
/* Succeeds when the moviecheque is updated because the         */
/* expirationDate is set after the current DATE.                */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_br_18_test_3() RETURNS BOOLEAN
AS $BODY$
DECLARE _movieChequeID INT;
BEGIN
  INSERT INTO MovieCheque (entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (0, 1, (SELECT fc_create_barcode(3)), TO_DATE('05-12-2030','DD-MM-YYYY'), TRUE) RETURNING moviechequeid INTO _movieChequeID;
  UPDATE MovieCheque SET expirationDate = TO_DATE('05-12-2029','DD-MM-YYYY') WHERE movieChequeID = _movieChequeID;
    IF EXISTS(SELECT 1 FROM MovieCheque WHERE movieChequeID = _movieChequeID AND expirationDate = TO_DATE('05-12-2029','DD-MM-YYYY')) THEN
    RETURN TRUE;
     ELSE
       RETURN FALSE;
    END IF;
    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Business rule name:      Business Rule 18 TESTS              */
/* Author:                  Tom van Grinsven                    */
/* Function:                Tests for business rule 18          */
/* Tests IF when a moviecheque is inserted OR updated, the DATE */
/* of inserting OR updating can not be after the expiration.    */
/* Succeeds when the moviecheque is not updated because the     */
/* expirationDate is set before the current DATE.               */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_br_18_test_4() RETURNS BOOLEAN
AS $BODY$
DECLARE _movieChequeID INT;
BEGIN
  INSERT INTO MovieCheque (entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (0, 1, (SELECT fc_create_barcode(4)), TO_DATE('05-12-2030','DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _movieChequeID;
  UPDATE MovieCheque SET expirationDate = TO_DATE('05-12-2010','DD-MM-YYYY') WHERE movieChequeID = _movieChequeID;
    IF EXISTS(SELECT 1 FROM MovieCheque WHERE movieChequeid = _movieChequeID AND expirationDate = TO_DATE('05-12-2010','DD-MM-YYYY')) THEN
    RETURN FALSE;
    END IF;
    EXCEPTION WHEN OTHERS THEN
      RETURN TRUE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 1 Test 1                                                 */
/* Function:  Function tests the add arrangement functionality                  */
/*        This tests adds an arrangement without movies OR products.            */
/*        Succeeds IF an error is raised AND the arrangement is not added.      */
/* Author:    Kevin Dorlas                                                      */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_1_test_1()
RETURNS BOOLEAN AS $BODY$
DECLARE _employeeID INT;
    _branchID INT;
    _deviceID INT;
    _name VARCHAR(65);
    _price MONEY;
    _expDate DATE;
    _amount INT;
    _movieIDs INT[];
    _products INT[][];
    _arrangementID INT;
BEGIN
  _name := 'Arr_1';
  _price := 99;
  _expDate := CURRENT_TIMESTAMP;
  _amount := 1;
  _movieIDs := '{}';
  _products := '{}';
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;

    /* Arrangement INSERT Only */
    SELECT fc_add_arrangement(_branchID, _deviceID, _employeeID, _name, _price::MONEY, _expDate, _movieIDs::INT[], _amount, _products::INT[][]) INTO _arrangementID;

  IF EXISTS (SELECT 1 FROM Arrangement WHERE arrangementID = _arrangementID) THEN
      RETURN FALSE;
    END IF;

    RETURN TRUE;

     EXCEPTION WHEN OTHERS THEN
       RETURN TRUE;
END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 1 Test 2                                                 */
/* Function:  Function to test the add arrangement functionality                */
/*              Succeeds when a new arrangement is added including a movie      */
/* Author:    Kevin Dorlas                                                      */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_1_test_2()
RETURNS BOOLEAN AS $BODY$
DECLARE _employeeID INT;
    _branchID INT;
    _deviceID INT;
    _movieID INT;
    _name VARCHAR(65);
    _price MONEY;
    _expDate DATE;
    _amount INT;
    _movieIDs INT[];
    _products INT[][];
    _arrangementID INT;
BEGIN
  _name := 'Arr_1';
  _price := 99;
  _expDate := CURRENT_TIMESTAMP;
  _amount := 1;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
   INSERT INTO Movie ("NAME", duration) VALUES ('Movie1', 100) RETURNING movieID INTO _movieID;
    _movieIDs := ARRAY[_movieID];
    _products := '{}';
    /* Arrangement & MovieInArrangement INSERT Only */
  SELECT fc_add_arrangement(_branchID, _deviceID, _employeeID, _name, _price::MONEY, _expDate, _movieIDs::INT[], _amount, _products::INT[][]) INTO _arrangementID;
    IF EXISTS (SELECT 1 FROM Arrangement WHERE arrangementID = _arrangementID) THEN
        IF EXISTS (SELECT 1 FROM MovieInArrangement WHERE arrangementID = _arrangementID AND movieID = _movieID) THEN
            RETURN TRUE;
        END IF;
    END IF;
    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 1 Test 3                                                 */
/* Function:  Function to test the add arrangement functionality                */
/*        Succeeds when a new arrangement is added including a product          */
/* Author:    Kevin Dorlas                                                      */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_1_test_3()
RETURNS BOOLEAN AS $BODY$
DECLARE _employeeID INT;
    _branchID INT;
    _deviceID INT;
    _movieIDs INT[];
    _productID INT;
    _productAmount INT;
    _products INT[][];
    _name VARCHAR(65);
    _price MONEY;
    _expDate DATE;
    _amount INT;
    _arrangementID INT;
BEGIN
  _name := 'Arr_1';
  _price := 99;
  _expDate := CURRENT_TIMESTAMP;
  _amount := 1;
  _movieIDs := '{}';
  _productAmount := 1;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Product ("NAME", price) VALUES ('Product1', 5) RETURNING productID INTO _productID;
    _products := ARRAY[ARRAY[_productID,_productAmount]];

    /* Arrangement & ProductInArrangement INSERT Only */
   SELECT fc_add_arrangement(_branchID, _deviceID, _employeeID, _name, _price, _expDate, _movieIDs::INT[], _amount, _products::INT[][]) INTO _arrangementID;

    IF EXISTS (SELECT 1 FROM Arrangement WHERE arrangementID = _arrangementID) THEN
        IF EXISTS (SELECT 1 FROM ProductInArrangement WHERE arrangementID = _arrangementID AND productID = _productID AND amount = _productAmount) THEN
            RETURN TRUE;
        END IF;
    END IF;

    RETURN FALSE;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 1 Test 4                                                 */
/* Function:  Function to test the add arrangement functionality                */
/*              Succeeds when a new arrangement is added including              */
/*              one movie AND one product                                       */
/* Author:    Kevin Dorlas                                                      */
/*==============================================================================*/
 CREATE OR REPLACE FUNCTION fc_uc_1_test_4()
 RETURNS BOOLEAN AS $BODY$
 DECLARE _employeeID INT;
    _branchID INT;
    _deviceID INT;
    _movieID INT;
    _movieIDs INT[];
    _productID INT;
    _productAmount INT;
    _products INT[][];
    _name VARCHAR(65);
    _price MONEY;
    _expDate DATE;
    _amount INT;
    _arrangementID INT;
 BEGIN
   _name := 'Arr_1';
    _price := 99;
    _expDate := CURRENT_TIMESTAMP;
    _amount := 1;
    _productAmount := 1;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
   INSERT INTO Movie ("NAME", duration) VALUES ('Movie1', 100) RETURNING movieID INTO _movieID;
    _movieIDs := ARRAY[_movieID];
    INSERT INTO Product ("NAME", price) VALUES ('Product1', 5) RETURNING productID INTO _productID;
    _products := ARRAY[ARRAY[_productID,_productAmount]];

     /* Arrangement, MovieInArrangement & ProductInArrangement INSERT */
   SELECT fc_add_arrangement(_branchID, _deviceID, _employeeID, _name, _price, _expDate, _movieIDs::INT[], _amount, _products::INT[][]) INTO _arrangementID;

   IF EXISTS (SELECT 1 FROM Arrangement WHERE arrangementID = _arrangementID) THEN
        IF EXISTS (SELECT 1 FROM MovieInArrangement WHERE arrangementID = _arrangementID AND movieID = _movieID) THEN
            IF EXISTS (SELECT 1 FROM ProductInArrangement WHERE arrangementID = _arrangementID AND productID = _productID AND amount = _productAmount) THEN
                RETURN TRUE;
            END IF;
        END IF;
    END IF;

    RETURN FALSE;

  EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
END;
 $BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 1 Test 5                                                 */
/* Function:  Function to test the add arrangement functionality                */
/*            Succeeds when a new arrangement is added including                */
/*              two movies AND two products.                                    */
/* Author:    Kevin Dorlas                                                      */
/*==============================================================================*/
 CREATE OR REPLACE FUNCTION fc_uc_1_test_5()
 RETURNS BOOLEAN AS $BODY$
 DECLARE _employeeID INT;
    _branchID INT;
    _deviceID INT;
    _movieID1 INT;
    _movieID2 INT;
    _movieIDs INT[];
    _productID1 INT;
    _productID2 INT;
    _productAmount1 INT;
    _productAmount2 INT;
    _products INT[][];
    _name VARCHAR(65);
    _price MONEY;
    _expDate DATE;
    _amount INT;
    _arrangementID INT;
 BEGIN
   _name := 'Arr_1';
    _price := 99;
    _expDate := CURRENT_TIMESTAMP;
    _amount := 1;
    _productAmount1 := 1;
    _productAmount2 := 2;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
   INSERT INTO Movie ("NAME", duration) VALUES ('Movie1', 100) RETURNING movieID INTO _movieID1;
    INSERT INTO Movie ("NAME", duration) VALUES ('Movie2', 50) RETURNING movieID INTO _movieID2;
    _movieIDs := ARRAY[_movieID1, _movieID2];
    INSERT INTO Product ("NAME", price) VALUES ('Product1', 5) RETURNING productID INTO _productID1;
    INSERT INTO Product ("NAME", price) VALUES ('Product2', 10) RETURNING productID INTO _productID2;
    _products := ARRAY[ARRAY[_productID1,_productAmount1], ARRAY[_productID2,_productAmount2]];

     /* Arrangement, MovieInArrangement & ProductInArrangement INSERT */
   SELECT fc_add_arrangement(_branchID, _deviceID, _employeeID, _name, _price, _expDate, _movieIDs::INT[], _amount, _products::INT[][]) INTO _arrangementID;

   IF NOT EXISTS (SELECT 1 FROM Arrangement WHERE arrangementID = _arrangementID) THEN
      RETURN FALSE;
    END IF;

    FOR i IN 1 .. array_upper(_movieIDs, 1)
        LOOP
            IF NOT EXISTS (SELECT 1 FROM MovieInArrangement WHERE arrangementID = _arrangementID AND movieID = _movieIDs[i]) THEN
                RETURN FALSE;
            END IF;
        END LOOP;

    FOR i IN array_lower(_products, 1) .. array_upper(_products, 1)
        LOOP
            IF NOT EXISTS (SELECT 1 FROM ProductInArrangement WHERE arrangementID = _arrangementID AND productID = _products[i][1] AND amount = _products[i][2]) THEN
                RETURN FALSE;
            END IF;
        END LOOP;

    RETURN TRUE;

   EXCEPTION WHEN OTHERS THEN
       RETURN FALSE;
END;
 $BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Use case name:      Use case 2 TESTS                         */
/* Author:              Tom van Grinsven                        */
/* Function:            Tests for Use case 2                    */
/* Tests IF the name, price AND DATE of an arrangement are      */
/* updated. Succeeds when these VALUES are updated.             */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_uc_2_test_1()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
DECLARE _productID1 INT;
DECLARE _productID2 INT;
DECLARE _deviceID INT;
DECLARE _branchID INT;
DECLARE _employeeID INT;
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Ladies night 25-12-2017', 10::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
    INSERT INTO Product ("NAME", price) VALUES ('Bavaria light', 5::MONEY) RETURNING productID INTO _productID1;
    INSERT INTO Product ("NAME", price) VALUES ('Bavaria Heavy', 6::MONEY) RETURNING productID INTO _productID2;
    INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID1, 1);
    INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID2, 1);
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;

  PERFORM fc_update_arrangement(_arrangementID, 'Ladies night 26-12-2017', 20::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY'), ARRAY[ ARRAY[_productID1,3], ARRAY[_productID2,3]], _employeeID, _deviceID, _branchID);

  IF EXISTS (SELECT 1 FROM Arrangement WHERE arrangementID = _arrangementID AND "NAME" = 'Ladies night 26-12-2017' AND price = 20::MONEY AND expirationDate = TO_DATE('10-10-2099', 'DD-MM-YYYY')) THEN
        RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Use case name:       Use case 2 TESTS                        */
/* Author:              Tom van Grinsven                        */
/* Function:            Tests for Use case 2                    */
/* Tests IF the amounts of the products listed on an arrangement*/
/* are updated. Succeeds when these amounts are updated.        */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_uc_2_test_2()
RETURNS BOOLEAN AS $BODY$
  DECLARE _arrangementID INT;
  DECLARE _productID1 INT;
  DECLARE _productID2 INT;
  DECLARE _deviceID INT;
  DECLARE _branchID INT;
  DECLARE _employeeID INT;
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Guys night 25-12-2017', 10::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
    INSERT INTO Product ("NAME", price) VALUES ('Bavaria light', 5::MONEY) RETURNING productID INTO _productID1;
    INSERT INTO Product ("NAME", price) VALUES ('Bavaria Heavy', 6::MONEY) RETURNING productID INTO _productID2;
    INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID1, 1);
    INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID2, 1);
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;

  PERFORM fc_update_arrangement(_arrangementID, 'Guys night 26-12-2017', 20::MONEY, TO_DATE('01-01-2099', 'DD-MM-YYYY'), ARRAY[ ARRAY[_productID1,3], ARRAY[_productID2,18]], _employeeID, _deviceID, _branchID);

  IF EXISTS (SELECT 1 FROM ProductInArrangement WHERE arrangementID = _arrangementID AND productID = _productID1 AND amount = 3) THEN
        BEGIN
          IF EXISTS (SELECT 1 FROM ProductInArrangement WHERE arrangementID = _arrangementID AND productID = _productID2 AND amount = 18) THEN
            RETURN TRUE;
        ELSE
          RETURN FALSE;
            END IF;
        END;
    END IF;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Use case name:      Use case 2 TESTS                         */
/* Author:             Tom van Grinsven                         */
/* Function:           Tests for Use case 2                     */
/* Tests IF when a non existing products on an arrangement      */
/* is passed to update the function throws an error.            */
/* Succeeds when the arrangement is not updated.                */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_uc_2_test_3()
RETURNS BOOLEAN AS $BODY$
  DECLARE _arrangementID INT;
  DECLARE _deviceID INT;
  DECLARE _branchID INT;
  DECLARE _employeeID INT;
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Ladies night 25-12-2017', 10::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;

  PERFORM fc_update_arrangement(_arrangementID, 'Ladies night 26-12-2017', 20::MONEY, TO_DATE('01-01-2099', 'DD-MM-YYYY'), ARRAY[ ARRAY[100000,3], ARRAY[200000,4]], _employeeID, _deviceID, _branchID);

  RETURN FALSE;

  EXCEPTION WHEN OTHERS THEN
      RETURN TRUE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 4 Tests                                                  */
/* Function:  Function that tests IF the movie cheque is added when             */
/*            Arrangement ID is NULL,                                           */
/*            entrance is NULL AND                                              */
/*            with remain amount is 0.                                          */
/*            Succeeds IF the movie cheque is correctly added with remaining    */
/*              amount 0 AND the entrance NULL is transformed to 0.             */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_4_test_1()
RETURNS BOOLEAN AS $BODY$
DECLARE _employeeID INT;
    _branchID INT;
    _deviceID INT;
    _barcode VARCHAR(65);
BEGIN
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    SELECT fc_add_movieCheque(_employeeID, NULL, NULL, 0::MONEY, _deviceID, _branchID) INTO _barcode;
  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND entrance = 0 AND remainingAmount = 0::MONEY) THEN
        RETURN TRUE;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 4 Tests                                                  */
/* Function:  Function that tests IF the movie cheque is added when             */
/*            Arrangement ID is NULL,                                           */
/*            entrance is NULL AND                                              */
/*            with remain amount is 10.                                         */
/*            Succeeds IF the movie cheque is correctly added with remaining    */
/*              amount 10 AND the entrance NULL is transformed to 0.            */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_4_test_2()
RETURNS BOOLEAN AS $BODY$
DECLARE _employeeID INT;
    _branchID INT;
    _deviceID INT;
    _barcode VARCHAR(65);
BEGIN
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    SELECT fc_add_movieCheque(_employeeID, NULL, NULL, 10::MONEY, _deviceID, _branchID) INTO _barcode;

  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND entrance = 0 AND remainingAmount = 10::MONEY) THEN
        RETURN TRUE;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 4 Tests                                                  */
/* Function:  Function that tests IF the movie cheque is added when             */
/*            Arrangement ID is NULL,                                           */
/*            entrance is 0 AND                                                 */
/*            with remaining amount is 0.                                       */
/*            Succeeds IF the movie cheque is correctly added with entrance 0   */
/*              AND remaining amount 0.                                         */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_4_test_3()
RETURNS BOOLEAN AS $BODY$
DECLARE _employeeID INT;
    _branchID INT;
    _deviceID INT;
    _barcode VARCHAR(65);
BEGIN
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    SELECT fc_add_movieCheque(_employeeID, NULL, 0, 0::MONEY, _deviceID, _branchID) INTO _barcode;
  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND entrance = 0 AND remainingAmount = 0::MONEY) THEN
        RETURN TRUE;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 4 Tests                                                  */
/* Function:  Function that tests IF the movie cheque is added when             */
/*            Arrangement ID is NULL,                                           */
/*            entrance is 1 AND                                                 */
/*            with remaining amount is 0.                                       */
/*            Succeeds IF the movie cheque is correctly added with entrance 1   */
/*              AND remaining amount 0.                                         */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_4_test_4()
RETURNS BOOLEAN AS $BODY$
DECLARE _employeeID INT;
    _branchID INT;
    _deviceID INT;
    _barcode VARCHAR(65);
BEGIN
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    SELECT fc_add_movieCheque(_employeeID, NULL, 1, 0::MONEY, _deviceID, _branchID) INTO _barcode;

  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND entrance = 1 AND remainingAmount = 0::MONEY) THEN
        RETURN TRUE;
    END IF;
    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 4 Tests                                                  */
/* Function:  Function that tests IF the movie cheque is added when             */
/*        Arrangement ID is NOT NULL but doesn't exists in Arrangement,         */
/*        entrance is NULL AND                                                  */
/*        with remaining amount is 0.                                           */
/*        Succeeds IF the movie cheque is not added.                            */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_4_test_5()
RETURNS BOOLEAN AS $BODY$
DECLARE _employeeID INT;
    _branchID INT;
    _deviceID INT;
    _barcode VARCHAR(65);
BEGIN
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    SELECT fc_add_movieCheque(_employeeID, 99999, NULL, 0::MONEY, _deviceID, _branchID) INTO _barcode;

  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND arrangementID = 99999 AND entrance = 0 AND remainingAmount = 0::MONEY) THEN
        RETURN FALSE;
  END IF;

    EXCEPTION WHEN OTHERS THEN
      RETURN TRUE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 4 Tests                                                  */
/* Function:  Function that tests IF the movie cheque is added when             */
/*            Arrangement ID is NOT NULL                                        */
/*            entrance is 0,                                                    */
/*            with remaining amount is 0,                                       */
/*            the number of entrances on the arrangement is 0 AND               */
/*            there are no products linked to the Arrangement.                  */
/*            Succeeds IF the movie cheque correctly added AND there is no      */
/*              Product linked in ProductInMoviecheque.                         */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_4_test_6()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
    _movieID INT;
    _employeeID INT;
    _branchID INT;
    _deviceID INT;
    _barcode VARCHAR(65);
BEGIN
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Guys night 25-12-2017', 10::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO Movie("NAME", duration) VALUES ('The Dark Knight', 120) RETURNING movieID INTO _movieID;
  INSERT INTO MovieInArrangement (movieID, arrangementID, amount) VALUES (_movieID, _arrangementID, 0);

  SELECT fc_add_movieCheque(_employeeID, _arrangementID, NULL, 0::MONEY, _deviceID, _branchID) INTO _barcode;

  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND entrance = 0 AND remainingAmount = 0::MONEY) THEN
      IF ((SELECT SUM(amount) FROM MovieInArrangement WHERE arrangementID = _arrangementID) = 0) THEN
          IF NOT EXISTS(SELECT 1 FROM ProductInMoviecheque WHERE movieChequeID = (SELECT movieChequeID FROM MovieCheque WHERE barcode = _barcode)) THEN
            RETURN TRUE;
            END IF;
        END IF;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 4 Tests                                                  */
/* Function:  Function that tests IF the movie cheque is added when             */
/*        Arrangement ID is NOT NULL                                            */
/*        self specified entrance is NULL,                                      */
/*        the number of entrances on the arrangement is 2 AND                   */
/*        there are no products linked to the Arrangement.                      */
/*        Succeeds IF the movie cheque correctly added AND there is no          */
/*              Product linked in ProductInMoviecheque.                         */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_4_test_7()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
    _movieID INT;
    _employeeID INT;
    _branchID INT;
    _deviceID INT;
    _barcode VARCHAR(65);
BEGIN
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Guys night 25-12-2017', 10::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
    INSERT INTO Movie("NAME", duration) VALUES ('The Dark Knight', 120) RETURNING movieID INTO _movieID;
    INSERT INTO MovieInArrangement (movieID, arrangementID, amount) VALUES (_movieID, _arrangementID, 2);
    SELECT fc_add_movieCheque(_employeeID, _arrangementID, NULL, 0::MONEY, _deviceID, _branchID) INTO _barcode;

  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND entrance = 2 AND remainingAmount = 0::MONEY) THEN
      IF ((SELECT SUM(amount) FROM MovieInArrangement WHERE arrangementID = _arrangementID) = 2) THEN
          IF NOT EXISTS(SELECT 1 FROM ProductInMoviecheque WHERE movieChequeID = (SELECT movieChequeID FROM MovieCheque WHERE barcode = _barcode)) THEN
            RETURN TRUE;
          END IF;
        END IF;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 4 Tests                                                  */
/* Function:  Function that tests IF the movie cheque is added when             */
/*        Arrangement ID is NOT NULL but doens't exists in Arrangement,         */
/*        entrance is NULL,                                                     */
/*        the number of entrances is 0 AND                                      */
/*        there is a product linked to the Arrangement.                         */
/*        Succeeds IF the movie cheque correctly added AND the Product          */
/*              in linked in ProductInMoviecheque.                              */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_4_test_8()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
    _movieID INT;
    _productID INT;
    _employeeID INT;
    _branchID INT;
    _deviceID INT;
    _barcode VARCHAR(65);
BEGIN
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Guys night 25-12-2017', 10::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO Movie("NAME", duration) VALUES ('The Dark Knight', 120) RETURNING movieID INTO _movieID;
  INSERT INTO MovieInArrangement (movieID, arrangementID, amount) VALUES (_movieID, _arrangementID, 0);
  INSERT INTO Product ("NAME", price) VALUES ('Bavaria light', 5::MONEY) RETURNING productID INTO _productID;
  INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID, 1);

  SELECT fc_add_movieCheque(_employeeID, _arrangementID, NULL, 0::MONEY, _deviceID, _branchID) INTO _barcode;

  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND entrance = 0 AND remainingAmount = 0::MONEY) THEN
      IF ((SELECT SUM(amount) FROM MovieInArrangement WHERE arrangementID = _arrangementID) = 0) THEN
          IF EXISTS (SELECT 1 FROM ProductInMoviecheque WHERE movieChequeID = (SELECT movieChequeID FROM MovieCheque WHERE barcode = _barcode) AND productID = _productID AND amount = 1) THEN
            RETURN TRUE;
            END IF;
        END IF;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 4 Tests                                                  */
/* Function:  Function that tests IF the movie cheque is added when             */
/*        Arrangement ID is NOT NULL                                            */
/*        entrance is NULL,                                                     */
/*        the number of entrances is 1 AND                                      */
/*        there is a product linked to the Arrangement.                         */
/*        Succeeds IF the movie cheque correctly added AND the Product          */
/*              in linked in ProductInMoviecheque.                              */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_4_test_9()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
    _movieID INT;
    _productID INT;
    _employeeID INT;
    _branchID INT;
    _deviceID INT;
    _barcode VARCHAR(65);
BEGIN
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Guys night 25-12-2017', 10::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO Movie("NAME", duration) VALUES ('The Dark Knight', 120) RETURNING movieID INTO _movieID;
  INSERT INTO MovieInArrangement (movieID, arrangementID, amount) VALUES (_movieID, _arrangementID, 1);
  INSERT INTO Product ("NAME", price) VALUES ('Bavaria light', 5::MONEY) RETURNING productID INTO _productID;
  INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID, 1);
  SELECT fc_add_movieCheque(_employeeID, _arrangementID, NULL, 0::MONEY, _deviceID, _branchID) INTO _barcode;
  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND entrance = 1 AND remainingAmount = 0::MONEY) THEN
      IF ((SELECT SUM(amount) FROM MovieInArrangement WHERE arrangementID = _arrangementID) = 1) THEN
          IF EXISTS (SELECT 1 FROM ProductInMoviecheque WHERE movieChequeID = (SELECT movieChequeID FROM MovieCheque WHERE barcode = _barcode) AND productID = _productID AND amount = 1) THEN
            RETURN TRUE;
            END IF;
        END IF;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 5 test 1                                     */
/* Function:            Test that tests a moviecheque with 3 entrances AND an */
/*            arrangementID with no movies on it. It succeeds IF the          */
/*                      entrances is set to 0. Because it tries to subtract 3 */
/*            entrances                                                       */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_5_test_1() RETURNS BOOLEAN
AS $BODY$
DECLARE
_arrangementID INT;
_barcode varchar;
_employeeRole INT;
_employeeID INT;
_deviceID INT;
_branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('a',0,TO_DATE('08-12-2040', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO Moviecheque( arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID,3,0,(SELECT fc_create_barcode(1)),TO_DATE('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;

  PERFORM fc_scan_Moviecheque(_barcode, _employeeID, _branchID, _deviceID, 3, 1);

  IF EXISTS (SELECT 1 FROM Moviecheque WHERE barcode = _barcode AND entrance = 0) THEN
    RETURN TRUE;
  ELSE
      RETURN FALSE;
    END IF;
  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 5 test 2                                     */
/* Function:            Test that tests a moviecheque with 3 entrances AND no */
/*            arrangementID. It succeeds IF the                               */
/*                      entrances is set to 2. Because it tries to subtract 1 */
/*            entrances.                                                      */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/

CREATE OR REPLACE FUNCTION fc_uc_5_test_2() RETURNS BOOLEAN
AS $BODY$
DECLARE
_barcode varchar;
_employeeRole INT;
_employeeID INT;
_deviceID INT;
_branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Moviecheque( arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (NULL,3,0,(SELECT fc_create_barcode(1)),TO_DATE('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;
  PERFORM fc_scan_Moviecheque(_barcode, _employeeID, _branchID, _deviceID, 1, 1);
  IF EXISTS (SELECT 1 FROM Moviecheque WHERE barcode = _barcode AND entrance = 2) THEN
    RETURN TRUE;
  ELSE
      RETURN FALSE;
    END IF;
  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 5 test 3                                     */
/* Function:            Test that tests IF a blocked moviecheque will get an  */
/*             exception                                                      */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/

CREATE OR REPLACE FUNCTION fc_uc_5_test_3() RETURNS BOOLEAN
AS $BODY$
DECLARE
_barcode varchar;
_employeeRole INT;
_employeeID INT;
_deviceID INT;
_branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Moviecheque( arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (NULL,0,3,(SELECT fc_create_barcode(1)),TO_DATE('08-12-2030', 'DD-MM-YYYY'),'0') RETURNING barcode INTO _barcode;
  PERFORM fc_scan_Moviecheque(_barcode, _employeeID, _branchID, _deviceID, 1, 1);
  IF EXISTS (SELECT 1 FROM Moviecheque WHERE barcode = _barcode AND entrance = 2) THEN
    RETURN FALSE;
  ELSE
      RETURN TRUE;
    END IF;
  EXCEPTION WHEN OTHERS THEN
    RETURN TRUE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 5 test 4                                     */
/* Function:            Test that tests IF an expired moviecheque will get an */
/*             exception                                                      */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/

CREATE OR REPLACE FUNCTION fc_uc_5_test_4() RETURNS BOOLEAN
AS $BODY$
DECLARE _barcode varchar;
_employeeRole INT;
_employeeID INT;
_deviceID INT;
_branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Moviecheque( arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (NULL,0,0,(SELECT fc_create_barcode(1)),TO_DATE('08-12-2014', 'DD-MM-YYYY'),'0') RETURNING barcode INTO _barcode;
  PERFORM fc_scan_Moviecheque(_barcode, _employeeID, _branchID, _deviceID, 1, 1);
  IF EXISTS (SELECT 1 FROM Moviecheque WHERE barcode = _barcode AND entrance = 2) THEN
    RETURN FALSE;
  ELSE
      RETURN TRUE;
    END IF;
  EXCEPTION WHEN OTHERS THEN
    RETURN TRUE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 5 test 5                                     */
/* Function:            Test that tests a moviecheque with 3 entrances AND an */
/*            arrangementID with movies linked to it. It succeeds             */
/*            IF the entrances is set to 2. Because it tries to               */
/*                      subtract 1 entrances AND the movie that is given is   */
/*                      a movie linked with the arrangement.                  */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/

CREATE OR REPLACE FUNCTION fc_uc_5_test_5() RETURNS BOOLEAN
AS $BODY$
DECLARE
_barcode varchar;
_movieID INT;
_arrangementID INT;
_employeeRole INT;
_employeeID INT;
_deviceID INT;
_branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('a',0,TO_DATE('08-12-2040', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO Movie ("NAME", duration)VALUES ('a',120) RETURNING movieID INTO _movieID;
  INSERT INTO MovieinArrangement VALUES (_movieID, _arrangementID, 1);
  INSERT INTO Moviecheque( arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID,3,0,(SELECT fc_create_barcode(1)),TO_DATE('04-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;
    PERFORM fc_scan_Moviecheque(_barcode, _employeeID, _branchID, _deviceID, 1, _movieID);
  IF EXISTS (SELECT 1 FROM Moviecheque WHERE barcode = _barcode AND entrance = 2) THEN
    RETURN TRUE;
  ELSE
      RETURN FALSE;
    END IF;
  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 57 test 1                                    */
/* Function:            Function that tests IF the given Moviecheque with an  */
/*                   Arrangement with a movie linked to it but not the given, */
/*                      0 entrances AND 4 requested entrances cost 40.80      */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_57_test_1() RETURNS BOOLEAN
AS $BODY$
DECLARE
_arrangementID INT;
_barcode varchar;
_amount MONEY;
_employeeRole INT;
_employeeID INT;
_deviceID INT;
_branchID INT;
_movieID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('a',0,TO_DATE('08-12-2040', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO Moviecheque( arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID,3,0,(SELECT fc_create_barcode(1)),TO_DATE('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;
  INSERT INTO Movie ("NAME", duration)VALUES ('a',120) RETURNING movieID INTO _movieID;
  INSERT INTO MovieinArrangement VALUES (_movieID, _arrangementID, 1);
   _amount := fc_scan_Moviecheque(_barcode, _employeeID, _branchID, _deviceID, 4, 0);
  IF EXISTS (SELECT 1 FROM Moviecheque WHERE barcode = _barcode AND entrance = 3) AND _amount = 40.80::MONEY THEN
    RETURN TRUE;
  ELSE
      RETURN FALSE;
    END IF;
  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 57 test 2                                    */
/* Function:            Function that tests IF the given Moviecheque with an  */
/*             Arrangement with a movies linked to it, 3 entrances            */
/*            AND 4 requested entrances cost 10.20                            */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/

CREATE OR REPLACE FUNCTION fc_uc_57_test_2() RETURNS BOOLEAN
AS $BODY$
DECLARE
_barcode varchar;
_movieID INT;
_arrangementID INT;
_amount MONEY;
_employeeRole INT;
_employeeID INT;
_deviceID INT;
_branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('a',0,TO_DATE('08-12-2040', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
    INSERT INTO Movie ("NAME", duration)VALUES ('a',120) RETURNING movieID INTO _movieID;
    INSERT INTO MovieinArrangement VALUES (_movieID, _arrangementID, 1);
  INSERT INTO Moviecheque( arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID,3,0,(SELECT fc_create_barcode(1)),TO_DATE('04-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;

    _amount := fc_scan_Moviecheque(_barcode, _employeeID, _branchID, _deviceID, 4, _movieID);
  IF EXISTS (SELECT 1 FROM Moviecheque WHERE barcode = _barcode AND entrance = 0) AND _amount = 10.20::MONEY THEN
    RETURN TRUE;
  ELSE
      RETURN FALSE;
    END IF;
  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 57 test 3                                    */
/* Function:            Function that tests IF the given Moviecheque with an  */
/*             Arrangement with no movies linked to it, 3 entrances           */
/*            AND 4 requested entrances cost 10.20                            */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/

CREATE OR REPLACE FUNCTION fc_uc_57_test_3() RETURNS BOOLEAN
AS $BODY$
DECLARE
_barcode varchar;
_amount MONEY;
_employeeRole INT;
_employeeID INT;
_deviceID INT;
_branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Moviecheque( arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (NULL,3,0,(SELECT fc_create_barcode(1)),TO_DATE('04-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;
    _amount := fc_scan_Moviecheque(_barcode, _employeeID, _branchID, _deviceID, 4, 0);
  IF EXISTS (SELECT 1 FROM Moviecheque WHERE barcode = _barcode AND entrance = 0) AND _amount = 10.20::MONEY THEN
    RETURN TRUE;
  ELSE
      RETURN FALSE;
    END IF;
  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 57 test 4                                    */
/* Function:            Function that tests IF the given Moviecheque with an  */
/*             Arrangement with a movies linked to it that is not the         */
/*         requested movie, 3 entrances. the amount given back should be 10.20*/
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_57_test_4() RETURNS BOOLEAN
AS $BODY$
DECLARE
_barcode varchar;
_movieID INT;
_arrangementID INT;
_employeeRole INT;
_employeeID INT;
_deviceID INT;
_branchID INT;
_amount MONEY;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('a',0,TO_DATE('08-12-2040', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
    INSERT INTO Movie ("NAME", duration)VALUES ('a',120) RETURNING movieID INTO _movieID;
    INSERT INTO MovieinArrangement VALUES (_movieID, _arrangementID, 1);
  INSERT INTO Moviecheque( arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID,3,0,(SELECT fc_create_barcode(1)),TO_DATE('04-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;

    _amount := fc_scan_Moviecheque(_barcode, _employeeID, _branchID, _deviceID, 1, 0);

    IF EXISTS (SELECT 1 FROM Moviecheque WHERE barcode = _barcode AND entrance = 3) AND _amount = 10.20::MONEY THEN
    RETURN TRUE;
  ELSE
      RETURN FALSE;
    END IF;
  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 57 test 5                                    */
/* Function:            Test that tests a moviecheque with 3 entrances AND no */
/*            arrangementID. It succeeds IF the                               */
/*                      entrances is set to 0. Because it tries to subtract 4 */
/*            entrances. The returned amount should be 10.20                  */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/

CREATE OR REPLACE FUNCTION fc_uc_57_test_5() RETURNS BOOLEAN
AS $BODY$
DECLARE
_barcode varchar;
_employeeRole INT;
_employeeID INT;
_deviceID INT;
_branchID INT;
_amount MONEY;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Moviecheque( arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (NULL,3,0,(SELECT fc_create_barcode(1)),TO_DATE('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;
  _amount := fc_scan_Moviecheque(_barcode, _employeeID, _branchID, _deviceID, 4, 1);
  IF EXISTS (SELECT 1 FROM Moviecheque WHERE barcode = _barcode AND entrance = 0) AND _amount = 10.20::MONEY THEN
    RETURN TRUE;
  ELSE
      RETURN FALSE;
    END IF;
  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 6 test 1                                     */
/* Function:            Test that tests a moviecheque with a product on it    */
/*             with an amount of 3. The amount that the user wants            */
/*             is 2. This function tests IF the amount is updates to          */
/*             1.                                                             */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_6_test_1()
RETURNS BOOLEAN AS $BODY$
DECLARE _movieChequeID INT;
    _productID INT;
    _barcode TEXT;
    _amount MONEY;
    _employeeRole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, Barcode, expirationDate, "STATUS") VALUES (NULL,3,0,(SELECT fc_create_barcode(1)),TO_DATE ('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING movieChequeID INTO _movieChequeID;
    SELECT Barcode INTO _barcode from MovieCheque WHERE movieChequeID = _movieChequeID;
    INSERT INTO Product ("NAME", price) VALUES ('bier', 1.20::MONEY) RETURNING productID INTO _productID;
    INSERT INTO ProductInMovieCheque VALUES (_movieChequeID, _productID, 3);

  _amount := fc_scan_product_from_moviecheque (_barcode, _employeeID, _branchID, _deviceID, ARRAY[ARRAY[_productID,2]]::INT[][]);

  IF EXISTS (SELECT 1 FROM ProductInMovieCheque WHERE movieChequeID = _movieChequeID AND productID = _productID AND amount = 1) AND _amount = 0::MONEY THEN
    RETURN TRUE;
  ELSE
      RETURN FALSE;
    END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 6 test 2                                     */
/* Function:            Tests that a moviecheque with 2 products on with      */
/*             one being an amount of 3 AND one being an amount of 4          */
/*             . This function uses an ARRAY with these products. the         */
/*             function will succeed IF update is succesfully                 */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_6_test_2()
RETURNS BOOLEAN AS $BODY$
DECLARE _movieChequeID INT;
    _productID INT;
    _productID2 INT;
    _barcode TEXT;
    _amount MONEY;
    _employeeRole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, Barcode, expirationDate, "STATUS") VALUES (NULL,3,0,(SELECT fc_create_barcode(1)),TO_DATE ('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING movieChequeID INTO _movieChequeID;
  SELECT Barcode INTO _barcode from MovieCheque WHERE movieChequeID = _movieChequeID;
  INSERT INTO Product ("NAME", price) VALUES ('bier', 1.20::MONEY) RETURNING productID INTO _productID;
  INSERT INTO Product ("NAME", price) VALUES ('duurder bier', 2::MONEY) RETURNING productID INTO _productID2;
  INSERT INTO ProductInMovieCheque VALUES (_movieChequeID, _productID, 3);
  INSERT INTO ProductInMovieCheque VALUES (_movieChequeID, _productID2, 4);

  _amount := fc_scan_product_from_moviecheque (_barcode, _employeeID, _branchID, _deviceID, ARRAY[ARRAY[_productID,2], ARRAY[_productID2, 4]]);

  IF EXISTS (SELECT 1 FROM ProductInMovieCheque WHERE movieChequeID = _movieChequeID AND productID = _productID AND amount = 1) AND _amount = 0.00::MONEY THEN
      IF EXISTS (SELECT 1 FROM ProductInMovieCheque WHERE movieChequeID = _movieChequeID AND productID = _productID2 AND amount = 0) THEN
      RETURN TRUE;

    ELSE
      RETURN FALSE;
        END IF;
    ELSE
    RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 67 test 1                                    */
/* Function:            Test that tests a moviecheque with a product on it    */
/*             with an amount of 1. The amount that the user wants            */
/*             is 2. This function tests IF the amount is updates to          */
/*             0 AND the _amount is 1.20.                                     */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_67_test_1()
RETURNS BOOLEAN AS $BODY$
DECLARE _movieChequeID INT;
    _productID INT;
    _barcode TEXT;
    _amount MONEY;
    _employeeRole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, Barcode, expirationDate, "STATUS") VALUES (NULL,3,0,(SELECT fc_create_barcode(1)),TO_DATE ('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING movieChequeID INTO _movieChequeID;
  SELECT Barcode INTO _barcode from MovieCheque WHERE movieChequeID = _movieChequeID;
  INSERT INTO Product ("NAME", price) VALUES ('bier', 1.20::MONEY) RETURNING productID INTO _productID;
  INSERT INTO ProductInMovieCheque VALUES (_movieChequeID, _productID, 1);

  _amount := fc_scan_product_from_moviecheque (_barcode, _employeeID, _branchID, _deviceID, ARRAY[ARRAY[_productID,2]]::INT[][]);

  IF EXISTS (SELECT 1 FROM ProductInMovieCheque WHERE movieChequeID = _movieChequeID AND productID = _productID AND amount = 0) AND _amount = 1.20::MONEY THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 67 test 2                                    */
/* Function:            Test that tests a moviecheque with a product on it    */
/*             with an amount of 0. The amount that the user wants            */
/*             is 2. This function tests IF the amount is updates to          */
/*             0 AND the _amount is 2.40.                                     */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_67_test_2()
RETURNS BOOLEAN AS $BODY$
DECLARE _movieChequeID INT;
    _productID INT;
    _barcode TEXT;
    _amount MONEY;
    _employeeRole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, Barcode, expirationDate, "STATUS") VALUES (NULL,3,0,(SELECT fc_create_barcode(1)),TO_DATE ('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING movieChequeID INTO _movieChequeID;
  SELECT Barcode INTO _barcode from MovieCheque WHERE movieChequeID = _movieChequeID;
  INSERT INTO Product ("NAME", price) VALUES ('bier', 1.20::MONEY) RETURNING productID INTO _productID;

  _amount := fc_scan_product_from_moviecheque (_barcode, _employeeID, _branchID, _deviceID, ARRAY[ARRAY[_productID,2]]::INT[][]);

  IF (_amount = 2.40::MONEY) THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 67 test 3                                    */
/* Function:            Test that tests a moviecheque with two products on it.*/
/*             one with an amount of 3, the second with AND amount of         */
/*            2. The amount that the user wants is 2 AND 4.                   */
/*            This function tests IF the amount is updates to                 */
/*             1 AND the _amount that is given back is 1.20.                  */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_67_test_3()
RETURNS BOOLEAN AS $BODY$
DECLARE _movieChequeID INT;
    _productID INT;
    _productID2 INT;
    _barcode TEXT;
    _amount MONEY;
    _employeeRole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, Barcode, expirationDate, "STATUS") VALUES (NULL,3,0,(SELECT fc_create_barcode(1)),TO_DATE ('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING movieChequeID INTO _movieChequeID;
  SELECT Barcode INTO _barcode from MovieCheque WHERE movieChequeID = _movieChequeID;
  INSERT INTO Product ("NAME", price) VALUES ('bier', 1.20::MONEY) RETURNING productID INTO _productID;
  INSERT INTO Product ("NAME", price) VALUES ('duurder bier', 2::MONEY) RETURNING productID INTO _productID2;
  INSERT INTO ProductInMovieCheque VALUES (_movieChequeID, _productID, 3);
  INSERT INTO ProductInMovieCheque VALUES (_movieChequeID, _productID2, 2);

  _amount := fc_scan_product_from_moviecheque (_barcode, _employeeID, _branchID, _deviceID, ARRAY[ARRAY[_productID,2], ARRAY[_productID2, 4]]);

  IF EXISTS (SELECT 1 FROM ProductInMovieCheque WHERE movieChequeID = _movieChequeID AND productID = _productID AND amount = 1) AND _amount = 4.00::MONEY THEN
      IF EXISTS(SELECT 1 FROM ProductInMovieCheque WHERE movieChequeID = _movieChequeID AND productID = _productID2 AND amount = 0) THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
        END IF;
    ELSE
    RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 7 test 1                                     */
/* Function:            Calucates the transaction for a user who wants to     */
/*                      visit a movie with 2 entrances AND 25 as              */
/*                      remainingAmount, the remainingAmount should be        */
/*                      4.60 AND the amount that is given back should         */
/*                      be 0, THEN the tests is succesfull                    */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_7_test_1()
RETURNS BOOLEAN AS $BODY$
DECLARE _barcode VARCHAR;
    _amount MONEY;
    _employeeRole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (NULL,0,25::MONEY,(SELECT fc_create_barcode(1)),TO_DATE('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;

  _amount = fc_transaction_movie(_barcode, 2, _employeeID, _branchID, _deviceID);

  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND remainingAmount = 4.60::MONEY) THEN
    IF (_amount = 0::MONEY) THEN
      RETURN TRUE;
        ELSE
          RETURN FALSE;
        END IF;
  ELSE
      RETURN FALSE;
    END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 7 test 2                                     */
/* Function:            Calucates the transaction for a user who wants to     */
/*                      visit a movie with 3 entrances AND 25 as              */
/*                      remainingAmount, the remainingAmount should be        */
/*                      0 AND the amount that is given back should            */
/*                      be 5.60, THEN the tests is succesfull                 */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_7_test_2()
RETURNS BOOLEAN AS $BODY$
DECLARE _barcode VARCHAR;
    _amount MONEY;
    _employeeRole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (NULL,0,25::MONEY,(SELECT fc_create_barcode(1)),TO_DATE('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;

  _amount = fc_transaction_movie(_barcode, 3, _employeeID, _branchID, _deviceID);

  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND remainingAmount = 0::MONEY) THEN
    IF (_amount = 5.60::MONEY) THEN
      RETURN TRUE;
        ELSE
          RETURN FALSE;
        END IF;
  ELSE
      RETURN FALSE;
    END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 7 test 3                                     */
/* Function:            Calucates the transaction for a user who wants to     */
/*                      visit a movie with 3 entrances AND 0 as               */
/*                      remainingAmount, the remainingAmount should be        */
/*                      0 AND the amount that is given back should            */
/*                      be 30.60, THEN the tests is succesfull                */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_7_test_3()
RETURNS BOOLEAN AS $BODY$
DECLARE _barcode VARCHAR;
    _amount MONEY;
    _employeeRole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (NULL,0,0::MONEY,(SELECT fc_create_barcode(1)),TO_DATE('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;

  _amount = fc_transaction_movie(_barcode, 3, _employeeID, _branchID, _deviceID);

  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND remainingAmount = 0::MONEY) THEN
    IF (_amount = 30.60::MONEY) THEN
      RETURN TRUE;
        ELSE
          RETURN FALSE;
        END IF;
  ELSE
      RETURN FALSE;
    END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 7 test 4                                     */
/* Function:            Calucates the transaction for a user who wants to     */
/*                      purchase 5 beers with the remainingAmount on the      */
/*            moviecheque. A beer costs 1.20. The  remainingAmount            */
/*            is 25. The test succeeds when the remainingAmount               */
/*            is 19 AND the amount that is given back is 0                    */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_7_test_4()
RETURNS BOOLEAN AS $BODY$
DECLARE _barcode VARCHAR;
    _amount MONEY;
    _productID INT;
    _employeeRole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (NULL,0,25::MONEY,(SELECT fc_create_barcode(1)),TO_DATE('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;
  INSERT INTO Product ("NAME", price) VALUES ('lekker pilsie', 1.20::MONEY) RETURNING productID INTO _productID;

  _amount = fc_transaction_product(_barcode, _productID, 5, _employeeID, _branchID, _deviceID);

  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND remainingAmount = 19::MONEY) THEN
    IF (_amount = 0::MONEY) THEN
      RETURN TRUE;
        ELSE
          RETURN FALSE;
        END IF;
  ELSE
      RETURN FALSE;
    END IF;
  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 7 test 5                                     */
/* Function:            Calucates the transaction for a user who wants to     */
/*                      purchase 5 beers with the remainingAmount on the      */
/*            moviecheque. A beer costs 6. The  remainingAmount               */
/*            is 25. The test succeeds when the remainingAmount               */
/*            is 0 AND the amount that is given back is 5                     */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/

CREATE OR REPLACE FUNCTION fc_uc_7_test_5()
RETURNS BOOLEAN AS $BODY$
DECLARE _barcode VARCHAR;
    _amount MONEY;
    _productID INT;
    _employeeRole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (NULL,0,25::MONEY,(SELECT fc_create_barcode(1)),TO_DATE('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;
  INSERT INTO Product ("NAME", price) VALUES ('lekkerder pilsie', 6::MONEY) RETURNING productID INTO _productID;

  _amount = fc_transaction_product(_barcode, _productID, 5, _employeeID, _branchID, _deviceID);
  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND remainingAmount = 0::MONEY) THEN
    IF (_amount = 5::MONEY) THEN
      RETURN TRUE;
    ELSE
      RETURN FALSE;
    END IF;
  ELSE
      RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 7 test 6                                     */
/* Function:            Calucates the transaction for a user who wants to     */
/*                      purchase 5 beers with the remainingAmount on the      */
/*            moviecheque. A beer costs 6. The  remainingAmount               */
/*            is 0. The test succeeds when the remainingAmount                */
/*            is 0 AND the amount that is given back is 30                    */
/* Author:              Cees Verhoeven                                        */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_7_test_6()
RETURNS BOOLEAN AS $BODY$
DECLARE _barcode VARCHAR;
    _amount MONEY;
    _productID INT;
    _employeeRole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (NULL,0,0::MONEY,(SELECT fc_create_barcode(1)),TO_DATE('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;
    INSERT INTO Product ("NAME", price) VALUES ('lekkerder pilsie', 6::MONEY) RETURNING productID INTO _productID;

  _amount = fc_transaction_product(_barcode, _productID,5, _employeeID, _branchID, _deviceID);

  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND remainingAmount = 0::MONEY) THEN
    IF (_amount = 30::MONEY) THEN
      RETURN TRUE;
        ELSE
          RETURN FALSE;
        END IF;
  ELSE
      RETURN FALSE;
    END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 8 test  1                                    */
/* Author:              Raymond de Bruine                                     */
/* Function:            Test function to block a moviecheque.                 */
/* What tested:    A normal employee is blocking a moviecheque within 10      */
/*                  minutes.                                                  */
/* When succeed:  The function returns TRUE when the moviecheque is blocked   */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_8_test_1()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
    _chequeID INT;
    _operationID INT;
    _employeerole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  --  Block a MovieCheque by a normal employee within 10 minutes.
  --  Insert data for the test
  SELECT employeeRoleID INTO _employeeRole FROM employeeRole WHERE roleName = 'Medewerker';
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeerole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO arrangement ("NAME", price, expirationDate) VALUES ('Series night', 10::MONEY, TO_DATE('11-10-2025', 'DD-MM-YYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID, 4, 20::MONEY, (SELECT fc_create_barcode(_employeeID)), TO_DATE('11-10-2025', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _chequeID;
  SELECT operationID INTO _operationID FROM operation WHERE operationDescription = 'Movie cheque created';
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchid INTO _branchID;
  INSERT INTO Device (branchid, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, moviechequeid_MH, date_MH, status_MH) VALUES (_employeeID, _branchID, _deviceID, _operationID, _chequeID, current_timestamp, TRUE);
  --  Execute function
  PERFORM fc_block_movie_cheque(_chequeID, _employeeID, _deviceID, _branchID);
  -- The test
  IF EXISTS (SELECT 1 FROM MovieCheque WHERE movieChequeID = _chequeID AND "STATUS" = FALSE) THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;
  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:     Use Case 8 test  2                                      */
/* Author:            Raymond de Bruine                                       */
/* Function:          Test function to block a moviecheque.                   */
/* What tested:      A normal employee is blocking a moviecheque after 10     */
/*                    minutes.                                                */
/* When succeed:    The function returns TRUE when the moviecheque is not     */
/*                    blocked                                                 */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_8_test_2()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
    _chequeID INT;
    _operationID INT;
    _employeerole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
    _creationTime TIMESTAMP := (CURRENT_TIMESTAMP - TIME '00:20'); -- Creation TIME 20 minutes earlier.
BEGIN
  SELECT employeeRoleID INTO _employeeRole FROM employeeRole WHERE roleName = 'Medewerker';
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeerole, 'Max', 'lastername', 'email@emailadres.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Random movie night', 10::MONEY, TO_DATE('12-01-2030', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID, 2, 30::MONEY, (SELECT fc_create_barcode(_employeeID)), TO_DATE('12-01-2030', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _chequeID;
  SELECT operationID INTO _operationID FROM operation WHERE operationDescription = 'Movie cheque created';
  INSERT INTO Branch("NAME", email, phoneNo) VALUES ('Amsterdamsco', 'branchemail@email.com', 1234567899) RETURNING branchID INTO _branchid;
  INSERT INTO Device (branchid, "NAME") VALUES (_branchID, 'cash desk 2') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, moviechequeid_MH, date_MH, status_MH) VALUES (_employeeID, _branchID, _deviceID, _operationID, _chequeID, _creationTime, TRUE);
  -- Execute function
  PERFORM fc_block_movie_cheque(_chequeID, _employeeID, _deviceID, _branchID);
  -- The test
  IF EXISTS (SELECT 1 FROM MovieCheque WHERE movieChequeID = _chequeID AND "STATUS" = TRUE) THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN TRUE;
END
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:     Use Case 8 test  3                                      */
/* Author:            Raymond de Bruine                                       */
/* Function:          Test function to block a moviecheque.                   */
/* What tested:      A marketing employee is blocking a moviecheque within    */
/*                    10 minutes.                                             */
/* When succeed:    The function returns TRUE when the moviecheque is         */
/*                    blocked                                                 */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_8_test_3()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
    _chequeID INT;
    _operationID INT;
    _employeeRole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  -- In this test, a marketing employee is trying to block a MovieCheque within 10 minutes
  SELECT employeeRoleID INTO _employeeRole FROM employeeRole WHERE roleName = 'Marketingafdeling';
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeerole, 'John', 'Doe', 'JohnDoe@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Action evening', 10::MONEY, TO_DATE('31-12-2030', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID, 4, 50::MONEY, (SELECT fc_create_barcode(_employeeID)), TO_DATE('31-12-2030', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _chequeID;
  SELECT operationID INTO _operationID FROM operation WHERE operationDescription = 'Movie cheque created';
  INSERT INTO Branch("NAME", email, phoneNo) VALUES ('Rotjeknor', 'emailbranch@email.com', 0987654321) RETURNING branchid INTO _branchID;
  INSERT INTO Device (branchId, "NAME") VALUES (_branchID, 'cash desk 3') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, moviechequeid_MH, date_MH, status_MH) VALUES (_employeeID, _branchID, _deviceID, _operationID, _chequeID, CURRENT_TIMESTAMP, TRUE);
  -- Execute function
  PERFORM fc_block_movie_cheque(_chequeID, _employeeID, _deviceID, _branchID);
  -- The test
  IF EXISTS (SELECT 1 FROM MovieCheque WHERE movieChequeID = _chequeID AND "STATUS" = FALSE) THEN
    RETURN TRUE;
  ELSE
    RETURN TRUE;
  END IF;
  -- If an exception is dropped
  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:   Use Case 8 test  4                                        */
/* Author:          Raymond de Bruine                                         */
/* Function:        Test function to block a moviecheque.                     */
/* What tested:     A marketing employee is blocking a moviecheque after 10   */
/*                    minutes.                                                */
/* When succeed:    The function returns TRUE when the moviecheque is         */
/*                    blocked                                                 */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_8_test_4()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
    _chequeID INT;
    _operationID INT;
    _employeerole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
    _creationTime TIMESTAMP := (CURRENT_TIMESTAMP - TIME '00:20'); -- Creation TIME 20 minutes earlier.
BEGIN
  -- In this test, a marketing employee is trying to block a MovieCheque after 10 minutes
  SELECT employeeRoleID INTO _employeeRole FROM employeeRole WHERE roleName = 'Marketingafdeling';
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeerole, 'Harry', 'Winter', 'harrywinter@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Horrornight', 10::MONEY, TO_DATE('31-12-2030', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID, 4, 50::MONEY, (SELECT fc_create_barcode(_employeeID)), TO_DATE('31-12-2030', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _chequeID;
  SELECT operationID INTO _operationID FROM operation WHERE operationDescription = 'Movie cheque created';
  INSERT INTO Branch("NAME", email, phoneNo) VALUES ('Rotjeknor', 'emailbranch@email.com', 0987654321) RETURNING branchid INTO _branchID;
  INSERT INTO Device (branchId, "NAME") VALUES (_branchID, 'cash desk 3') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, moviechequeid_MH, date_MH, status_MH) VALUES (_employeeID, _branchID, _deviceID, _operationID, _chequeID, _creationTime, TRUE);
  -- Execute the function
  PERFORM fc_block_movie_cheque(_chequeID, _employeeID, _deviceID, _branchID);
  -- The test
  IF EXISTS (SELECT 1 FROM MovieCheque WHERE movieChequeID = _chequeID AND "STATUS" = FALSE) THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:   Use Case 8 test  5                                        */
/* Author:          Raymond de Bruine                                         */
/* Function:        Test function to block a moviecheque.                     */
/* What tested:     A financial employee is blocking a moviecheque within     */
/*                    10 minutes.                                             */
/* When succeed:    The function returns TRUE when the moviecheque is         */
/*                    blocked                                                 */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_8_test_5()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
    _chequeID INT;
    _operationID INT;
    _employeeRole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  -- In this test, a Financial employee is trying to block a MovieCheque within 10 minutes
  SELECT employeeRoleID INTO _employeeRole FROM employeeRole WHERE roleName = 'Financiele administratie';
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeerole, 'John', 'Doe', 'JohnDoe@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Comedy day', 10::MONEY, TO_DATE('31-12-2030', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID, 4, 50::MONEY, (SELECT fc_create_barcode(_employeeID)), TO_DATE('31-12-2030', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _chequeID;
  SELECT operationID INTO _operationID FROM operation WHERE operationDescription = 'Movie cheque created';
  INSERT INTO Branch("NAME", email, phoneNo) VALUES ('Rotjeknor', 'emailbranch@email.com', 0987654321) RETURNING branchid INTO _branchID;
  INSERT INTO Device (branchId, "NAME") VALUES (_branchID, 'cash desk 3') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, moviechequeid_MH, date_MH, status_MH) VALUES (_employeeID, _branchID, _deviceID, _operationID, _chequeID, CURRENT_TIMESTAMP, TRUE);
  -- Execute the function
  PERFORM fc_block_movie_cheque(_chequeID, _employeeID, _deviceID, _branchID);
  -- The test
  IF EXISTS (SELECT 1 FROM MovieCheque WHERE movieChequeID = _chequeID AND "STATUS" = FALSE) THEN
    RETURN TRUE;
  ELSE
    RETURN TRUE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:   Use Case 8 test  6                                        */
/* Author:            Raymond de Bruine                                       */
/* Function:       Test function to block a moviecheque.                      */
/* What tested:    A financial employee is blocking a moviecheque after 10    */
/*                  10 minutes.                                               */
/* When succeed:  The function returns TRUE when the moviecheque is blocked   */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_8_test_6()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
    _chequeID INT;
    _operationID INT;
    _employeerole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
    _creationTime TIMESTAMP := (CURRENT_TIMESTAMP - TIME '00:20'); -- Creation TIME 20 minutes earlier.
BEGIN
  -- In this test, a financial employee is trying to block a MovieCheque after 10 minutes
  SELECT employeeRoleID INTO _employeeRole FROM employeeRole WHERE roleName = 'Financiele administratie';
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeerole, 'Harry', 'Winter', 'harrywinter@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Kids day', 10::MONEY, TO_DATE('31-12-2030', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID, 4, 50::MONEY, (SELECT fc_create_barcode(_employeeID)), TO_DATE('31-12-2030', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _chequeID;
  SELECT operationID INTO _operationID FROM operation WHERE operationDescription = 'Movie cheque created';
  INSERT INTO Branch("NAME", email, phoneNo) VALUES ('Rotjeknor', 'emailbranch@email.com', 0987654321) RETURNING branchid INTO _branchID;
  INSERT INTO Device (branchId, "NAME") VALUES (_branchID, 'cash desk 3') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, moviechequeid_MH, date_MH, status_MH) VALUES (_employeeID, _branchID, _deviceID, _operationID, _chequeID, _creationTime, TRUE);
  -- Execute the function
  PERFORM fc_block_movie_cheque(_chequeID, _employeeID, _deviceID, _branchID);
  -- The test
  IF EXISTS (SELECT 1 FROM MovieCheque WHERE movieChequeID = _chequeID AND "STATUS" = FALSE) THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:   Use Case 8 test  7                                        */
/* Author:          Raymond de Bruine                                         */
/* Function:        Test function to block a moviecheque.                     */
/* What tested:     In this test, an employee tried to block a moviecheque    */
/*                    that already is blocked                                 */
/* When succeed:    The test returns TRUE from the exception what will be     */
/*                    dropped                                                 */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_8_test_7()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
    _chequeID INT;
    _operationID INT;
    _employeerole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  -- In this test, an employee is trying to block an movie cheque, but the cheque is already blocked
  SELECT employeeRoleID INTO _employeeRole FROM employeeRole WHERE roleName = 'Medewerker';
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeerole, 'John', 'Doe', 'JohnDoe@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Comedy day', 10::MONEY, TO_DATE('31-12-2030', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID, 4, 50::MONEY, (SELECT fc_create_barcode(_employeeID)), TO_DATE('31-12-2030', 'DD-MM-YYYY'), FALSE) RETURNING movieChequeID INTO _chequeID;
  SELECT operationID INTO _operationID FROM operation WHERE operationDescription = 'Movie cheque created';
  INSERT INTO Branch("NAME", email, phoneNo) VALUES ('Rotjeknor', 'emailbranch@email.com', 0987654321) RETURNING branchid INTO _branchID;
  INSERT INTO Device (branchId, "NAME") VALUES (_branchID, 'cash desk 3') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, moviechequeid_MH, date_MH, status_MH) VALUES (_employeeID, _branchID, _deviceID, _operationID, _chequeID, CURRENT_TIMESTAMP, TRUE);
  -- Execute function
  PERFORM fc_block_movie_cheque(_chequeID, _employeeID, _deviceID, _branchID);
  -- The test
  IF EXISTS (SELECT 1 FROM MovieCheque WHERE movieChequeID = _chequeID AND "STATUS" = FALSE) THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN TRUE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:   Use Case 8 test  8                                        */
/* Author:          Raymond de Bruine                                         */
/* Function:        Test function to block a moviecheque.                     */
/* What tested:     In this test, a user who is unknown to the database, tried*/
/*                    to block a moviecheque                                  */
/* When succeed:    The test returns TRUE from the exception what will be     */
/*                    dropped                                                 */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_8_test_8()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
    _chequeID INT;
    _operationID INT;
    _employeerole INT;
    _employeeID INT;
    _deviceID INT;
    _branchID INT;
BEGIN
  -- In this test, there is an employee who cant block a MovieCheque.
  SELECT employeeRoleID INTO _employeeRole FROM employeeRole WHERE roleName = 'fakerol';
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeerole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO arrangement ("NAME", price, expirationDate) VALUES ('Series night', 10::MONEY, TO_DATE('11-10-2025', 'DD-MM-YYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID, 4, 20::MONEY, (SELECT fc_create_barcode(_employeeID)), TO_DATE('11-10-2025', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _chequeID;
  SELECT operationID INTO _operationID FROM operation WHERE operationDescription = 'Movie cheque created';
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchid INTO _branchID;
  INSERT INTO Device (branchid, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
  INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, moviechequeid_MH, date_MH, status_MH) VALUES (_employeeID, _branchID, _deviceID, _operationID, _chequeID, current_timestamp, TRUE);
  -- Execute the function
  PERFORM fc_block_movie_cheque(_chequeID, _employeeID, _deviceID, _branchID);
  -- The test
  IF EXISTS (SELECT 1 FROM MovieCheque WHERE movieChequeID = _chequeID AND "STATUS" = TRUE) THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN TRUE;
END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 9 UC 1 Test 1                                            */
/* Function:  Function to test the add arrangement to history functionality     */
/*        Only a new arrangement would be added to the history database         */
/* Author:    Kevin Dorlas                                                      */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc9_add_arrangement_to_history_test_1()
RETURNS BOOLEAN AS $BODY$
DECLARE _branchID INT;
    _deviceID INT;
    _employeeID INT;
    _arrangementID INT;
    _expDate DATE;
    _historyID INT;
BEGIN
    _expDate := CURRENT_TIMESTAMP;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Employee (employeeroleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (2, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Guys night 25-12-2017', 10::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY')) RETURNING arrangementid INTO _arrangementID;

    SELECT fc_add_arrangement_to_history(_branchID, _deviceID, _employeeID, _arrangementID, _expDate) INTO _historyID;

    IF EXISTS (SELECT 1 FROM ArrangementHistory WHERE historyID_A = _historyID) THEN
      IF EXISTS (SELECT 1 FROM ExpArrangementH WHERE historyID_A = _historyID AND newExpirationDate_A = _expDate) THEN
        RETURN TRUE;
        END IF;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
        RAISE NOTICE 'Error occured, the test uc9: add arrangement to history test has failed.';
        RAISE NOTICE '% %', SQLERRM, SQLSTATE;
        RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql STRICT;

/*==============================================================================*/
/* Use Case:  Use Case 9 UC1 Test 2                                             */
/* Function:  Function to test the add product to arrangement                   */
/*        to history functionality                                              */
/*        A new product linked to an arrangement is added to the history        */
/* Author:    Kevin Dorlas                                                      */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc9_add_product_arrangement_to_history_test_2()
RETURNS BOOLEAN AS $BODY$
DECLARE _branchID INT;
    _deviceID INT;
    _employeeID INT;
    _operationID INT;
    _arrangementID INT;
    _historyID INT;
    _prodhisID INT;
    _newAmount INT;
BEGIN
    _newAmount := 2;
    _operationID := 3;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (2, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeid INTO _employeeID;
    INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Guys night 25-12-2017', 10::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY')) RETURNING arrangementid INTO _arrangementID;
    INSERT INTO ArrangementHistory (branchID, deviceID, employeeID, operationID, arrangementID_AH, date_AH) VALUES (_branchID, _deviceID, _employeeID, _operationID, _arrangementID, CURRENT_TIMESTAMP) RETURNING historyID_A INTO _historyID;
    INSERT INTO Product ("NAME", price) VALUES ('Product 1', 3::MONEY) RETURNING productID INTO _prodhisID;

    PERFORM fc_add_product_to_arrangement_history(_historyID, _prodhisID, _newAmount) ;

    IF EXISTS (SELECT 1 FROM ProductArrangementHistory WHERE historyID_A = _historyID AND productHistoryID_A = _prodhisID AND newAmount_A = _newAmount) THEN
        RETURN TRUE;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
    RAISE NOTICE 'Error occured, the test uc9: link product to arrangement in the history test has failed.';
    RAISE NOTICE '% %', SQLERRM, SQLSTATE;
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql STRICT;

/*==============================================================*/
/* Use case name:      Use case 9 from 2 TESTS                  */
/* Author:             Tom van Grinsven                         */
/* Function:      Tests for Use case 9 included from Use case 2 */
/* Tests IF an update without a new expiration DATE OR product  */
/* amounts no history is added to the history tables.           */
/* Succeeds when nothing is inserted in the history of an       */
/* arrangement.                                                 */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_test_insert_history_without_updated_values()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
DECLARE _productID1 INT;
DECLARE _productID2 INT;
DECLARE _deviceID INT;
DECLARE _branchID INT;
DECLARE _employeeID INT;
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Ladies night 25-12-2017', 10::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
    INSERT INTO Product ("NAME", price) VALUES ('Bavaria light', 5::MONEY) RETURNING productID INTO _productID1;
    INSERT INTO Product ("NAME", price) VALUES ('Bavaria Heavy', 6::MONEY) RETURNING productID INTO _productID2;
    INSERT INTO Productinarrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID1, 1);
    INSERT INTO Productinarrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID2, 1);
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;

  PERFORM fc_update_arrangement(_arrangementID, 'Ladies night 26-12-2017', 20::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY'), ARRAY[ ARRAY[_productID1,1], ARRAY[_productID2,1]], _employeeID, _deviceID, _branchID);
    IF EXISTS (SELECT 1 FROM ArrangementHistory WHERE arrangementID_AH = _arrangementID AND branchID = _branchID AND deviceID = _deviceID AND employeeID = _employeeID) THEN
        RETURN FALSE;
    ELSE
      RETURN TRUE;
    END IF;
    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Use case name:      Use case 9 from 2 TESTS                  */
/* Author:             Tom van Grinsven                         */
/* Function:     Tests for Use case 9 included from Use case 2  */
/* Tests IF an update with a new expiration DATE                */
/* the correct history is added to the history tables.          */
/* Succeeds when there is a new record in ArrangementHistory AND*/
/* that record is linked to a new record in the expirationDate  */
/* history.                                                     */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_test_update_expirationDate_history() RETURNS BOOLEAN
AS $BODY$
DECLARE _arrangementID INT;
DECLARE _productID1 INT;
DECLARE _productID2 INT;
DECLARE _deviceID INT;
DECLARE _branchID INT;
DECLARE _employeeID INT;
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Ladies night 25-12-2017', 10::MONEY, TO_DATE('10-10-2133', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
    INSERT INTO Product ("NAME", price) VALUES ('Bavaria light', 5::MONEY) RETURNING productID INTO _productID1;
    INSERT INTO Product ("NAME", price) VALUES ('Bavaria Heavy', 6::MONEY) RETURNING productID INTO _productID2;
    INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID1, 1);
    INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID2, 1);
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;

  PERFORM fc_update_arrangement(_arrangementID, 'Ladies night 26-12-2017', 20::MONEY, TO_DATE('10-10-2134', 'DD-MM-YYYY'), ARRAY[ ARRAY[_productID1,1], ARRAY[_productID2,1]], _employeeID, _deviceID, _branchID);
    IF EXISTS (SELECT 1 FROM ArrangementHistory WHERE arrangementID_AH = _arrangementID AND branchID = _branchID AND deviceID = _deviceID AND employeeID = _employeeID) THEN
      IF EXISTS (SELECT 1 FROM ExpArrangementH WHERE oldExpirationDate_A = TO_DATE('10-10-2133', 'DD-MM-YYYY') AND newExpirationDate_A = TO_DATE('10-10-2134', 'DD-MM-YYYY')) THEN
          RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;
    END IF;
    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Use case name:      Use case 9 from 2 TESTS                  */
/* Author:             Tom van Grinsven                         */
/* Function:     Tests for Use case 9 included from Use case 2  */
/* Tests IF an update with multiple new product amounts         */
/* the correct history is added to the history tables.          */
/* Succeeds when there is a new record in ArrangementHistory AND*/
/* that record is linked to a new record in the product history */
/* table.                                                       */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_test_update_product_history_multiple() RETURNS BOOLEAN
AS $BODY$
DECLARE _arrangementID INT;
DECLARE _productID1 INT;
DECLARE _productID2 INT;
DECLARE _deviceID INT;
DECLARE _branchID INT;
DECLARE _employeeID INT;
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Ladies night 25-12-2017', 10::MONEY, TO_DATE('10-10-2133', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
    INSERT INTO Product ("NAME", price) VALUES ('Bavaria light', 5::MONEY) RETURNING productID INTO _productID1;
    INSERT INTO Product ("NAME", price) VALUES ('Bavaria Heavy', 6::MONEY) RETURNING productID INTO _productID2;
    INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID1, 1);
    INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID2, 1);
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;

  PERFORM fc_update_arrangement(_arrangementID, 'Ladies night 26-12-2017', 20::MONEY, TO_DATE('10-10-2133', 'DD-MM-YYYY'), ARRAY[ ARRAY[_productID1,10], ARRAY[_productID2,15]], _employeeID, _deviceID, _branchID);
    IF EXISTS (SELECT 1 FROM ArrangementHistory WHERE arrangementID_AH = _arrangementID AND branchID = _branchID AND deviceID = _deviceID AND employeeID = _employeeID) THEN
      IF EXISTS (SELECT 1 FROM ProductArrangementHistory WHERE productHistoryID_A = _productID1 AND oldAmount_A = 1 AND newAmount_A = 10) THEN
          IF EXISTS (SELECT 1 FROM ProductArrangementHistory WHERE productHistoryID_A = _productID2 AND oldAmount_A = 1 AND newAmount_A = 15) THEN
            RETURN TRUE;
        ELSE
            RETURN FALSE;
        END IF;
    END IF;
    END IF;
    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Use case name:      Use case 9 from 2 TESTS                  */
/* Author:            Tom van Grinsven                          */
/* Function:     Tests for Use case 9 included from Use case 2  */
/* Tests IF an update with a single new product amount          */
/* the correct history is added to the history tables.          */
/* Succeeds when there is a new record in ArrangementHistory AND*/
/* that record is linked to a new record in the product history */
/* table but only for the first product                         */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_test_update_product_history_single() RETURNS BOOLEAN
AS $BODY$
DECLARE _arrangementID INT;
DECLARE _productID1 INT;
DECLARE _productID2 INT;
DECLARE _deviceID INT;
DECLARE _branchID INT;
DECLARE _employeeID INT;
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Ladies night 25-12-2017', 10::MONEY, TO_DATE('10-10-2133', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO Product ("NAME", price) VALUES ('Bavaria light', 5::MONEY) RETURNING productID INTO _productID1;
  INSERT INTO Product ("NAME", price) VALUES ('Bavaria Heavy', 6::MONEY) RETURNING productID INTO _productID2;
  INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID1, 1);
  INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID2, 1);
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;

  PERFORM fc_update_arrangement(_arrangementID, 'Ladies night 26-12-2017', 20::MONEY, TO_DATE('10-10-2133', 'DD-MM-YYYY'), ARRAY[ ARRAY[_productID1,8], ARRAY[_productID2,1]], _employeeID, _deviceID, _branchID);
    IF EXISTS (SELECT 1 FROM ArrangementHistory WHERE arrangementID_AH = _arrangementID AND branchID = _branchID AND deviceID = _deviceID AND employeeID = _employeeID) THEN
      IF EXISTS (SELECT 1 FROM ProductArrangementHistory WHERE productHistoryID_A = _productID1 AND oldAmount_A = 1 AND newAmount_A = 8) THEN
          IF NOT EXISTS (SELECT 1 FROM ProductArrangementHistory WHERE productHistoryID_A = _productID2) THEN
            RETURN TRUE;
      ELSE
          RETURN FALSE;
      END IF;
    END IF;
    END IF;
    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================*/
/* Use case name:      Use case 9 from 2 TESTS                  */
/* Author:            Tom van Grinsven                          */
/* Function:     Tests for Use case 9 included from Use case 2  */
/* Tests IF an update with multiple new product amounts         */
/* AND a new expirationDate, the correct history is added       */
/* to the history tables.                                       */
/* Succeeds when there is a new record in ArrangementHistory AND*/
/* that record is linked to a new record in the product history */
/* table.                                                       */
/*==============================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_test_update_product_and_expirationDate_history() RETURNS BOOLEAN
AS $BODY$
DECLARE _arrangementID INT;
DECLARE _productID1 INT;
DECLARE _productID2 INT;
DECLARE _deviceID INT;
DECLARE _branchID INT;
DECLARE _employeeID INT;
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Ladies night 25-12-2017', 10::MONEY, TO_DATE('10-10-2233', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
    INSERT INTO Product ("NAME", price) VALUES ('Bavaria light', 5::MONEY) RETURNING productID INTO _productID1;
    INSERT INTO Product ("NAME", price) VALUES ('Bavaria Heavy', 6::MONEY) RETURNING productID INTO _productID2;
    INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID1, 1);
    INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID2, 1);
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (1, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;

  PERFORM fc_update_arrangement(_arrangementID, 'Ladies night 26-12-2017', 20::MONEY, TO_DATE('10-10-2234', 'DD-MM-YYYY'), ARRAY[ ARRAY[_productID1,20], ARRAY[_productID2,25]], _employeeID, _deviceID, _branchID);
    IF EXISTS (SELECT 1 FROM ArrangementHistory WHERE arrangementID_AH = _arrangementID AND branchID = _branchID AND deviceID = _deviceID AND employeeID = _employeeID) THEN
      IF EXISTS (SELECT 1 FROM ProductArrangementHistory WHERE productHistoryID_A = _productID1 AND oldAmount_A = 1 AND newAmount_A = 20) THEN
          IF EXISTS (SELECT 1 FROM ProductArrangementHistory WHERE productHistoryID_A = _productID2 AND oldAmount_A = 1 AND newAmount_A = 25) THEN
              IF EXISTS (SELECT 1 FROM ExpArrangementH WHERE oldExpirationDate_A = TO_DATE('10-10-2233', 'DD-MM-YYYY') AND newExpirationDate_A = TO_DATE('10-10-2234', 'DD-MM-YYYY')) THEN
              RETURN TRUE;
        ELSE
            RETURN FALSE;
          END IF;
        END IF;
      END IF;
    END IF;
    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 9 Tests UC4                                              */
/* Author:    Tom van Grinsven                                                  */
/* Function:  Function that tests IF when a MovieCheque is created WITH         */
/*            an Arrangement that has ONLY products AND WITH a remaining        */
/*              amount is added, the correct history is added                   */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_test_create_moviecheque_1()
  RETURNS BOOLEAN AS $BODY$
  DECLARE _arrangementID INT;
  DECLARE _productID1 INT;
  DECLARE _productID2 INT;
  DECLARE _deviceID INT;
  DECLARE _branchID INT;
  DECLARE _employeeID INT;
  DECLARE _barcode text;
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Ladies night 25-12-2017', 10::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
    INSERT INTO Product ("NAME", price) VALUES ('Bavaria light', 5::MONEY) RETURNING productID INTO _productID1;
    INSERT INTO Product ("NAME", price) VALUES ('Bavaria Heavy', 6::MONEY) RETURNING productID INTO _productID2;
    INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID1, 1);
    INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID2, 2);
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (2, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;

  PERFORM fc_add_movieCheque(_employeeID, _arrangementID, 0, 21::MONEY, _deviceID, _branchID);

    SELECT barcode FROM MovieCheque WHERE movieChequeID = (SELECT MAX(movieChequeID) FROM MovieCheque) INTO _barcode;

    IF EXISTS (SELECT 1 FROM MovieChequeHistory WHERE movieChequeID_mh = (SELECT movieChequeID FROM MovieCheque WHERE barcode = _barcode)) THEN
      IF EXISTS (SELECT 1 FROM ProductHistory WHERE productHistoryID = _productID1 AND oldAmount = 0 AND newAmount = 1) THEN
          IF EXISTS (SELECT 1 FROM ProductHistory WHERE productHistoryID = _productID2 AND oldAmount = 0 AND newAmount = 2) THEN
              IF EXISTS (SELECT 1 FROM AmountHistory WHERE oldRemainingAmount = 0::MONEY AND newRemainingAmount = 21::MONEY) THEN
            RETURN TRUE;
                END IF;
            END IF;
        END IF;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 9 Tests UC4                                              */
/* Author:    Tom van Grinsven                                                  */
/* Function:  Function that tests IF when a MovieCheque is created WITH         */
/*              an Arrangement that has ONLY movies,                            */
/*              the correct history is added                                    */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_test_create_moviecheque_2()
RETURNS BOOLEAN AS $BODY$
  DECLARE _arrangementID INT;
  DECLARE _movieID INT;
  DECLARE _deviceID INT;
  DECLARE _branchID INT;
  DECLARE _employeeID INT;
  DECLARE _barcode text;
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Ladies night 25-12-2017', 10::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
    INSERT INTO Movie ("NAME", duration) VALUES ('The Dark Knight', 140)RETURNING movieID INTO _movieID;
    INSERT INTO MovieInArrangement VALUES (_movieID, _arrangementID, 3);

    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (2, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
  PERFORM fc_add_movieCheque(_employeeID, _arrangementID, 0, 0::MONEY, _deviceID, _branchID);

    SELECT barcode FROM MovieCheque WHERE movieChequeID = (SELECT MAX(movieChequeID) FROM MovieCheque) INTO _barcode;

    IF EXISTS (SELECT 1 FROM MovieChequeHistory WHERE movieChequeID_mh = (SELECT movieChequeID FROM MovieCheque WHERE barcode = _barcode)) THEN
      IF EXISTS (SELECT 1 FROM EntranceHistory WHERE oldEntrance = 0 AND newEntrance = 3) THEN
        RETURN TRUE;
         END IF;
     END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 9 Tests UC4                                              */
/* Author:    Tom van Grinsven                                                  */
/* Function:  Function that tests IF when a MovieCheque is created WITHOUT      */
/*              an Arrangement but WITH a number of entrances,                  */
/*              the correct history is added                                    */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_test_create_moviecheque_3()
RETURNS BOOLEAN AS $BODY$
DECLARE _deviceID INT;
DECLARE _branchID INT;
DECLARE _employeeID INT;
DECLARE _barcode text;
BEGIN
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (2, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;
  PERFORM fc_add_movieCheque(_employeeID, NULL, 2, 0::MONEY, _deviceID, _branchID);

    SELECT barcode FROM MovieCheque WHERE movieChequeID = (SELECT MAX(movieChequeID) FROM MovieCheque) INTO _barcode;

    IF EXISTS (SELECT 1 FROM MovieChequeHistory WHERE movieChequeID_mh = (SELECT movieChequeID FROM MovieCheque WHERE barcode = _barcode)) THEN
      IF EXISTS (SELECT 1 FROM EntranceHistory WHERE oldEntrance = 0 AND newEntrance = 2) THEN
        RETURN TRUE;
         END IF;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 9 Tests UC4                                              */
/* Author:    Tom van Grinsven                                                  */
/* Function:  Function that tests IF when a MovieCheque is created WITHOUT      */
/*        an Arrangement but WITH a remaining amount,                           */
/*              the correct history is added                                    */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_test_create_moviecheque_4()
RETURNS BOOLEAN AS $BODY$
DECLARE _deviceID INT;
DECLARE _branchID INT;
DECLARE _employeeID INT;
DECLARE _barcode text;
BEGIN
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (2, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;

  PERFORM fc_add_movieCheque(_employeeID, NULL, 0, 30::MONEY, _deviceID, _branchID);

    SELECT barcode FROM MovieCheque WHERE movieChequeID = (SELECT MAX(movieChequeID) FROM MovieCheque) INTO _barcode;

    IF EXISTS (SELECT 1 FROM MovieChequeHistory WHERE movieChequeID_mh = (SELECT movieChequeID FROM MovieCheque WHERE barcode = _barcode)) THEN
        IF EXISTS (SELECT 1 FROM AmountHistory WHERE oldRemainingAmount = 0::MONEY AND newRemainingAmount = 30::MONEY) THEN
        RETURN TRUE;
        END IF;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 9 Tests UC4                                              */
/* Author:   Tom van Grinsven                                                   */
/* Function:  Function that tests IF when a MovieCheque is created WITHOUT      */
/*        an Arrangement but WITH a self specified number of entrances          */
/*        AND remaining amount, the correct history is added                    */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_test_create_moviecheque_5()
RETURNS BOOLEAN AS $BODY$
DECLARE _deviceID INT;
DECLARE _branchID INT;
DECLARE _employeeID INT;
DECLARE _barcode text;
BEGIN
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (2, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;

  PERFORM fc_add_movieCheque(_employeeID, NULL, 5, 35::MONEY, _deviceID, _branchID);

    SELECT barcode FROM MovieCheque WHERE movieChequeID = (SELECT MAX(movieChequeID) FROM MovieCheque) INTO _barcode;

    IF EXISTS (SELECT 1 FROM MovieChequeHistory WHERE movieChequeID_mh = (SELECT movieChequeID FROM MovieCheque WHERE barcode = _barcode)) THEN
        IF EXISTS (SELECT 1 FROM AmountHistory WHERE oldRemainingAmount = 0::MONEY AND newRemainingAmount = 30::MONEY) THEN
        RETURN TRUE;
        END IF;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 9 Tests UC4                                              */
/* Author:    Tom van Grinsven                                                  */
/* Function:  Function that tests IF when a MovieCheque is created WITH         */
/*        an Arrangement with products AND a Movie AND a remainingAmount,       */
/*              the correct history is added                                    */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_test_create_moviecheque_6()
RETURNS BOOLEAN AS $BODY$
  DECLARE _arrangementID INT;
  DECLARE _movieID INT;
  DECLARE _deviceID INT;
  DECLARE _branchID INT;
  DECLARE _employeeID INT;
  DECLARE _barcode text;
  DECLARE _productID1 INT;
  DECLARE _productID2 INT;
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Ladies night 25-12-2017', 10::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO Movie ("NAME", duration) VALUES ('The Dark Knight', 140)RETURNING movieID INTO _movieID;
  INSERT INTO MovieInArrangement VALUES (_movieID, _arrangementID, 4);
  INSERT INTO Product ("NAME", price) VALUES ('Bavaria light', 5::MONEY) RETURNING productID INTO _productID1;
  INSERT INTO Product ("NAME", price) VALUES ('Bavaria Heavy', 6::MONEY) RETURNING productID INTO _productID2;
  INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID1, 2);
  INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID2, 4);

  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (2, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;

  PERFORM fc_add_movieCheque(_employeeID, _arrangementID, 0, 40::MONEY, _deviceID, _branchID);

    SELECT barcode FROM MovieCheque WHERE movieChequeID = (SELECT MAX(movieChequeID) FROM MovieCheque) INTO _barcode;

    IF EXISTS (SELECT 1 FROM MovieChequeHistory WHERE movieChequeID_mh = (SELECT movieChequeID FROM MovieCheque WHERE barcode = _barcode)) THEN
      IF EXISTS (SELECT 1 FROM EntranceHistory WHERE oldEntrance = 0 AND newEntrance = 4) THEN
          IF EXISTS (SELECT 1 FROM AmountHistory WHERE oldRemainingAmount = 0::MONEY AND newRemainingAmount = 40::MONEY) THEN
                IF EXISTS (SELECT 1 FROM productHistory WHERE productHistoryid = _productID1 AND oldAmount = 0 AND newAmount = 2) THEN
              IF EXISTS (SELECT 1 FROM productHistory WHERE productHistoryid = _productID2 AND oldAmount = 0 AND newAmount = 4) THEN
              RETURN TRUE;
                    END IF;
                END IF;
            END IF;
         END IF;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==============================================================================*/
/* Use Case:  Use Case 9 Tests UC4                                              */
/* Author:    Tom van Grinsven                                                  */
/* Function:  Function that tests when a MovieCheque is created WITH            */
/*              an Arrangement WITHOUT movies but with products,                */
/*              a self specified number of entrances AND a remainingAmount,     */
/*              the correct history is added.                                   */
/*==============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_test_create_moviecheque_7()
RETURNS BOOLEAN AS $BODY$
  DECLARE _arrangementID INT;
  DECLARE _movieID INT;
  DECLARE _deviceID INT;
  DECLARE _branchID INT;
  DECLARE _employeeID INT;
  DECLARE _barcode text;
  DECLARE _productID1 INT;
  DECLARE _productID2 INT;
BEGIN
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES ('Ladies night 25-12-2017', 10::MONEY, TO_DATE('10-10-2099', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO Product ("NAME", price) VALUES ('Bavaria light', 5::MONEY) RETURNING productID INTO _productID1;
  INSERT INTO Product ("NAME", price) VALUES ('Bavaria Heavy', 6::MONEY) RETURNING productID INTO _productID2;
  INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID1, 2);
  INSERT INTO ProductInArrangement (arrangementID, productID, amount) VALUES (_arrangementID, _productID2, 4);

  INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('Branch Nijmegen', 'nijmegen@cinema.nl', '06123') RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'DeviceNijmegen1') RETURNING deviceID INTO _deviceID;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (2, 'firstname', 'surname', 'employee1@cinema.nl', 'password', TRUE) RETURNING employeeID INTO _employeeID;

  PERFORM fc_add_movieCheque(_employeeID, _arrangementID, 5, 50::MONEY, _deviceID, _branchID);

    SELECT barcode FROM MovieCheque WHERE movieChequeID = (SELECT MAX(movieChequeID) FROM MovieCheque) INTO _barcode;

    IF EXISTS (SELECT 1 FROM MovieChequeHistory WHERE movieChequeID_mh = (SELECT movieChequeID FROM MovieCheque WHERE barcode = _barcode)) THEN
      IF EXISTS (SELECT 1 FROM EntranceHistory WHERE oldEntrance = 0 AND newEntrance = 5) THEN
          IF EXISTS (SELECT 1 FROM AmountHistory WHERE oldRemainingAmount = 0::MONEY AND newRemainingAmount = 40::MONEY) THEN
                IF EXISTS (SELECT 1 FROM productHistory WHERE productHistoryid = _productID1 AND oldAmount = 0 AND newAmount = 2) THEN
              IF EXISTS (SELECT 1 FROM productHistory WHERE productHistoryid = _productID2 AND oldAmount = 0 AND newAmount = 4) THEN
              RETURN TRUE;
                    END IF;
                END IF;
            END IF;
         END IF;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
  END;
$BODY$ LANGUAGE plpgsql;

/*==================================================================================*/
/* Use Case:  Use Case 9 UC5 test 1                                                 */
/* Function:    Tests that when a moviecheque with 10 entrances is deducted by 1,   */
/*        the correct amounts of entrances are inserted in history                  */
/*        Succeeds when a new record is inserted in both                            */
/*              Movie Cheque History AND Entrance History,                          */
/*              the old entrance amount = 10 AND the new entrance amount = 9        */
/* Author:      Kevin Dorlas                                                        */
/*==================================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_entrance_history_test_1() RETURNS BOOLEAN
AS $BODY$
DECLARE _arrangementID INT;
        _barcode varchar;
        _employeeRole INT;
        _employeeID INT;
        _deviceID INT;
        _branchID INT;
        _movieChequeID INT;
        _originalEntrances INT;
        _deductEntrances INT;
        _residualEntrances INT;
BEGIN
  _originalEntrances := 10;
  _deductEntrances := 1;
    _residualEntrances := _originalEntrances - _deductEntrances;
    INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Moviecheque( arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (NULL, _originalEntrances, 0, (SELECT fc_create_barcode(1)),TO_DATE('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;
    SELECT movieChequeID INTO _movieChequeID from Moviecheque where barcode = _barcode;

    PERFORM fc_scan_Moviecheque(_barcode, _employeeID, _branchID, _deviceID, _deductEntrances, 1);

    IF EXISTS (SELECT 1 FROM EntranceHistory WHERE oldEntrance = _originalEntrances AND newEntrance = _residualEntrances) THEN
        RETURN TRUE;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*==================================================================================*/
/* Use Case:  Use Case 9 UC5 test 2                                                 */
/* Function:    Tests that when a moviecheque with 5 entrances is deducted by 5,    */
/*        the correct amounts of entrances are inserted in history                  */
/*        Succeeds when a new record is inserted in both                            */
/*              Movie Cheque History AND Entrance History,                          */
/*              the old entrance amount = 5 AND the new entrance amount = 0         */
/* Author:      Kevin Dorlas                                                        */
/*==================================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_entrance_history_test_2() RETURNS BOOLEAN
AS $BODY$
DECLARE _arrangementID INT;
        _barcode varchar;
        _employeeRole INT;
        _employeeID INT;
        _deviceID INT;
        _branchID INT;
        _movieChequeID INT;
        _originalEntrances INT;
        _deductEntrances INT;
        _residualEntrances INT;
BEGIN
  _originalEntrances := 10;
  _deductEntrances := 10;
    _residualEntrances := _originalEntrances - _deductEntrances;
    INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Moviecheque( arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (NULL, _originalEntrances, 0, (SELECT fc_create_barcode(1)),TO_DATE('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;
    SELECT movieChequeID INTO _movieChequeID from Moviecheque where barcode = _barcode;

    PERFORM fc_scan_Moviecheque(_barcode, _employeeID, _branchID, _deviceID, _deductEntrances, 1);

    IF EXISTS (SELECT 1 FROM EntranceHistory WHERE oldEntrance = _originalEntrances AND newEntrance = _residualEntrances) THEN
        RETURN TRUE;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*==================================================================================*/
/* Use Case:  Use Case 9 UC5 test 3                                                 */
/* Function:    Tests that when a moviecheque with 8 entrances is deducted by 12,   */
/*        the correct amounts of entrances are inserted in history                  */
/*        Succeeds when a new record is inserted in both                            */
/*              Movie Cheque History AND Entrance History,                          */
/*              the old entrance amount = 8 AND the new entrance amount = 0         */
/* Author:      Kevin Dorlas                                                        */
/*==================================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_entrance_history_test_3() RETURNS BOOLEAN
AS $BODY$
DECLARE _arrangementID INT;
        _barcode varchar;
        _employeeRole INT;
        _employeeID INT;
        _deviceID INT;
        _branchID INT;
        _movieChequeID INT;
        _originalEntrances INT;
        _deductEntrances INT;
        _residualEntrances INT;
BEGIN
  _originalEntrances := 10;
  _deductEntrances := 20;
    _residualEntrances := 0;
    INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum Ipsum dolor dit is een fake tekst') RETURNING employeeRoleID INTO _employeeRole;
    INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRole, 'firstname', 'surname', 'email@email.com', 'thisIsAFakePassword', TRUE) RETURNING employeeID INTO _employeeID;
    INSERT INTO Branch ("NAME", email, phoneNo) VALUES ('testBranch', 'testemail@email.com', 1234567890) RETURNING branchID INTO _branchID;
    INSERT INTO Device (branchID, "NAME") VALUES (_branchID, 'cash desk 1') RETURNING deviceID INTO _deviceID;
    INSERT INTO Moviecheque( arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (NULL, _originalEntrances, 0, (SELECT fc_create_barcode(1)),TO_DATE('08-12-2030', 'DD-MM-YYYY'),'1') RETURNING barcode INTO _barcode;
    SELECT movieChequeID INTO _movieChequeID from Moviecheque where barcode = _barcode;

    PERFORM fc_scan_Moviecheque(_barcode, _employeeID, _branchID, _deviceID, _deductEntrances, 1);

    IF EXISTS (SELECT 1 FROM EntranceHistory WHERE oldEntrance = _originalEntrances AND newEntrance = _residualEntrances) THEN
        RETURN TRUE;
    END IF;

    RETURN FALSE;

    EXCEPTION WHEN OTHERS THEN
      RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:         Use case 9 Use case 6 test 1                        */
/* Function:              Test the function from Use case 6 so it falls under */
/*              the first IF that calls to the Use Case 9                     */
/* Author:                Cees Verhoeven                                      */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_uc_6_test_1()
RETURNS BOOLEAN AS $BODY$
DECLARE _barcode TEXT;
    _employeeroleID INT;
    _employeeID INT;
    _branchID INT;
    _deviceID INT;
        _productID INT;
        _movieChequeID INT;
BEGIN
  INSERT INTO Operation (operationdescription) VALUES ('Changed the amount of the product');
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES(NULL, 5, 0::MONEY, (select fc_create_barcode(1)), TO_DATE('08-12-2030', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _movieChequeID;
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum ipsum fake text') RETURNING employeeroleID INTO _employeeroleID;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "STATUS", "PASSWORD") VALUES (_employeeroleID, 'Peter', 'Testcase', 'peter-testcase@email.com', TRUE, 'aaaaaa') RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES('Branch Amsterdam', 'amsterdam@cinemavenezia.nl', 2134567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES(_branchID, 'Cash desk 2') RETURNING deviceID INTO _deviceID;
  SELECT Barcode INTO _barcode from MovieCheque WHERE movieChequeID = _movieChequeID;
  INSERT INTO Product ("NAME", price) VALUES ('bier', 1.20::MONEY) RETURNING productID INTO _productID;
  INSERT INTO ProductInMovieCheque VALUES (_movieChequeID, _productID, 10);
  PERFORM fc_scan_product_from_moviecheque (_barcode, _employeeID, _branchID, _deviceID, ARRAY[ARRAY[_productID,8]]);
  IF EXISTS (SELECT 1 FROM producthistory WHERE oldAmount = 10 AND newAmount = 2) THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:         Use case 9 Use case 6 test 2                        */
/* Function:              Test the function from Use case 6 so it falls under */
/*              the second IF that calls to the Use Case 9                    */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_uc_6_test_2()
RETURNS BOOLEAN AS $BODY$
DECLARE _barcode TEXT;
    _employeeroleID INT;
    _employeeID INT;
    _branchID INT;
    _deviceID INT;
        _productID INT;
        _movieChequeID INT;
BEGIN
  INSERT INTO Operation (operationdescription) VALUES ('Changed the amount of the product');
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES(NULL, 5, 0::MONEY, (select fc_create_barcode(1)), TO_DATE('08-12-2030', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _movieChequeID;
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum ipsum fake text') RETURNING employeeroleID INTO _employeeroleID;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "STATUS", "PASSWORD") VALUES (_employeeroleID, 'Peter', 'Testcase', 'peter-testcase@email.com', TRUE, 'aaaaaa') RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES('Branch Amsterdam', 'amsterdam@cinemavenezia.nl', 2134567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES(_branchID, 'Cash desk 2') RETURNING deviceID INTO _deviceID;
  SELECT Barcode INTO _barcode from MovieCheque WHERE movieChequeID = _movieChequeID;
  INSERT INTO Product ("NAME", price) VALUES ('bier', 1.20::MONEY) RETURNING productID INTO _productID;
  INSERT INTO ProductInMovieCheque VALUES (_movieChequeID, _productID, 12);
  PERFORM fc_scan_product_from_moviecheque (_barcode, _employeeID, _branchID, _deviceID, ARRAY[ARRAY[_productID,14]]);
  IF EXISTS (SELECT 1 FROM productHistory WHERE oldAmount = 12 AND newAmount = 0) THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:         Use case 9 Use case 6 test 3                        */
/* Function:              Test the function from Use case 6 so it falls under */
/*              the first IF that calls to the Use Case 9 but now             */
/*              with arrays                                                   */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_uc_9_uc_6_test_3()
RETURNS BOOLEAN AS $BODY$
DECLARE _barcode TEXT;
    _employeeroleID INT;
    _employeeID INT;
    _branchID INT;
    _deviceID INT;
        _productID INT;
        _productID2 INT;
        _movieChequeID INT;
BEGIN
  INSERT INTO Operation (operationdescription) VALUES ('Changed the amount of the product');
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES(NULL, 5, 0::MONEY, (select fc_create_barcode(1)), TO_DATE('08-12-2030', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _movieChequeID;
  INSERT INTO EmployeeRole (roleName, roleDescription) VALUES ('Medewerker', 'Lorum ipsum fake text') RETURNING employeeroleID INTO _employeeroleID;
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "STATUS", "PASSWORD") VALUES (_employeeroleID, 'Peter', 'Testcase', 'peter-testcase@email.com', TRUE, 'aaaaaa') RETURNING employeeID INTO _employeeID;
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES('Branch Amsterdam', 'amsterdam@cinemavenezia.nl', 2134567890) RETURNING branchID INTO _branchID;
  INSERT INTO Device (branchID, "NAME") VALUES(_branchID, 'Cash desk 2') RETURNING deviceID INTO _deviceID;
  SELECT Barcode INTO _barcode from MovieCheque WHERE movieChequeID = _movieChequeID;
  INSERT INTO Product ("NAME", price) VALUES ('bier', 1.20::MONEY) RETURNING productID INTO _productID;
  INSERT INTO Product ("NAME", price) VALUES ('duurder bier', 2::MONEY) RETURNING productID INTO _productID2;
  INSERT INTO ProductInMovieCheque VALUES (_movieChequeID, _productID, 20);
  INSERT INTO ProductInMovieCheque VALUES (_movieChequeID, _productID2, 19);
  PERFORM fc_scan_product_from_moviecheque (_barcode, _employeeID, _branchID, _deviceID, ARRAY[ARRAY[_productID,18],ARRAY[_productID2,20]]);
  IF EXISTS (SELECT 1 FROM productHistory WHERE oldAmount = 20 AND newAmount = 2) THEN
    IF EXISTS (SELECT 1 FROM productHistory WHERE oldAmount = 19 AND newAmount = 0) THEN
        RETURN TRUE;
      ELSE
         RETURN FALSE;
      END IF;
  ELSE
      RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:         Use case 9 transaction movie test 1 UC7             */
/* Author:                Raymond de Bruine                                   */
/* Function:              Test function fc_transaction_movie                  */
/* What tested:       Test IF the transaction is saved in the history         */
/* When succeed:      If the right operationDescription can be found in the   */
/*                      history. This test goes directly to UC7               */
/*============================================================================*/

CREATE OR REPLACE FUNCTION fc_uc_9_history_transaction_for_movie_entrances_test_1()
RETURNS BOOLEAN AS $BODY$
DECLARE
  _arrangementID INT;
  _chequeID INT;
  _movieID INT;
  _operationID INT;
  _employeeRoleID INT;
  _employeeID INT;
  _branchID INT;
  _deviceID INT;
  _barcode TEXT;
BEGIN
  SELECT employeeRoleid INTO _employeeRoleID FROM employeeRole WHERE roleName = 'Medewerker';
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRoleID, 'firstname', 'surname', 'email@email.com', 'fakepassword', TRUE) RETURNING employeeid INTO _employeeID;
  _barcode := (SELECT fc_create_barcode(_employeeID));
  INSERT INTO Arrangement("NAME", price, expirationDate) VALUES('Arrangementnaam', 25::MONEY, TO_DATE('01-01-2028', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO MovieCheque(arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID, 0, 50::MONEY, _barcode, TO_DATE('31-12-2028', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _chequeID;
  INSERT INTO Movie("NAME", duration) VALUES ('test movie', 100) RETURNING movieID INTO _movieID;
  INSERT INTO MovieInArrangement(movieID, arrangementID, amount) VALUES (_movieID, _arrangementID, 6);
  SELECT operationID INTO _operationID FROM Operation WHERE operationDescription = 'Movie cheque created';
  INSERT INTO Branch("NAME", email, phoneNo) VALUES('uc 9 testbranch', 'testbranch@uc9transaction.com', 1234567890) RETURNING branchid INTO _branchID;
  INSERT INTO Device(branchid, "NAME") VALUES (_branchID, 'transactions desk') RETURNING deviceid INTO _deviceID;
  INSERT INTO MovieChequeHistory(employeeID, branchID, deviceID, operationID, movieChequeID_MH, date_MH, status_MH) VALUES(_employeeID, _branchID, _deviceID, _operationID, _chequeID, current_timestamp, TRUE);
  PERFORM fc_transaction_movie(_barcode, 2, _employeeID, _branchID, _deviceID);
  IF EXISTS (SELECT 1 FROM MovieChequeHistory MH INNER JOIN Operation O ON MH.operationID = O.operationID WHERE O.operationDescription = 'Transaction made for movie entrances' AND MH.moviechequeID_mh = _chequeID) THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:         Use case 9 transaction product test 1 UC7           */
/* Author:                Raymond de Bruine                                   */
/* Function:              Test function fc_transaction_product                */
/* What tested:       Test IF the transaction of product purchases is saved in*/
/*                      the history                                           */
/* When succeed:      If the right operationDescription is found in           */
/*                      MovieChequeHistory                                    */
/*============================================================================*/

CREATE OR REPLACE FUNCTION fc_uc_9_history_transaction_for_product_purchases_test_1()
RETURNS BOOLEAN AS $BODY$
DECLARE
  _arrangementID INT;
  _chequeID INT;
  _productID INT;
  _movieID INT;
  _operationID INT;
  _employeeRoleID INT;
  _employeeID INT;
  _branchID INT;
  _deviceID INT;
  _barcode TEXT;
BEGIN
  SELECT employeeRoleid INTO _employeeRoleID FROM employeeRole WHERE roleName = 'Medewerker';
  INSERT INTO Employee (employeeRoleID, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRoleID, 'firstname', 'surname', 'email@email.com', 'fakepassword', TRUE) RETURNING employeeid INTO _employeeID;
  _barcode := (SELECT fc_create_barcode(_employeeID));
  INSERT INTO Product ("NAME", price) VALUES ('Coca cola', 1.50::MONEY) RETURNING productID INTO _productID;
  INSERT INTO Arrangement("NAME", price, expirationDate) VALUES('Arrangementnaam', 25::MONEY, TO_DATE('01-01-2028', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO MovieCheque(arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES (_arrangementID, 0, 50::MONEY, _barcode, TO_DATE('31-12-2028', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeID INTO _chequeID;
  INSERT INTO Movie("NAME", duration) VALUES ('test movie', 100) RETURNING movieID INTO _movieID;
  INSERT INTO MovieInArrangement(movieID, arrangementID, amount) VALUES (_movieID, _arrangementID, 6);
  SELECT operationID INTO _operationID FROM Operation WHERE operationDescription = 'Movie cheque created';
  INSERT INTO Branch("NAME", email, phoneNo) VALUES('uc 9 testbranch', 'testbranch@uc9transaction.com', 1234567890) RETURNING branchid INTO _branchID;
  INSERT INTO Device(branchid, "NAME") VALUES (_branchID, 'transactions desk') RETURNING deviceid INTO _deviceID;
  INSERT INTO MovieChequeHistory(employeeID, branchID, deviceID, operationID, movieChequeID_MH, date_MH, status_MH) VALUES(_employeeID, _branchID, _deviceID, _operationID, _chequeID, current_timestamp, TRUE);
  PERFORM fc_transaction_product(_barcode, _productID, 4, _employeeID, _branchID, _deviceID);

  IF EXISTS (SELECT 1 FROM MovieChequeHistory MH INNER JOIN Operation O ON MH.operationID = O.operationID WHERE O.operationDescription = 'Transaction made for product purchases' AND MH.moviechequeID_mh = _chequeID) THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;


/*============================================================================*/
/* Use Case name:         Use case 9 test UC8                                 */
/* Function:              Test function fc_set_history_block_MovieCheque      */
/* Author:                Raymond de Bruine                                   */
/*============================================================================*/

CREATE OR REPLACE FUNCTION fc_uc_9_test_write_history_block_cheque_test_1()
RETURNS BOOLEAN AS $BODY$
DECLARE _arrangementID INT;
    _chequeID INT;
    _operationID INT;
    _employeeRoleID INT;
    _employeeID INT;
    _branchID INT;
    _deviceID INT;
BEGIN
  SELECT employeeRoleid INTO _employeeRoleID FROM EmployeeRole WHERE roleName = 'Medewerker';
  INSERT INTO Employee (employeeRoleid, firstname, surname, e_mail, "PASSWORD", "STATUS") VALUES (_employeeRoleID, 'Peter', 'Testcase', 'peter-testcase@email.com', 'fakepassword', TRUE) RETURNING employeeID INTO _employeeID;
  INSERT INTO Arrangement ("NAME", price, expirationDate) VALUES('Series night', 10::MONEY, TO_DATE('01-05-2028', 'DD-MM-YYYY')) RETURNING arrangementID INTO _arrangementID;
  INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS") VALUES(_arrangementid, 5, 25::MONEY, (SELECT fc_create_barcode(_employeeID)), TO_DATE('01-05-2028', 'DD-MM-YYYY'), TRUE) RETURNING movieChequeid INTO _chequeID;
  SELECT operationID INTO _operationID FROM Operation WHERE operationDescription = 'Movie cheque created';
  INSERT INTO Branch ("NAME", email, phoneNo) VALUES('Branch Amsterdam', 'amsterdam@cinemavenezia.nl', 2134567890) RETURNING branchid INTO _branchID;
  INSERT INTO Device (branchid, "NAME") VALUES(_branchID, 'Cash desk 2') RETURNING deviceid INTO _deviceID;
  INSERT INTO MovieChequeHistory (employeeID, branchid, deviceid, operationID, movieChequeid_MH, date_MH, status_MH) VALUES(_employeeID, _branchID, _deviceID, _operationID, _chequeID, current_timestamp, TRUE);
  PERFORM fc_block_movie_cheque(_chequeID, _employeeID, _deviceID, _branchID);
  IF EXISTS (SELECT 1 FROM MovieChequeHistory MH INNER JOIN Operation O ON MH.operationID = O.operationID WHERE O.operationDescription = 'Movie cheque is blocked' AND MH.movieChequeid_MH = _chequeID) THEN
    RETURN TRUE;
  ELSE
    RETURN FALSE;
  END IF;

  EXCEPTION WHEN OTHERS THEN
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;


-- Creating a temporary test table

DROP TABLE IF EXISTS TEST_TABLE;
CREATE TEMP TABLE TEST_TABLE (
  what_test text,
    Succesfull boolean
);

-- inserting tests of BR1 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_br_1_test_1', (SELECT fc_br_1_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_br_1_test_2', (SELECT fc_br_1_test_2()));
INSERT INTO TEST_TABLE VALUES ('fc_br_1_test_3', (SELECT fc_br_1_test_3()));
INSERT INTO TEST_TABLE VALUES ('fc_br_1_test_4', (SELECT fc_br_1_test_4()));

-- inserting tests of BR2 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_br_2_test_1', (SELECT fc_br_2_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_br_2_test_2', (SELECT fc_br_2_test_2()));

-- inserting tests of BR3 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_br_3_test_1', (SELECT fc_br_3_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_br_3_test_2', (SELECT fc_br_3_test_2()));
INSERT INTO TEST_TABLE VALUES ('fc_br_3_test_3', (SELECT fc_br_3_test_3()));

-- inserting tests of BR6 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_br_6_test_1', (SELECT fc_br_6_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_br_6_test_2', (SELECT fc_br_6_test_2()));

-- inserting tests of BR13 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_br_13_test_1', (SELECT fc_br_13_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_br_13_test_2', (SELECT fc_br_13_test_2()));

-- inserting tests of BR15 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_br_15_test_1', (SELECT fc_br_15_test_1()));

-- inserting tests of BR16 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_br_16_test_1', (SELECT fc_br_16_test_1()));

-- inserting tests of BR18 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_br_18_test_1', (SELECT fc_br_18_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_br_18_test_2', (SELECT fc_br_18_test_2()));
INSERT INTO TEST_TABLE VALUES ('fc_br_18_test_3', (SELECT fc_br_18_test_3()));
INSERT INTO TEST_TABLE VALUES ('fc_br_18_test_4', (SELECT fc_br_18_test_4()));

-- inserting tests of UC1 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_1_test_1', (SELECT fc_uc_1_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_1_test_2', (SELECT fc_uc_1_test_2()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_1_test_3', (SELECT fc_uc_1_test_3()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_1_test_4', (SELECT fc_uc_1_test_4()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_1_test_5', (SELECT fc_uc_1_test_5()));

-- inserting tests of UC2 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_2_test_1', (SELECT fc_uc_2_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_2_test_2', (SELECT fc_uc_2_test_2()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_2_test_3', (SELECT fc_uc_2_test_3()));

-- inserting tests of UC4 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_4_test_1', (SELECT fc_uc_4_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_4_test_2', (SELECT fc_uc_4_test_2()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_4_test_3', (SELECT fc_uc_4_test_3()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_4_test_4', (SELECT fc_uc_4_test_4()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_4_test_5', (SELECT fc_uc_4_test_5()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_4_test_6', (SELECT fc_uc_4_test_6()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_4_test_7', (SELECT fc_uc_4_test_7()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_4_test_8', (SELECT fc_uc_4_test_8()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_4_test_9', (SELECT fc_uc_4_test_9()));

-- inserting tests of UC5 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_5_test_1', (SELECT fc_uc_5_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_5_test_2', (SELECT fc_uc_5_test_2()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_5_test_3', (SELECT fc_uc_5_test_3()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_5_test_4', (SELECT fc_uc_5_test_4()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_5_test_5', (SELECT fc_uc_5_test_5()));

-- inserting tests of UC6 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_6_test_1', (SELECT fc_uc_6_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_6_test_2', (SELECT fc_uc_6_test_2()));

-- inserting tests of UC7 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_7_test_1', (SELECT fc_uc_7_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_7_test_2', (SELECT fc_uc_7_test_2()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_7_test_3', (SELECT fc_uc_7_test_3()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_7_test_4', (SELECT fc_uc_7_test_4()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_7_test_5', (SELECT fc_uc_7_test_5()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_7_test_6', (SELECT fc_uc_7_test_6()));

-- inserting tests of UC5 that use UC7 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_5_uc_7_test_1', (SELECT fc_uc_57_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_5_uc_7_test_2', (SELECT fc_uc_57_test_2()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_5_uc_7_test_3', (SELECT fc_uc_57_test_3()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_5_uc_7_test_4', (SELECT fc_uc_57_test_4()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_5_uc_7_test_5', (SELECT fc_uc_57_test_5()));

-- inserting tests of UC6 that use UC7 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_6_uc_7_test_1', (SELECT fc_uc_67_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_6_uc_7_test_2', (SELECT fc_uc_67_test_2()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_6_uc_7_test_3', (SELECT fc_uc_67_test_3()));

-- inserting tests of UC8 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_8_test_1', (SELECT fc_uc_8_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_8_test_2', (SELECT fc_uc_8_test_2()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_8_test_3', (SELECT fc_uc_8_test_3()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_8_test_4', (SELECT fc_uc_8_test_4()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_8_test_5', (SELECT fc_uc_8_test_5()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_8_test_6', (SELECT fc_uc_8_test_6()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_8_test_7', (SELECT fc_uc_8_test_7()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_8_test_8', (SELECT fc_uc_8_test_8()));

-- inserting tests of UC9 UC1 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_1_test_1', (SELECT fc_uc9_add_arrangement_to_history_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_1_test_2', (SELECT fc_uc9_add_product_arrangement_to_history_test_2()));

-- inserting tests of UC9 UC2 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_2_test_1', (SELECT fc_uc_9_test_insert_history_without_updated_values()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_2_test_2', (SELECT fc_uc_9_test_update_expirationdate_history()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_2_test_3', (SELECT fc_uc_9_test_update_product_history_multiple()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_2_test_4', (SELECT fc_uc_9_test_update_product_history_single()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_2_test_5', (SELECT fc_uc_9_test_update_product_and_expirationdate_history()));

-- inserting tests of UC9 UC4 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_4_test_1', (SELECT fc_uc_9_test_create_moviecheque_1()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_4_test_2', (SELECT fc_uc_9_test_create_moviecheque_2()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_4_test_3', (SELECT fc_uc_9_test_create_moviecheque_3()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_4_test_4', (SELECT fc_uc_9_test_create_moviecheque_4()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_4_test_5', (SELECT fc_uc_9_test_create_moviecheque_5()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_4_test_6', (SELECT fc_uc_9_test_create_moviecheque_6()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_4_test_7', (SELECT fc_uc_9_test_create_moviecheque_7()));

-- inserting tests of UC9 UC5 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_5_test_1', (SELECT fc_uc_9_entrance_history_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_5_test_2', (SELECT fc_uc_9_entrance_history_test_2()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_5_test_3', (SELECT fc_uc_9_entrance_history_test_3()));

-- inserting tests of UC9 UC7 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_6_test_1', (select fc_uc_9_uc_6_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_6_test_2', (select fc_uc_9_uc_6_test_2()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_6_test_3', (select fc_uc_9_uc_6_test_3()));

-- inserting tests of UC9 UC7 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_7_test_1', (SELECT fc_uc_9_history_transaction_for_movie_entrances_test_1()));
INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_7_test_2', (SELECT fc_uc_9_history_transaction_for_product_purchases_test_1()));

-- inserting tests of UC9 UC8 INTO Test Table

INSERT INTO TEST_TABLE VALUES ('fc_uc_9_uc_8_test_1', (SELECT fc_uc_9_test_write_history_block_cheque_test_1()));

-- selecting everything from the Test Table (testing everything)

SELECT * FROM TEST_TABLE;
