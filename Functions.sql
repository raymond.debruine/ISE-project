/*==============================================================*/
/* Business rule name:      Business rule 1                     */
/* Author:   Tom van Grinsven                                   */
/* Function:     The domain Price can NOT be below zero         */
/*==============================================================*/

ALTER DOMAIN PUBLIC.price
    ADD CONSTRAINT "chk_price_not_below_zero" CHECK (VALUE >= 0::MONEY);

/*==============================================================*/
/* Business rule name:      Business Rule 2                     */
/* Author:   Tom van Grinsven                                   */
/* Function:     The remaining amount domain which IS used      */
/* in table MovieCheque can NOT be below zero.                  */
/*==============================================================*/

ALTER DOMAIN public.remainingAmount
    ADD CONSTRAINT "chk_remainingAmount" CHECK (VALUE >= 0::MONEY);

/*==============================================================*/
/* Business rule name:      Business Rule 3                     */
/* Author:   Tom van Grinsven                                   */
/* Function:     When a MovieCheque IS updated                  */
/* for values of remaining amount OR entrances                  */
/* the expirationDate can NOT be before the current DATE        */
/*==============================================================*/

CREATE OR REPLACE FUNCTION fc_moviecheque_expirationdate_check()
RETURNS TRIGGER AS $BODY$
BEGIN
  IF (OLD.entrance <> NEW.entrance) THEN
      RAISE EXCEPTION 'This ticket has expired %', NEW.entrance
      USING HINT = 'This ticket has expired';
        RETURN OLD;

  ELSEIF (OLD.remainingAmount <> NEW.remainingAmount) THEN
      RAISE EXCEPTION 'This ticket has expired %', NEW.entrance
      USING HINT = 'This ticket has expired';
        RETURN OLD;
    END IF;

   RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql;

CREATE TRIGGER trg_moviecheque_expirationdate
  BEFORE UPDATE ON MovieCheque
  FOR EACH ROW
  WHEN (OLD.expirationDate < CURRENT_DATE)

EXECUTE PROCEDURE fc_moviecheque_expirationdate_check();

/*==============================================================*/
/* Business rule name:      Business Rule 6                     */
/* Author:                  Tom van Grinsven                    */
/* Function:     Barcode on MovieCheque IS unique               */
/*==============================================================*/

ALTER TABLE MovieCheque
    ADD CONSTRAINT "unq_barcode_is_unique" UNIQUE(barcode);

/*==============================================================*/
/* Business rule name:      Business Rule 13                    */
/* Author:                  Tom van Grinsven                    */
/* Function:     When the expirationDate of an Arrangement IS   */
/* updated, so must be the expirationDate of the MovieCheque    */
/* that IS linked.                                              */
/*==============================================================*/

CREATE OR REPLACE FUNCTION fc_moviecheque_update_expirationdate()
RETURNS TRIGGER AS $BODY$
BEGIN
  UPDATE MovieCheque SET expirationDate = NEW.expirationDate;
    RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql;

CREATE TRIGGER trg_arrangement_expirationdate_update
  BEFORE UPDATE
  ON Arrangement
  FOR EACH ROW
  WHEN (OLD.expirationDate <> NEW.expirationDate)
EXECUTE PROCEDURE fc_moviecheque_update_expirationdate();

/*==============================================================*/
/* Business rule name:      Business Rule 15                    */
/* Author:   Tom van Grinsven                                   */
/* Function:     The domain remainingAmount IS by default       */
/*  set to zero.                                                */
/*==============================================================*/

ALTER DOMAIN public.remainingAmount
    SET DEFAULT 0;

/*==============================================================*/
/* Business rule name:      Business Rule 16                    */
/* Author:                  Tom van Grinsven                    */
/* Function:     The domain entrances should be set to zero     */
/* on default                                                   */
/*==============================================================*/

ALTER DOMAIN public.entrance
  SET DEFAULT 0;

/*==============================================================*/
/* Business rule name:      Business Rule 18                    */
/* Author:                  Cees Verhoeven                      */
/* Function:     When a MovieCheque IS inserted OR updated      */
/* its expirationDate can NOT be before the current DATE        */
/*==============================================================*/

CREATE OR REPLACE FUNCTION fc_expirationdate_before_now_error()
  RETURNS trigger AS
$BODY$
BEGIN
   RAISE EXCEPTION 'The expirationDate IS before now %', new.expirationDate
    USING HINT = 'Please check the to be inserted expirationDate';
   RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql;

CREATE TRIGGER trg_expirationdate_not_before_current_date
  BEFORE INSERT OR UPDATE
  ON MovieCheque
  FOR EACH ROW
  WHEN (new.expirationDate < CURRENT_DATE)
  EXECUTE PROCEDURE fc_expirationdate_before_now_error();

/*============================================================================*/
/* Use Case:    Use Case 1                                                    */
/* Function:    Adds Arrangement to the database                              */
/* Parameter:   _branchID; the id of the branch                               */
/* Parameter:   _deviceID; the id of the device                               */
/* Parameter:   _employeeID; the id of the employee                           */
/* Parameter:   _NAME; the name of the Arrangement                            */
/* Parameter:   _price; the price of the Arrangement                          */
/* Parameter:   _expirationDate; the expiration DATE of a the Arrangement     */
/* Parameter:   _movieIDs[]; a list of the from the movies of an Arrangement  */
/* Parameter:   _amount; amount of movies in an Arrangement                   */
/* Parameter:   _products[][]; a list of products of an Arrangement           */
/*              The first dimension are the product id's                      */
/*              The second dimension are the amount of the choosen products   */
/* Author:      Kevin Dorlas                                                  */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_add_arrangement(_branchID INT, _deviceID INT, _employeeID INT, "_NAME" VARCHAR(65), _price MONEY, _expirationDate DATE, _movieIDs INT[], _amount INT, _products INT[][])
RETURNS INT AS $BODY$
DECLARE _arrangementID INT;
    _arrangementHistoryID INT;
BEGIN
  IF (SELECT fc_get_employee_role(_employeeID) = 'Marketingafdeling') THEN
    IF ((array_upper(_movieIDs, 1) IS NOT NULL) OR (array_upper(_products, 1) IS NOT NULL)) THEN

            INSERT INTO Arrangement ("NAME", price, expirationDate) /* AUTO INCREMENT: arrangementID */
                VALUES ("_NAME", _price::MONEY, _expirationDate)
                    RETURNING arrangementID INTO _arrangementID;

            IF (_arrangementID IS NOT NULL) THEN
                SELECT fc_add_arrangement_to_history(_branchID, _deviceID, _employeeID, _arrangementID, _expirationDate) INTO _arrangementHistoryID;

                IF (array_upper(_movieIDs, 1) IS NOT NULL) THEN
                    PERFORM fc_add_movies_in_arrangement(_arrangementID, _movieIDs, _amount);
                END IF;

                IF (array_upper(_products, 1) IS NOT NULL) THEN
                    PERFORM fc_add_products_in_arrangement(_arrangementHistoryID, _arrangementID, _products);
                END IF;

                RETURN _arrangementID;
            ELSE
              RAISE EXCEPTION 'The Arrangement IS NOT added.';
            END IF;
    ELSE
          RAISE EXCEPTION 'There are no movies OR products to add to the Arrangement.';
        END IF;
     ELSE
         RAISE EXCEPTION 'Employee has NOT the right role to execute this functionality.';
     END IF;

     RETURN NULL;

     EXCEPTION WHEN OTHERS THEN
       RAISE NOTICE 'Error occured, the Arrangement AND movies/prodcuts are NOT added.';
        RAISE NOTICE '% %', SQLERRM, SQLSTATE;
        RETURN NULL;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case:    Use Case 1                                                    */
/* Function:    Adds link between a movie AND an Arrangement in the database  */
/* Parameter:   _arrangementID; the id of an Arrangement                      */
/* Parameter:   _movieIDs[]; a list of the from the movies of an Arrangement  */
/* Parameter:   _amount; the amount of allowed entrances per movie            */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_add_movies_in_arrangement(_arrangementID INT, _movieIDs INT[], _amount INT)
RETURNS BOOLEAN AS $BODY$
BEGIN
    FOR i IN 1 .. array_upper(_movieIDs, 1)
        LOOP
            INSERT INTO MovieInArrangement (arrangementID, movieID, amount)
                VALUES (_arrangementID::INT, _movieIDs[i]::INT, _amount);
        END LOOP;
  RETURN TRUE;

     EXCEPTION WHEN OTHERS THEN
        RAISE EXCEPTION 'Error occured, the movieIDs are NOT added to the Arrangement.';
        RAISE NOTICE '% %', SQLERRM, SQLSTATE;
    RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case:    Use Case 1                                                    */
/* Function:    Adds link between a product AND an Arrangement in the database*/
/* Parameter:   _historyID_A; the id of an Arrangement in the history         */
/* Parameter:   _arrangementID; the id of an Arrangement                      */
/* Parameter:   _products[][]; a list of products of an Arrangement           */
/*              The first dimension are the product id's                      */
/*              The second dimension are the amount of the choosen products   */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_add_products_in_arrangement(_historyID_A INT, _arrangementID INT, _products INT[][])
RETURNS BOOLEAN AS $BODY$
BEGIN
  FOR i IN 1 .. array_upper(_products, 1)
        LOOP
             INSERT INTO ProductInArrangement (arrangementID, productID, amount)
                 VALUES (_arrangementID, _products[i][1], _products[i][2]);
             PERFORM fc_add_product_to_arrangement_history(_historyID_A, _products[i][1], _products[i][2]);
        END LOOP;
    RETURN TRUE;

    EXCEPTION WHEN OTHERS THEN
        RAISE EXCEPTION 'Error occured, the products are NOT added to the Arrangement.';
        RAISE NOTICE '% %', SQLERRM, SQLSTATE;
        RETURN FALSE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 2                                            */
/* Function:            Updates an Arrangement with IF included               */
/*                        their products AND amount of those products         */
/* Parameter:            _inputArrangementID; the id of the Arrangement       */
/* Parameter:           _inputName; the (new) name of the Arrangement         */
/* Parameter:           _inputPrice; the (new) price of the Arrangement       */
/* Parameter:           _inputExpirationDate; the (new) expirationDate        */
/*                        of the Arrangement                                  */
/* Parameter:           _inputProducts; the list of products AND their        */
/*                        (new) amounts                                       */
/* Parameter:           _inputEmployeeID; the id of the employee              */
/* Parameter:           _inputDeviceID; the id of the device                  */
/* Parameter:           _inputBranchID; the id of the branch                  */
/*============================================================================*/

-- Function that updates arrangements AND IF present their products AND amounts. Takes a 2d array as parameter to loop over the products AND their amounts
CREATE OR REPLACE FUNCTION fc_update_arrangement(_inputArrangementID INT, _inputName TEXT, _inputPrice MONEY, _inputExpirationDate DATE, _inputProducts INT[][], _inputEmployeeID INT, _inputDeviceID INT, _inputBranchID INT)
RETURNS VOID AS $BODY$
DECLARE _oldExpirationDate DATE;
DECLARE _oldAmount INT;
BEGIN
  SELECT expirationDate INTO _oldExpirationDate FROM Arrangement WHERE arrangementID = _inputArrangementID;
      PERFORM fc_history_update_arrangement_expirationdate(_inputArrangementID, _oldExpirationDate, _inputExpirationDate, _inputEmployeeID, _inputDeviceID, _inputBranchID);
    UPDATE Arrangement SET "NAME" = _inputName, price = _inputPrice, expirationDate = _inputExpirationDate WHERE arrangementID = _inputArrangementID;
        IF array_length(_inputProducts, 1) > 0 THEN
          FOR i IN array_lower(_inputProducts, 1) .. array_upper(_inputProducts, 1)
            LOOP
              IF EXISTS(SELECT * FROM ProductInArrangement WHERE productID = _inputProducts[i][1] AND arrangementID = _inputArrangementID) THEN
            SELECT amount INTO _oldAmount FROM ProductInArrangement WHERE productID = _inputProducts[i][1] AND arrangementID = _inputArrangementID;
              PERFORM fc_history_update_arrangement_product(_inputArrangementID, _inputProducts[i][1], _oldAmount, _inputProducts[i][2], _inputEmployeeID, _inputDeviceID, _inputBranchID);
            UPDATE ProductInArrangement set amount = _inputProducts[i][2] WHERE productID = _inputProducts[i][1] AND arrangementID = _inputArrangementID;
                ELSE
              RAISE EXCEPTION 'The product with this id does NOT exists :,%', _inputProducts[i][1];
            END IF;
      END LOOP;
        END IF;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case:      Use Case 4                                                  */
/* Function:      Adds a new MovieCheque with a unique barcode                */
/* Parameter:     _barcode; given in the function, to find the _chequeID      */
/* Parameter:     _employeeID; given through the application, for history     */
/* Parameter:     _deviceID; given through the application, for history       */
/* Parameter:     _oldRemainingAmount; the remainingAmount of the cheque      */
/*                  before the transaction IS made                            */
/* Parameter:     _price; the price the entrances to calculate the new amount */
/*=============================================================================*/

-- Procedure to INSERT a new MovieCheque
CREATE OR REPLACE FUNCTION fc_add_movieCheque(_employeeID INT, _arrangementID INT, _entrance INT, _remainingAmount MONEY, _deviceID INT, _branchID INT)
RETURNS TEXT AS $BODY$
DECLARE _barcode VARCHAR(65);
    _movieChequeID INT;
    _entranceOfArrangement INT;
    temprow ProductInArrangement%rowtype;
BEGIN
  -- create barcode INTO variable
  SELECT fc_create_barcode(_employeeID)
      INTO _barcode;

    IF(_entrance IS NULL) THEN
      _entrance := 0;
    END IF;

    -- IF an Arrangement was passed THEN
  IF(_arrangementID IS NOT NULL) THEN
      -- Count the number of movies listed on that Arrangement to set the number of entrances
      SELECT SUM(amount) INTO _entranceOfArrangement FROM MovieInArrangement WHERE arrangementID = _arrangementID;
        -- If the number of entrances IS bigger than zero set this number als entrances on the new MovieCheque
        IF(_entranceOfArrangement > 0) THEN
          _entrance := _entranceOfArrangement;
        END IF;

      -- Insert the new MovieCheque with the number of entrances given to the function
      INSERT INTO MovieCheque (arrangementID, entrance, remainingAmount, barcode, expirationDate, "STATUS")
          VALUES (_arrangementID, _entrance, _remainingAmount::MONEY, _barcode, (SELECT expirationDate FROM Arrangement WHERE arrangementID = _arrangementID), TRUE)
              RETURNING movieChequeID INTO _movieChequeID;
      -- Insert the products listed on the Arrangement in the productinmoviecheque table
      FOR temprow IN
          SELECT * FROM ProductInArrangement where arrangementID = _arrangementID
              LOOP
                  INSERT INTO ProductInMoviecheque(movieChequeID, productID, amount) VALUES(_movieChequeID, temprow.productID, temprow.amount);
              END LOOP;
      -- Insert the newly made MovieCheque with its Arrangement INTO history
      PERFORM fc_history_add_moviecheque_with_arrangement(_movieChequeID, _employeeID, _deviceID, _branchID, _entrance, _remainingAmount);
      -- If there IS no Arrangement given INSERT the MovieCheque with its number of entrances AND remaining amount (no products AND no set number of entrances are listed on this MovieCheque)
      ELSE
        INSERT INTO MovieCheque (entrance, remainingAmount, barcode, expirationDate, "STATUS")
          VALUES (_entrance::INT, _remainingAmount::MONEY, _barcode, CURRENT_DATE + interval '10 years', TRUE)
            RETURNING movieChequeID INTO _movieChequeID;
        PERFORM fc_history_add_moviecheque(_movieChequeID, _employeeID, _deviceID, _branchID, _entrance, _remainingAmount);
      END IF;

    RETURN _barcode;

    EXCEPTION WHEN OTHERS THEN
        RAISE EXCEPTION 'Error occurred, the Movie Cheque IS NOT added.';
        RAISE NOTICE '% %', SQLERRM, SQLSTATE;

  RETURN NULL;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 5                                            */
/* Function:            This function checks IF the choosen movie IS in       */
/*                        the Arrangement                                     */
/* Author:              Cees Verhoeven                                        */
/* Parameter:           _movieID -> the movie the customer wants to visit     */
/* Parameter:           _arrangementID -> the Arrangement on the barcode      */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_check_movie_in_arrangement(_movieID INT, _arrangementID INT)
RETURNS BOOLEAN AS $BODY$
    BEGIN
    IF EXISTS (SELECT 1 FROM MovieinArrangement MO WHERE MO.movieID = _movieID AND MO.arrangementID = _arrangementID) THEN
        RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 5                                            */
/* Function:            Returns true IF the MovieCheque IS NOT blocked,       */
/*                      returns false IF the MovieCheque IS blocked           */
/* Author:              Cees Verhoeven                                        */
/* Parameter:   _barcode -> the scanned barcode of the movie cheque           */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_check_if_moviecheque_is_blocked(_barcode TEXT)
RETURNS BOOLEAN AS $BODY$
BEGIN
    IF EXISTS (SELECT 1 FROM MovieCheque MO WHERE MO.barcode = _barcode AND MO."STATUS" = TRUE) THEN
        RETURN FALSE;
    ELSE
        RETURN TRUE;
    END IF;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 5                                            */
/* Function:            Returns true IF the MovieCheque IS NOT expired,       */
/*                      returns false IF the MovieCheque IS expired           */
/* Author:              Cees Verhoeven                                        */
/* Parameter:   _barcode -> the scanned barcode of the movie cheque           */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_check_if_moviecheque_is_not_expired(_barcode TEXT)
RETURNS BOOLEAN AS $BODY$
BEGIN
    IF EXISTS (SELECT 1 FROM MovieCheque MO WHERE MO.barcode = _barcode AND MO.expirationDate >= CURRENT_TIMESTAMP) THEN
        RETURN FALSE;
    ELSE
        RETURN TRUE;
    END IF;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 5                                            */
/* Function:            Returns the Arrangement from a barcode                */
/* Author:              Cees Verhoeven                                        */
/* Parameter:          _barcode -> the scanned barcode of the movie cheque    */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_get_arrangement_from_barcode(_barcode TEXT)
RETURNS INT AS $BODY$
DECLARE _arrangementID INT;
BEGIN
    IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND arrangementID IS NOT NULL) THEN
        SELECT arrangementID INTO _arrangementid FROM MovieCheque WHERE barcode = _barcode;
        RETURN _arrangementID;
    ELSE
        RETURN NULL;
    END IF;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 5                                            */
/* Function:            Scans a MovieCheque AND returns the amount that needs */
/*                        to be paid                                          */
/* Author:              Cees Verhoeven                                        */
/* Parameter:         _barcode -> the scanned barcode of the movie cheque     */
/* Parameter:         _employeeID -> the current employee                     */
/* Parameter:         _branchID -> the current branch where the device IS     */
/* Parameter:         _deviceID -> the current device, what the employee uses */
/* Parameter:         _entrance -> number of entrances the customer wants     */
/* Parameter:         _movieID -> the movie the customer wants to visit       */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_scan_moviecheque(_barcode TEXT, _employeeID INT, _branchID INT, _deviceID INT, _entrance INT, _movieID INT)
RETURNS MONEY AS $BODY$
DECLARE _entranceThatNeedToBePaid INT;
      _oldEntranceAmount INT;
        _entrancesLeft INT;
BEGIN
  -- When a MovieCheque IS blocked it will fall under this IF
    IF (fc_check_if_moviecheque_is_blocked(_barcode)) THEN
      RAISE EXCEPTION 'This ticket has been blocked %', _barcode;
    -- When a MovieCheque IS expired it will fall under this IF
    ELSEIF (fc_check_if_moviecheque_is_not_expired(_barcode)) THEN
      RAISE EXCEPTION 'This ticket has been expired %', _barcode;
    END IF;

  -- Gets old amount of entrances linked to the scanned barcode
    SELECT entrance INTO _oldEntranceAmount FROM MovieCheque WHERE barcode = _barcode;
    -- When a MovieCheque has no Arrangement AND wants more entrances THEN it has on the MovieCheque it will fall under this IF
    IF EXISTS (SELECT 1 FROM MovieCheque MO WHERE MO.barcode = _barcode AND MO.entrance - _entrance < 0 AND MO.arrangementId IS NULL) THEN
      SELECT MovieCheque.entrance INTO _entranceThatNeedToBePaid FROM MovieCheque WHERE MovieCheque.barcode = _barcode;
      _entranceThatNeedToBePaid = _entrance - _entranceThatNeedToBePaid;
      UPDATE MovieCheque SET entrance = 0 WHERE barcode = _barcode;

        -- Inserts a new row INTO Movie Cheque History AND a new row INTO Entrances History with the same historyID
        PERFORM fc_add_entrance_to_history(_employeeID, _branchID, _deviceID, _barcode, _oldEntranceAmount, 0);

      RETURN fc_transaction_movie(_barcode, _entranceThatNeedToBePaid, _employeeID, _deviceID, _branchID);

    -- When a MovieCheque has no Arrangement AND wants to enter a movie with the MovieCheque while the entrances on the MovieCheque are more than asked it will fall under this IF
    ELSEIF EXISTS (SELECT 1 FROM MovieCheque MO WHERE MO.barcode = _barcode AND MO.entrance - _entrance >= 0 AND MO.arrangementId IS NULL) THEN
        UPDATE MovieCheque SET entrance = entrance - _entrance WHERE barcode = _barcode;
        SELECT entrance INTO _entrancesLeft FROM MovieCheque WHERE barcode = _barcode;

        -- Inserts a new row INTO Movie Cheque History AND a new row INTO Entrances History with the same historyID
        PERFORM fc_add_entrance_to_history(_employeeID, _branchID, _deviceID, _barcode, _oldEntranceAmount, _entrancesLeft);

    -- When there IS a MovieCheque with an Arrangement AND the asked entrances are less THEN the entrances on the MovieCheque it will fall under this IF
    ELSEIF EXISTS (SELECT 1 FROM MovieCheque MO WHERE MO.arrangementId IS NOT NULL AND MO.barcode = _barcode AND MO.entrance - _entrance >= 0 AND _movieID IS NOT NULL) THEN
        -- When the Arrangement includes the to be visited movie it will fall under this IF
        IF (fc_check_movie_in_arrangement(_movieID, fc_get_arrangement_from_barcode(_barcode))) THEN
            UPDATE MovieCheque SET entrance = entrance - _entrance WHERE barcode = _barcode;
            SELECT entrance INTO _entrancesLeft FROM MovieCheque WHERE barcode = _barcode;

            -- Inserts a new row INTO Movie Cheque History AND a new row INTO Entrances History with the same historyID
          PERFORM fc_add_entrance_to_history(_employeeID, _branchID, _deviceID, _barcode, _oldEntranceAmount, _entrancesLeft);

        --When the Arrangement has no movies linked to it, it will fall under this IF
        ELSEIF NOT EXISTS (SELECT 1 FROM MovieinArrangement mia WHERE mia.arrangementID = fc_get_arrangement_from_barcode(_barcode)) THEN
            UPDATE MovieCheque SET entrance = entrance - _entrance WHERE barcode = _barcode;
            SELECT entrance INTO _entrancesLeft FROM MovieCheque WHERE barcode = _barcode;

            -- Inserts a new row INTO Movie Cheque History AND a new row INTO Entrances History with the same historyID
          PERFORM fc_add_entrance_to_history(_employeeID, _branchID, _deviceID, _barcode, _oldEntranceAmount, _entrancesLeft);

        -- When both of these conditions aren't valid it will get this elseif AND it tries to pay from the MovieCheque
        ELSEIF (fc_check_movie_in_arrangement(_movieID, fc_get_arrangement_from_barcode(_barcode)) = false) THEN
            RETURN fc_transaction_movie(_barcode, _entrance, _employeeID, _deviceID, _branchID);

        ELSE
            RAISE EXCEPTION 'This should NOT happen %', _barcode;
        END IF;
    -- When a MovieCheque has an Arrangement AND wants more entrances THEN it has on the MovieCheque it will fall under this IF
  ELSEIF EXISTS (SELECT 1 FROM MovieCheque MO WHERE MO.arrangementId IS NOT NULL AND MO.barcode = _barcode AND MO.entrance - _entrance < 0 AND _movieID IS NOT NULL) THEN
  -- When the given movieID IS linked to the Arrangement
      IF fc_check_movie_in_arrangement(_movieID, fc_get_arrangement_from_barcode(_barcode)) THEN

          SELECT MovieCheque.entrance INTO _entranceThatNeedToBePaid FROM MovieCheque WHERE MovieCheque.barcode = _barcode;
          _entranceThatNeedToBePaid := _entrance - _entranceThatNeedToBePaid;

          UPDATE MovieCheque SET entrance = 0 WHERE barcode = _barcode;

          -- Inserts a new row INTO Movie Cheque History AND a new row INTO Entrances History with the same historyID
          PERFORM fc_add_entrance_to_history(_employeeID, _branchID, _deviceID, _barcode, _oldEntranceAmount, 0);

          RETURN fc_transaction_movie(_barcode, _entranceThatNeedToBePaid, _employeeID, _deviceID, _branchID);
     -- When the given arrangementID isn't linked in the MovieinArrangement table
        ELSEIF NOT EXISTS (SELECT 1 FROM MovieinArrangement mia WHERE mia.arrangementID = fc_get_arrangement_from_barcode(_barcode)) THEN
      SELECT MovieCheque.entrance INTO _entranceThatNeedToBePaid FROM MovieCheque WHERE MovieCheque.barcode = _barcode;
          _entranceThatNeedToBePaid := _entrance - _entranceThatNeedToBePaid;

          UPDATE MovieCheque SET entrance = 0 WHERE barcode = _barcode;

          -- Inserts a new row INTO Movie Cheque History AND a new row INTO Entrances History with the same historyID
          PERFORM fc_add_entrance_to_history(_employeeID, _branchID, _deviceID, _barcode, _oldEntranceAmount, 0);

          RETURN fc_transaction_movie(_barcode, _entranceThatNeedToBePaid, _employeeID, _deviceID, _branchID);
    -- When the Arrangement doesn't include the to be visited movie it will fall under this IF
      ELSEIF (fc_check_movie_in_arrangement(_movieID, fc_get_arrangement_from_barcode(_barcode)) = false) THEN
        RETURN fc_transaction_movie(_barcode, _entrance, _employeeID, _deviceID, _branchID);

      ELSE
          RAISE EXCEPTION 'This should NOT happen %', _barcode;
      END IF;
    ELSE
        RAISE EXCEPTION 'This should NOT happen %', _barcode;
    END IF;
  RETURN 0::MONEY;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 6                                            */
/* Function:            Scans a product AND a barcode AND returns the amount  */
/*                      that needs to be paid                                 */
/* Author:              Cees Verhoeven                                        */
/* Parameter:   _barcode -> the scanned barcode of the movie cheque           */
/* Parameter:   _employeeID -> the current employee                           */
/* Parameter:   _branchID -> the current branch WHERE the device IS           */
/* Parameter:   _deviceID -> the current device, what the employee uses       */
/* Parameter:   _products[][] -> the products the customer wants AND the      */
/*                   amounts                                                  */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_scan_product_from_moviecheque(_barcode TEXT, _employeeID INT, _branchID INT, _deviceID INT, _products INT[][])
RETURNS MONEY AS $BODY$
DECLARE _toBePaidAmount INT;
    _toBePaid MONEY;
    _newAmount INT;
        _productID INT;
        _productAmount INT;
BEGIN
  _productID := 1;
  _productAmount := 2;
  _toBePaid := 0;

  IF (fc_check_if_moviecheque_is_blocked(_barcode)) THEN
         RAISE EXCEPTION 'This ticket has been blocked %', _barcode
        USING HINT = 'Please check MovieCheque because it IS blocked';
    ELSEIF (fc_check_if_moviecheque_is_not_expired(_barcode)) THEN
         RAISE EXCEPTION 'This ticket has been expired %', _barcode
        USING HINT = 'Please check MovieCheque because it IS expired';
    END IF;

    FOR i IN array_lower(_products, 1) .. array_upper(_products, 1)
    LOOP
    _newAmount := 0;
    _toBePaidAmount := 0;
    IF EXISTS (SELECT 1 FROM ProductInMovieCheque PIM INNER JOIN MovieCheque MO ON PIM.moviechequeID = MO.moviechequeID WHERE MO.barcode = _barcode AND PIM.amount < _products[i][_productAmount] AND PIM.productID = _products[i][_productID]) THEN
          SELECT PIM.amount INTO _toBePaidAmount FROM ProductInMovieCheque PIM INNER JOIN MovieCheque MO ON PIM.moviechequeID = MO.moviechequeID WHERE MO.barcode = _barcode AND PIM.productID = _products[i][_productID];

        _toBePaidAmount := _products[i][_productAmount] -_toBePaidAmount;

        PERFORM fc_history_products_moviecheque(_barcode, _products[i][_productID], 0, _employeeID, _deviceID, _branchID);
        UPDATE ProductInMovieCheque AS PIM set amount = 0 FROM MovieCheque MO WHERE PIM.moviechequeID = MO.moviechequeID AND MO.barcode = _barcode AND PIM.productID = _products[i][_productID];
        _toBePaid := _toBePaid + fc_transaction_product(_barcode, _products[i][_productID], _toBePaidAmount, _employeeID, _deviceID, _branchID);

    ELSEIF EXISTS (SELECT 1 FROM ProductInMovieCheque PIM INNER JOIN MovieCheque MO ON PIM.moviechequeID = MO.moviechequeID WHERE MO.barcode = _barcode AND PIM.amount >= _products[i][_productAmount] AND PIM.productID = _products[i][_productID]) THEN
          SELECT amount INTO _newAmount FROM ProductInMovieCheque PIM INNER JOIN MovieCheque MO ON PIM.moviechequeID = MO.moviechequeID WHERE MO.barcode = _barcode AND PIM.productID = _products[i][_productID];

        _newAmount := _newAmount - _products[i][_productAmount];

        PERFORM fc_history_products_moviecheque(_barcode, _products[i][_productID], _newAmount, _employeeID, _deviceID, _branchID);
        UPDATE ProductInMovieCheque AS PIM set amount = _newAmount FROM MovieCheque AS MO WHERE PIM.moviechequeID = MO.moviechequeID AND MO.barcode = _barcode AND PIM.productID = _products[i][_productID];

    ELSEIF NOT EXISTS (SELECT 1 FROM ProductInMovieCheque PIM INNER JOIN MovieCheque MO ON PIM.moviechequeID = MO.moviechequeID WHERE MO.barcode = _barcode AND PIM.productID = _products[i][_productID]) THEN
        _toBePaid := _toBePaid + fc_transaction_product(_barcode, _products[i][_productID], _products[i][_productAmount], _employeeID, _deviceID, _branchID);

    ELSE
        RAISE EXCEPTION 'This should NOT happen %', _barcode;
        END IF;
    END LOOP;

    RETURN _toBePaid;
END;
$BODY$ language plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 7                                            */
/* Function:            Calculates transactions for of the movie cheque when  */
/*                      a customer wants to pay the movie from the cheque     */
/* Author:              Cees Verhoeven                                        */
/* Parameter:   _barcode -> the scanned barcode of the movie cheque           */
/* Parameter:   _toBePaidEntrance -> number of entrances the customer wants   */
/* Parameter:   _employeeID -> the ID of the employee that is currently       */
/*                 using this function                                        */
/* Parameter:   _deviceID -> the current device, what the employee uses       */
/* Parameter:   _branchID -> the current branch where the device is           */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_transaction_movie(_barcode TEXT, _toBePaidEntrance INT, _employeeID INT, _branchID INT, _deviceID INT)
RETURNS MONEY AS $BODY$
DECLARE _cost MONEY;
    _returningAmount MONEY;
    _remainingAmount MONEY;
        _movieCost MONEY;
BEGIN
  _movieCost := 10.20;
  _cost := _toBePaidEntrance * _movieCost;
  SELECT remainingAmount INTO _remainingAmount FROM MovieCheque WHERE barcode = _barcode;

  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND remainingAmount = 0::MONEY) THEN
    RETURN _cost;

  ELSEIF EXISTS(SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND remainingAmount >= _cost) THEN

    UPDATE MovieCheque SET remainingAmount = remainingAmount - _cost WHERE barcode = _barcode;
    PERFORM fc_transaction_movie_to_history(_barcode, _employeeID, _branchID, _deviceID, _remainingAmount, _cost);

  RETURN 0::MONEY;

  ELSEIF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND remainingAmount < _cost) THEN

    _returningAmount := _cost - _remainingAmount;
    UPDATE MovieCheque SET remainingAmount = 0::MONEY WHERE barcode = _barcode;
    PERFORM fc_transaction_movie_to_history(_barcode, _employeeID, _branchID, _deviceID, _remainingAmount, _cost);

  RETURN _returningAmount;

  ELSE
    RAISE EXCEPTION 'This should not happen %', _barcode;
  END IF;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 7                                            */
/* Function:            Calculates transactions for of the movie cheque when  */
/*                      a customer wants to pay products from the cheque      */
/* Author:              Cees Verhoeven                                        */
/* Parameter:   _barcode -> the scanned barcode of the movie cheque           */
/* Parameter:   _productID -> the ID of the to be paid product                */
/* Parameter:   _amountOfProduct -> The amount of the to be paid product      */
/* Parameter:   _employeeID -> the ID of the employee that is currently       */
/*                 using this function                                        */
/* Parameter:   _deviceID -> the current device, what the employee uses       */
/* Parameter:   _branchID -> the current branch where the device is           */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_transaction_product(_barcode TEXT, _productID INT, _amountOfProduct INT, _employeeID INT, _branchID INT, _deviceID INT)
RETURNS MONEY AS $BODY$
DECLARE _productCost MONEY;
    _cost MONEY;
    _returningAmount MONEY;
    _remainingAmount MONEY;
BEGIN
  SELECT price INTO _productCost FROM product WHERE productID = _productID;

  _cost := _productCost * _amountOfProduct;
  SELECT remainingamount INTO _remainingAmount FROM moviecheque WHERE barcode = _barcode;
  IF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND remainingAmount = 0::MONEY) THEN
    RETURN _cost;

  ELSEIF EXISTS(SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND remainingAmount >= _cost) THEN
    UPDATE MovieCheque SET remainingAmount = remainingAmount - _cost WHERE barcode = _barcode;
    PERFORM fc_transaction_products_to_history(_barcode, _employeeID, _branchID, _deviceID, _remainingAmount, _cost);
    RETURN 0;

  ELSEIF EXISTS (SELECT 1 FROM MovieCheque WHERE barcode = _barcode AND remainingAmount < _cost) THEN
    _returningAmount := _cost - _remainingAmount;
    UPDATE MovieCheque SET remainingAmount = 0 WHERE barcode = _barcode;
    PERFORM fc_transaction_products_to_history(_barcode, _employeeID, _branchID, _deviceID, _remainingAmount, _cost);
    RETURN _returningAmount;

  ELSE
    RAISE EXCEPTION 'This should not happen %', _barcode;
  END IF;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:       Use Case 8                                            */
/* Function:            Blocks a movie cheque. A normal cashier can block     */
/*                        cashier can block a movie cheque within 10 minutes, */
/*                        marketing OR financial employee can always block a  */
/*                        movie cheque                                        */
/* Author:            Raymond de Bruine                                       */
/* Param:             _chequeID -> ID of the current cheque                   */
/* Param:             _employeeID -> ID of the current employee, to check the */
/*                    role of the employees                                   */
/* Param:    _deviceID -> ID of the current device, for management reports    */
/* Param:    _branchID -> ID of the current branch, for management reports    */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_block_movie_cheque(_chequeID INT, _employeeID INT, _deviceID INT, _branchID INT)
RETURNS VOID AS $$
DECLARE _creationDate TIMESTAMP;
    _roleEmployee VARCHAR;
BEGIN
  -- First, get the creation timestamp of the MovieCheque.
  SELECT M.date_MH INTO _creationDate
  FROM MovieChequeHistory M INNER JOIN Operation O ON M.operationID = O.operationID
  WHERE movieChequeID_MH = _chequeID AND operationDescription = 'Movie cheque created';
  -- Get employee role with hard coded function from DDL
  SELECT fc_get_employee_role(_employeeID) INTO _roleEmployee;
  -- Check IF MovieCheque IS already blocked
  IF EXISTS (SELECT 1 FROM MovieCheque WHERE "STATUS" = FALSE AND movieChequeID = _chequeID) THEN
    RAISE EXCEPTION 'The movie cheque IS already blocked';
  ELSE
    -- Check for employee role, normal employees are alowed to block a MovieCheque within 10 minutes.
    IF  (_roleEmployee = 'Medewerker') THEN
      IF (CURRENT_TIMESTAMP <= (_creationDate + TIME '00:10')) THEN
          -- UPDATE status of MovieCheque.
          UPDATE MovieCheque SET "STATUS" = FALSE WHERE movieChequeID = _chequeID;
          RAISE NOTICE 'Movie cheque IS blocked';
          PERFORM fc_set_history_block_moviecheque(_chequeID, _employeeID, _deviceID, _branchID, FALSE);
      ELSE
          RAISE EXCEPTION 'The movie cheque can NOT be blocked.'
          USING HINT = 'Only a supervisor can block the movie cheque.';
      END IF;

    ELSEIF (_roleEmployee = 'Marketingafdeling' OR _roleEmployee = 'Financiele administratie') THEN
      UPDATE MovieCheque SET "STATUS" = FALSE WHERE movieChequeID = _chequeID;
      PERFORM fc_set_history_block_moviecheque(_chequeID, _employeeID, _deviceID, _branchID, FALSE);
      RAISE NOTICE 'Movie cheque IS blocked';
    ELSE
      RAISE EXCEPTION 'Movie cheque can NOT be blocked, the current user IS NOT authenticated';
    END IF;
  END IF;
END;
$$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case:      Use Case 9 UC1                                              */
/* Function:      Adds Arrangement to the history database                    */
/* Parameter:     _branchID; the id of the branch                             */
/* Parameter:     _deviceID; the id of the device                             */
/* Parameter:     _employeeID; the id of the employee                         */
/* Parameter:     _arrangementID_AH; the id of the Arrangement for history    */
/* Author:        Kevin Dorlas                                                */
/*=============================================================================*/
CREATE OR REPLACE FUNCTION fc_add_arrangement_arrangement_history(_branchID INT, _deviceID INT, _employeeID INT, _arrangementID_AH INT)
RETURNS INT AS $BODY$
DECLARE _historyID INT;
    _operationID INT;
BEGIN
  _operationID := 3;

    INSERT INTO ArrangementHistory(branchID, deviceID, employeeID, operationID, arrangementID_AH, date_AH)
      VALUES (_branchID, _deviceID, _employeeID, _operationID, _arrangementID_AH, CURRENT_TIMESTAMP)
          RETURNING historyID_A INTO _historyID;

  RETURN _historyID;

    EXCEPTION WHEN OTHERS THEN
        RAISE NOTICE 'Error occured, the INSERT to the Arrangement History table went wrong.';
        RAISE NOTICE '% %', SQLERRM, SQLSTATE;
    RETURN NULL;
END;
$BODY$ LANGUAGE plpgsql STRICT;

/*============================================================================*/
/* Use Case:   Use Case 9 UC1                                                 */
/* Function:   Adds experation DATE of the Arrangement to the history database*/
/* Parameter:  _historyID; the id of the history linked to the Arrangement    */
/* Parameter:  _expirationDate; the expiration DATE of a the Arrangement      */
/* Author:     Kevin Dorlas                                                   */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_add_arrangement_exp_history(_historyID INT, _expirationDate DATE)
RETURNS VOID AS $BODY$
DECLARE
BEGIN
    INSERT INTO ExpArrangementH (historyID_A, oldExpirationDate_A, newExpirationDate_A)
      VALUES (_historyID, NULL, _expirationDate::DATE);

    EXCEPTION WHEN OTHERS THEN
        RAISE NOTICE 'Error occured, the INSERT to the Expiration Arrangement History table went wrong.';
        RAISE NOTICE '% %', SQLERRM, SQLSTATE;
END;
$BODY$ LANGUAGE plpgsql STRICT;

/*============================================================================*/
/* Use Case:    Use Case 9 UC1                                                */
/* Function:    Adds Arrangement included the expiration DATE of the          */
/*                Arrangement to the history database                         */
/* Parameter:  _branchID; the id of the branch                                */
/* Parameter:  _deviceID; the id of the device                                */
/* Parameter:  _employeeID; the id of the employee                            */
/* Parameter:  _arrangementID_AH; the id of linked Arrangement in the history */
/* Parameter:  _expirationDate; the expiration DATE of a the Arrangement      */
/* Author:      Kevin Dorlas                                                  */
/*=============================================================================*/
CREATE OR REPLACE FUNCTION fc_add_arrangement_to_history(_branchID INT, _deviceID INT, _employeeID INT, _arrangementID_AH INT, _expirationDate DATE)
RETURNS INT AS $BODY$
DECLARE
  _historyID INT;
  _operationID INT;
BEGIN
  _operationID := 3;

  SELECT fc_add_arrangement_arrangement_history(_branchID, _deviceID, _employeeID, _arrangementID_AH) INTO _historyID;
    PERFORM fc_add_arrangement_exp_history(_historyID, _expirationDate);

    RETURN _historyID;

    EXCEPTION WHEN OTHERS THEN
        RAISE NOTICE 'Error occured, the Arrangement IS NOT added to the history.';
        RAISE NOTICE '% %', SQLERRM, SQLSTATE;
      RETURN NULL;
END;
$BODY$ LANGUAGE plpgsql STRICT;

/*============================================================================*/
/* Use Case:  Use Case 9 UC1                                                  */
/* Function:    Adds a product to an existing Arrangement in history database */
/* Parameter:   _historyID; the id of the history linked to the Arrangement   */
/* Parameter:   _productHistoryID_A; the id of the product of an Arrangement  */
/*                in the history database                                     */
/* Parameter:   _newAmount_A; the new amount of products of an Arrangement    */
/* Author:      Kevin Dorlas                                                  */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_add_product_to_arrangement_history(_historyID_A INT, _productHistoryID_A INT, _newAmount_A INT)
RETURNS VOID AS $BODY$
BEGIN
  INSERT INTO ProductArrangementHistory (historyID_A, productHistoryID_A, oldAmount_A, newAmount_A)
      VALUES (_historyID_A, _productHistoryID_A, 0, _newAmount_A);

    EXCEPTION WHEN OTHERS THEN
        RAISE NOTICE 'Error occured, the product IS NOT linked to an Arrangement AND added to the history.';
        RAISE NOTICE '% %', SQLERRM, SQLSTATE;
END;
$BODY$ LANGUAGE plpgsql STRICT;

/*============================================================================*/
/* Use Case name:     Use case 9 (included from UC2)                          */
/* Author:            Tom van Grinsven                                        */
/* Function:          Saves the history of updating an expiration             */
/*                      DATE of an Arrangement                                */
/* Parameter:         _arrangementID; the id of the Arrangement               */
/* Parameter:         _oldExpirationDate; the old expirationDate              */
/*                      of the Arrangement                                    */
/* Parameter:         _newExpirationDate; the new expirationDate              */
/*                      of the Arrangement                                    */
/* Parameter:         _employeeID; the id of the employee                     */
/* Parameter:         _deviceID; the id of the device                         */
/* Parameter:         _branchID; the id of the branch                         */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_history_update_arrangement_expirationdate(_arrangementID INT, _oldExpirationDate DATE, _newExpirationDate DATE, _employeeID INT, _deviceID INT, _branchID INT)
RETURNS VOID AS $BODY$
DECLARE _historyID INT;
BEGIN
  IF(_oldExpirationDate <> _newExpirationDate) THEN
    INSERT INTO ArrangementHistory (branchID, deviceID, employeeID, operationID, arrangementID_AH, date_AH)
      VALUES (_branchID, _deviceID, _employeeID, 1, _arrangementID, CURRENT_TIMESTAMP)
        RETURNING historyID_A INTO _historyID;

      INSERT INTO ExpArrangementH (historyID_A, oldExpirationDate_A, newExpirationDate_A) VALUES (_historyID, _oldExpirationDate, _newExpirationDate);
  END IF;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:     Use case 9 (included from UC2)                          */
/* Author:            Tom van Grinsven                                        */
/* Function:          Saves the history of updating amounts of                */
/*                      products linked to an Arrangement                     */
/* Parameter:         _arrangementID; the id of the Arrangement               */
/* Parameter:         _productID; the id of the product                       */
/* Parameter:         _oldAmount; the old amount of the product               */
/* Parameter:         _newAmount; the new amount of the product               */
/* Parameter:         _employeeID; the id of the employee                     */
/* Parameter:         _deviceID; the id of the device                         */
/* Parameter:         _branchID; the id of the branch                         */
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_history_update_arrangement_product(_arrangementID INT, _productID INT, _oldAmount INT, _newAmount INT, _employeeID INT, _deviceID INT, _branchID INT)
RETURNS VOID AS $BODY$
DECLARE _historyID INT;
BEGIN
IF(_oldAmount <> _newAmount) THEN
  INSERT INTO ArrangementHistory (branchID, deviceID, employeeID, operationID, arrangementID_AH, date_AH)
    VALUES (_branchID, _deviceID, _employeeID, 1, _arrangementID, CURRENT_TIMESTAMP)
      RETURNING historyID_A INTO _historyID;

    INSERT INTO ProductArrangementHistory (historyID_A, productHistoryID_a, oldAmount_A, newAmount_A)
    VALUES (_historyID, _productID, _oldAmount, _newAmount);
    END IF;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case:    Use Case 9 UC4                                                */
/* Function:    Add history for the creation of a MovieCheque WITH Arrangement*/
/* Author:      Tom van Grinsven                                              */
/* Parameter:   _moviechequeID; the id of a MovieCheque                       */
/* Parameter:   _employeeID; the id of the employee                           */
/* Parameter:   _deviceID; the id of the device                               */
/* Parameter:   _branchID; the id of the branch                               */
/* Parameter:   _entrance; the number of entrances linked to an Arrangement   */
/*                Or specified by the user on creation of a MovieCheque       */
/* Parameter:   _remainingAmount; the amount specified by the user on         */
/*                creation of a MovieCheque                                   */
/*============================================================================*/
-- Procedure that inserts the history of a creation of a MovieCheque with an Arrangement
CREATE OR REPLACE FUNCTION fc_history_add_moviecheque_with_arrangement(_movieChequeID INT, _employeeID INT, _deviceID INT, _branchID INT, _entrance INT, _remainingAmount MONEY)
RETURNS VOID AS $BODY$
DECLARE _historyID INT;
    temprow ProductInMovieCheque%rowtype;
BEGIN
  INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, movieChequeID_MH, date_MH, status_MH) VALUES (_employeeID, _branchID, _deviceID, 2, _movieChequeID, CURRENT_TIMESTAMP, TRUE) RETURNING historyID_M INTO _historyID;
    INSERT INTO EntranceHistory (historyID_M, oldEntrance, newEntrance) VALUES (_historyID, 0, _entrance);
    INSERT INTO AmountHistory (historyID_M, oldRemainingAmount, newRemainingAmount) VALUES (_historyID, 0::MONEY, _remainingAmount);

  -- For each product listed in the ProductInMovieCheque table, add a new record in the ProductHistory table with the corresponding productID AND the amount as listed on the Arrangement
    FOR temprow IN
      SELECT * FROM ProductInMovieCheque WHERE movieChequeID = _moviechequeID
        LOOP
          INSERT INTO ProductHistory(historyID_M, producthistoryID, oldAmount, newAmount) VALUES(_historyID, temprow.productID, 0, temprow.amount);
        END LOOP;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case:    Use Case 9 UC4                                                */
/* Author:      Tom van Grinsven                                              */
/* Function:    Adds history for the creation of a MovieCheque                */
/*                WITHOUT Arrangement                                         */
/* Parameter:   _moviechequeID; the id of a MovieCheque                       */
/* Parameter:   _employeeID; the id of the employee                           */
/* Parameter:   _deviceID; the id of the device                               */
/* Parameter:   _branchID; the id of the branch                               */
/* Parameter:   _entrance; the number of entrances linked to an Arrangement   */
/*                Or specified by the user on creation of a MovieCheque       */
/* Parameter:   _remainingAmount; the amount specified by the user on         */
/*                creation of a MovieCheque                                   */
/*============================================================================*/
-- Procedure that inserts the history of a creation of a MovieCheque without an Arrangement so there IS no need to write to ProductHistory
CREATE OR REPLACE FUNCTION fc_history_add_moviecheque(_movieChequeID INT, _employeeID INT, _deviceID INT, _branchID INT, _entrance INT, _remainingAmount MONEY)
RETURNS VOID AS $BODY$
DECLARE _historyID INT;
BEGIN
  INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, movieChequeID_MH, date_MH, status_MH)
      VALUES (_employeeID, _branchID, _deviceID, 2, _movieChequeID, CURRENT_TIMESTAMP, TRUE) RETURNING historyID_M INTO _historyID;

  INSERT INTO EntranceHistory (historyID_M, oldEntrance, newEntrance)
      VALUES (_historyID, 0, _entrance);

  INSERT INTO AmountHistory (historyID_M, oldRemainingAmount, newRemainingAmount)
      VALUES (_historyID, 0::MONEY, _remainingAmount);
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case:      Use Case 9 UC5                                              */
/* Function:      Adds entrances to the history database                      */
/* Parameter:     _employeeID; the id of the employee                         */
/* Parameter:     _branchID; the id of the branch                             */
/* Parameter:     _deviceID; the id of the used device                        */
/* Parameter:     _barcode; the id of movie in the history                    */
/* Parameter:     _oldEntrance; the amount of the old registered entrances    */
/* Parameter:     _newEntrance; the amount of the new registered entrances    */
/* Author:        Kevin Dorlas                                                */
/*============================================================================*/

CREATE OR REPLACE FUNCTION fc_add_entrance_to_history(_employeeID INT, _branchID INT, _deviceID INT, _barcode TEXT, _oldEntrance INT, _newEntrance INT)
RETURNS VOID AS $BODY$
DECLARE _historyID_M INT;
        _operationID INT;
        _movieChequeID_MH INT;
        _status_MH BOOLEAN;
BEGIN
  _operationID := 9;

    SELECT movieChequeID, "STATUS" INTO _movieChequeID_MH, _status_MH FROM MovieCheque WHERE barcode = _barcode;

  INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, movieChequeID_MH, date_MH, status_MH)
      VALUES (_employeeID, _branchID, _deviceID, _operationID, _movieChequeID_MH, CURRENT_TIMESTAMP, _status_MH)
          RETURNING historyID_M INTO _historyID_M;

  IF (_historyID_M IS NOT NULL) THEN
        INSERT INTO EntranceHistory (historyID_M, oldEntrance, newEntrance)
            VALUES (_historyID_M, _oldEntrance, _newEntrance);
    ELSE
      RAISE EXCEPTION 'Error occured, Entrance history can NOT be added';
    END IF;

    EXCEPTION WHEN OTHERS THEN
      RAISE NOTICE 'Error occured, the new amount of entrances IS NOT added to the history.';
        RAISE NOTICE '% %', SQLERRM, SQLSTATE;
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:         Use case 9 UC6                                      */
/* Function:              Archiving the change in amounts of a product on     */
/*                          a MovieCheque;                                    */
/* Author:                Cees Verhoeven                                      */
/* Parameter:       _barcode -> barcode of the cheque                         */
/* Parameter:       _productID -> the ID of the product                       */
/* Parameter:       _amount -> the new amount of the product                  */
/* Parameter:       _employeeID -> ID of the current employee                 */
/* Parameter:       _deviceID -> ID of the currently used device              */
/* Parameter:       _branchID -> ID of the current branch                     */
/*============================================================================*/

CREATE OR REPLACE FUNCTION fc_history_products_moviecheque(_barcode TEXT, _productID INT, _amount INT, _employeeID INT, _deviceID INT, _branchID INT)
RETURNS VOID AS $BODY$
DECLARE _operationID INT;
    _chequeID INT;
        _oldAmount INT;
        _historyID INT;
        _status BOOLEAN;
BEGIN
  SELECT movieChequeID INTO _chequeID FROM MovieCheque WHERE barcode = _barcode;
    SELECT operationID INTO _operationID FROM Operation WHERE operationDescription = 'Changed the amount of the product';
    SELECT amount INTO _oldAmount FROM ProductInMovieCheque WHERE movieChequeID = _chequeID AND productID = _productID;
    SELECT "STATUS" INTO _status FROM MovieCheque WHERE moviechequeID = _chequeID;
    INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, movieChequeID_MH, date_MH, status_MH)
    VALUES (_employeeID, _branchID, _deviceID, _operationID, _chequeID, CURRENT_TIMESTAMP, _status) RETURNING historyID_M INTO _historyID;
    INSERT INTO ProductHistory VALUES (_historyID, _productID, _oldAmount, _amount);
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case:        Use Case 9 UC7                                            */
/* Function:        Writes the history for the transactions there are made to */
/*                  pay entrances for a movie                                 */
/* Author:          Raymond de Bruine                                         */
/* Parameter:     _barcode -> given in the function, to find the _chequeID    */
/* Parameter:     _employeeID -> given through the application, for history   */
/* Parameter:     _deviceID -> given through the application, for history     */
/* Parameter:     _oldRemainingAmount -> The remainingAmount of the cheque    */
/*                before the transaction IS made                              */
/* Parameter:     _price -> the price the entrances to calculate the newamount*/
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_transaction_movie_to_history(_barcode TEXT, _employeeID INT, _branchID INT, _deviceID INT, _oldRemainingAmount MONEY, _price MONEY)
RETURNS VOID AS $BODY$
DECLARE _chequeID INT;
    _status BOOLEAN;
    _operationID INT;
    _historyID INT;
    _newRemainingAmount MONEY;
BEGIN
  _newRemainingAmount := _oldRemainingAmount - _price;
  IF (_newRemainingAmount < 0::MONEY) THEN
    _newRemainingAmount = 0::MONEY;
  END IF;
  SELECT movieChequeID INTO _chequeID FROM MovieCheque WHERE barcode = _barcode;
  SELECT "STATUS" INTO _status FROM MovieCheque WHERE barcode = _barcode;
  SELECT operationID INTO _operationID FROM Operation WHERE operationDescription = 'Transaction made for movie entrances';
  INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, movieChequeID_MH, date_MH, status_MH)
    VALUES (_employeeID, _branchID, _deviceID, _operationID, _chequeID, CURRENT_TIMESTAMP, _status) RETURNING historyID_M INTO _historyID;
  INSERT INTO AmountHistory (historyID_M, oldRemainingAmount, newRemainingAmount) VALUES (_historyID, _oldRemainingAmount, _newRemainingAmount);
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case:        Use Case 9 UC7                                            */
/* Function:        Writes the history for the transactions there are made to */
/*                  pay for the products that are purchased                   */
/* Author:          Raymond de Bruine                                         */
/* Parameter:     _barcode -> given in the function, to find the _chequeID    */
/* Parameter:     _employeeID -> given through the application, for history   */
/* Parameter:     _deviceID -> given through the application, for history     */
/* Parameter:     _oldRemainingAmount -> The remainingAmount of the cheque    */
/*                before the transaction IS made                              */
/* Parameter:     _price -> the price the entrances to calculate the newamount*/
/*============================================================================*/
CREATE OR REPLACE FUNCTION fc_transaction_products_to_history(_barcode TEXT, _employeeID INT, _branchID INT, _deviceID INT, _oldRemainingAmount MONEY, _price MONEY)
RETURNS VOID AS $BODY$
DECLARE _chequeID INT;
    _status BOOLEAN;
    _operationID INT;
    _historyID INT;
    _newRemainingAmount MONEY;
BEGIN
  _newRemainingAmount := _oldRemainingAmount - _price;
  IF (_newRemainingAmount < 0::MONEY) THEN
    _newRemainingAmount = 0::MONEY;
  END IF;
  SELECT movieChequeID INTO _chequeID FROM MovieCheque WHERE barcode = _barcode;
  SELECT "STATUS" INTO _status FROM MovieCheque WHERE barcode = _barcode;
  SELECT operationID INTO _operationID FROM  Operation WHERE operationDescription = 'Transaction made for product purchases';
  INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, movieChequeID_MH, date_MH, status_MH)
    VALUES (_employeeID, _branchID, _deviceID, _operationID, _chequeID, CURRENT_TIMESTAMP, _status) RETURNING historyID_M INTO _historyID;
  INSERT INTO AmountHistory (historyID_M, oldRemainingAmount, newRemainingAmount) VALUES (_historyID, _oldRemainingAmount, _newRemainingAmount);
END;
$BODY$ LANGUAGE plpgsql;

/*============================================================================*/
/* Use Case name:         Use case 9 UC8                                      */
/* Function:              Notice the blocking movie cheque in the history     */
/* Author:                Raymond de Bruine                                   */
/* Parameter:       _chequeID -> ID of the cheque                             */
/* Parameter:       _employeeID -> ID of the current employee                 */
/* Parameter:       _deviceID -> ID of the currently used device              */
/* Parameter:       _branchID -> ID of the current branch                     */
/* Parameter:       _status -> status of the current cheque                   */
/*============================================================================*/

CREATE OR REPLACE FUNCTION fc_set_history_block_moviecheque(_chequeID INT, _employeeID INT, _deviceID INT, _branchID INT, _status BOOLEAN)
RETURNS VOID AS $$
    DECLARE _operationID INT;
BEGIN
    SELECT operationID INTO _operationID FROM Operation WHERE operationDescription = 'Movie cheque is blocked';
  INSERT INTO MovieChequeHistory (employeeID, branchID, deviceID, operationID, movieChequeID_MH, date_MH, status_MH)
      VALUES(_employeeID, _branchID, _deviceID, _operationID, _chequeID, CURRENT_TIMESTAMP, _status);
END;
$$ LANGUAGE plpgsql;
